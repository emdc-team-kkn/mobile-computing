package pt.tecnico.kkn.airdesk.db;

import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import junit.framework.Assert;

/**
 * Test database creation code
 */
public class DatabaseCreationTest extends AndroidTestCase {

    private static final String TEST_FILE_PREFIX = "test_";
    private AirDeskDbOpenHelper dbHelper;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        RenamingDelegatingContext context
            = new RenamingDelegatingContext(getContext(), TEST_FILE_PREFIX);
        dbHelper = new AirDeskDbOpenHelper(context);
    }

    public void testOpenDatabase() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Assert.assertEquals(1, db.getVersion());
    }
}
