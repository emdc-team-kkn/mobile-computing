package pt.tecnico.kkn.airdesk.datasource;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import junit.framework.Assert;

import java.util.List;

import pt.tecnico.kkn.airdesk.exception.WorkspaceNotFoundException;

/**
 * Test for LocalWorkspaceDatasource class
 */
public class LocalWorkspaceDatasourceTest extends AndroidTestCase {

    private static final String TEST_FILE_PREFIX = "test_";
    private LocalWorkspaceDatasource datasource;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        RenamingDelegatingContext context
            = new RenamingDelegatingContext(getContext(), TEST_FILE_PREFIX);
        datasource = new LocalWorkspaceDatasource(context);
        datasource.open();

        datasource.newWorkspace(new Workspace(false, false, 51, "workspace 1"));
        datasource.newWorkspace(new Workspace(true, false, 52, "workspace 2"));
        datasource.newWorkspace(new Workspace(false, true, 53, "workspace 3"));
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        datasource.close();
    }

    public void testGetWorkspacesList() {
        List<Workspace> workspaceList = datasource.getWorkspacesList();
        Assert.assertEquals(3, workspaceList.size());
    }

    public void testNewWorkspace() throws WorkspaceNotFoundException {
        Workspace w = new Workspace(true, true, 54, "workspace 4");
        Workspace w2 = datasource.newWorkspace(w);

        Assert.assertEquals(w.getName(), w2.getName());
        Assert.assertEquals(w.getQuota(), w2.getQuota());
        Assert.assertEquals(w.isForeign(), w.isForeign());
        Assert.assertEquals(w.isPublic(), w.isPublic());
        Assert.assertTrue(w2.getId() != -1);
    }

    public void testGetWorkspaceDetail() throws WorkspaceNotFoundException {
        Workspace w = new Workspace(1, null);
        Workspace w2 = datasource.getWorkspaceDetail(w);

        Assert.assertEquals(1, w2.getId());
        Assert.assertEquals("workspace 1", w2.getName());
        Assert.assertEquals(51, w2.getQuota());
        Assert.assertFalse(w2.isForeign());
        Assert.assertFalse(w2.isPublic());
    }

    public void testUpdateWorkspace() throws WorkspaceNotFoundException {
        Workspace w = new Workspace(1, "workspace 1 updated");
        w.setForeign(false);
        w.setPublic(true);
        w.setQuota(1000);
        w.setPublicProfile("");
        boolean result = datasource.updateWorkspace(w);

        Assert.assertTrue(result);
    }

    public void testDeleteWorkspace() throws WorkspaceNotFoundException {
        Workspace w = new Workspace(1, null);
        boolean result = datasource.deleteWorkspace(w);
        List<Workspace> ret = datasource.getWorkspacesList();

        Assert.assertTrue(result);
        Assert.assertEquals(2, ret.size());
    }

    public void testUndeleteWorkspace() throws WorkspaceNotFoundException {
        testDeleteWorkspace();

        Workspace w = new Workspace(1, null);
        boolean undeleteResult = datasource.undeleteWorkspace(w);
        List<Workspace> ret = datasource.getWorkspacesList();

        Assert.assertTrue(undeleteResult);
        Assert.assertEquals(3, ret.size());
    }

    public void testUserWorkspace() {
        Workspace w = new Workspace(1, null);
        User u1 = new User(2, null, null);

        boolean addResult = datasource.addUserToWorkspace(w, u1);
        Assert.assertTrue(addResult);

        boolean checkResult = datasource.isUserAllowed(w, u1);
        Assert.assertTrue(checkResult);

        boolean removeResult = datasource.removeUserFromWorkspace(w, u1);
        Assert.assertTrue(removeResult);

        checkResult = datasource.isUserAllowed(w, u1);
        Assert.assertFalse(checkResult);
    }
}
