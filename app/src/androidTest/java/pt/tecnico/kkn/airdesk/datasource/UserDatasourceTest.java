package pt.tecnico.kkn.airdesk.datasource;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import junit.framework.Assert;

import java.util.List;

import pt.tecnico.kkn.airdesk.exception.UserNotFoundException;

/**
 * Test for user datasource
 */
public class UserDatasourceTest extends AndroidTestCase {

    private static final String TEST_FILE_PREFIX = "test_";
    private UserDatasource datasource;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        RenamingDelegatingContext context
            = new RenamingDelegatingContext(getContext(), TEST_FILE_PREFIX);

        datasource = new UserDatasource(context);
        datasource.open();
        datasource.newUser(new User("1@1.com", "1"));
        datasource.newUser(new User("2@2.com", "2"));
        datasource.newUser(new User("3@3.com", "3"));
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        datasource.close();
    }

    public void testGetUsersList() {
        List<User> ret = datasource.getUsersList();

        Assert.assertEquals(3, ret.size());
    }

    public void testNewUser() throws UserNotFoundException {
        User u = datasource.newUser(new User("4@4.com", "4"));
        List<User> ret = datasource.getUsersList();

        Assert.assertEquals(4, ret.size());
        Assert.assertEquals("4@4.com", u.getEmail());
        Assert.assertEquals("4", u.getNickName());
        Assert.assertEquals(4, u.getId());
    }

    public void testGetUserDetail() throws UserNotFoundException {
        User ret = datasource.getUserDetail(new User(1, null, null));

        Assert.assertEquals("1@1.com", ret.getEmail());
        Assert.assertEquals("1", ret.getNickName());
    }

    public void testDeleteUser() throws UserNotFoundException {
        testNewUser();
        boolean deleteResult = datasource.deleteUser(new User(1, null, null));
        List<User> ret = datasource.getUsersList();

        Assert.assertTrue(deleteResult);
        Assert.assertEquals(3, ret.size());
    }

}
