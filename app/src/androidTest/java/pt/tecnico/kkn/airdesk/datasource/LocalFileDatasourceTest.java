package pt.tecnico.kkn.airdesk.datasource;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import junit.framework.Assert;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * Test for LocalFileDatasource class
 */
public class LocalFileDatasourceTest extends AndroidTestCase {

    private static final String TEST_FILE_PREFIX = "test_";
    private LocalFileDatasource datasource;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        RenamingDelegatingContext context
            = new RenamingDelegatingContext(getContext(), TEST_FILE_PREFIX);
        datasource = new LocalFileDatasource(context, 1);
        datasource.open();

        datasource.newFile(new File(1, "file 1", "ha"));
        datasource.newFile(new File(1, "file 2", "haha"));
        datasource.newFile(new File(1, "file 3", "hahaha"));
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        datasource.close();
    }

    public void testGetFilesList() {
        List<File> fileList = datasource.getFilesList();

        Assert.assertEquals(3, fileList.size());
    }

    public void testNewFile() throws FileNotFoundException {
        File f = new File(1, "file 4", "hahahaha");
        File f2 = datasource.newFile(f);

        Assert.assertEquals(4, f2.getId());
        Assert.assertEquals(1, f2.getWorkspaceId());
        Assert.assertEquals("file 4", f2.getFilename());
        Assert.assertEquals("hahahaha", f2.getContent());
        Assert.assertEquals(8, f2.getFileSize());
    }

    public void testGetFileDetail() throws FileNotFoundException {
        File f = new File(1, 1, null);
        File f2 = datasource.getFileDetail(f);

        Assert.assertEquals(1, f2.getId());
        Assert.assertEquals(1, f2.getWorkspaceId());
        Assert.assertEquals("file 1", f2.getFilename());
        Assert.assertEquals("ha", f2.getContent());
        Assert.assertEquals(2, f2.getFileSize());
    }

    public void testUpdateFile() throws FileNotFoundException {
        File f = new File(1, 1, "new file 1");
        f.setContent("aaaaaaa");
        boolean updated = datasource.updateFile(f);

        Assert.assertTrue(updated);
    }

    public void testDeleteFile() throws FileNotFoundException {
        File f = new File(1, 1, null);
        boolean deleted = datasource.deleteFile(f);
        List<File> fileList = datasource.getFilesList();

        Assert.assertTrue(deleted);
        Assert.assertEquals(2, fileList.size());
    }

    public void testUndeleteFile() throws FileNotFoundException {
        testDeleteFile();
        File f = new File(1, 1, null);
        boolean restored = datasource.undeleteFile(f);
        List<File> fileList = datasource.getFilesList();

        Assert.assertTrue(restored);
        Assert.assertEquals(3, fileList.size());
    }

    public void testLockFile() throws FileNotFoundException {
        File f = new File(1, 1, null);
        f.setLockedBy(5);
        boolean locked = datasource.lockFile(f);
        File f2 = datasource.getFileDetail(f);

        Assert.assertTrue(locked);
        Assert.assertEquals(5, f2.getLockedBy());
    }

    public void testUnlockFile() throws FileNotFoundException {
        testLockFile();
        File f = new File(1, 1, null);
        boolean unlocked = datasource.lockFile(f);
        File f2 = datasource.getFileDetail(f);

        Assert.assertTrue(unlocked);
        Assert.assertEquals(0, f2.getLockedBy());
    }
}
