package pt.tecnico.kkn.airdesk.wifidirect;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.tecnico.kkn.airdesk.datasource.Workspace;

/**
 * This is used to discover workspace from other devices
 */
public class DiscoveryCommTask implements Callable<List<Workspace>> {
    private static final String TAG = "DiscoveryTask";
    private static final String REQUEST = "discover";
    private final String ipAddress;
    private final String email;
    private final String password;
    private final int port;

    public DiscoveryCommTask(String ipAddress, String email, String password, int port) {
        this.ipAddress = ipAddress;
        this.email = email;
        this.password = password;
        this.port = port;
    }

    @Override
    public List<Workspace> call() {
        //TODO SEND EMAIL ID, receive list of workspaces
        String result;
        //add workspaces to list
        Log.d(TAG, "Client Started");
        List<Workspace> workspaceList = new ArrayList<>();
        try {
            Log.d(TAG, "To Server: " + ipAddress + " " + port);
            SimWifiP2pSocket clientSocket = new SimWifiP2pSocket(ipAddress, port);
            Log.d(TAG, "Email :" + email);
            PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);

            writer.println(REQUEST);
            writer.println(email);
            writer.println(password);

            Log.d(TAG, "Write Complete");

            BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            Log.d(TAG, "Read Started");
            result = br.readLine();

            Log.d(TAG, "Result: " + result);

            JSONArray json = new JSONArray(result);

            for (int i = 0; i < json.length(); i++) {
                Workspace workspace = Workspace.fromJSONObject(json.getJSONObject(i));
                if (workspace != null) {
                    workspace.setForeign(true);
                    workspace.setIpAddress(ipAddress);
                    workspaceList.add(workspace);
                }
            }
            Log.d(TAG, "Read Finished");
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "IO Exception", e);
        } catch (JSONException e) {
            Log.e(TAG, "Json exception", e);
        }
        return workspaceList;
    }
}
