package pt.tecnico.kkn.airdesk.helper;

import android.widget.EditText;

import java.util.regex.Pattern;

/**
 * Created by nikola on 9.4.15.
 */
public class Validation {


    // call this method when you need to check email validation
    public static boolean isEmailAddress(EditText editText, String regex, String errMsg, boolean required) {
        return isValid(editText, regex, errMsg, required);
    }

    // return true if the input field is valid, based on the parameter passed
    public static boolean isValid(EditText editText, String regex, String errMsg, boolean required) {

        String text = editText.getText().toString().trim();
        // clearing the error, if it was previously set by some other values
        editText.setError(null);

        // text required and editText is blank, so return false
        if ( required && !hasText(editText, "This field is required") ) return false;

        // pattern doesn't match so returning false
        if (required && !Pattern.matches(regex, text)) {
            editText.setError(errMsg);
            return false;
        }

        return true;
    }

    // check the input field has any text or not
    // return true if it contains text otherwise false
    public static boolean hasText(EditText editText, String errMsg) {

        String text = editText.getText().toString().trim();
        editText.setError(null);

        // length 0 means there is no text
        if (text.length() == 0) {
            editText.setError(errMsg);
            return false;
        }

        return true;
    }
}
