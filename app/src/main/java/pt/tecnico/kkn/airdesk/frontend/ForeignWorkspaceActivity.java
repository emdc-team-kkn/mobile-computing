package pt.tecnico.kkn.airdesk.frontend;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonFloat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.File;
import pt.tecnico.kkn.airdesk.datasource.LocalFileDatasource;
import pt.tecnico.kkn.airdesk.datasource.RemoteWorkspaceDatasource;
import pt.tecnico.kkn.airdesk.datasource.User;
import pt.tecnico.kkn.airdesk.datasource.Workspace;
import pt.tecnico.kkn.airdesk.exception.UserNotFoundException;
import pt.tecnico.kkn.airdesk.frontend.adapter.FileAdapter;

public class ForeignWorkspaceActivity extends ActionBarActivity {
    private ListView listViewFiles;
    private ButtonFloat btnAddFile;

    private Workspace workspace;
    private File[] files = {};
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foreign_workspace);

        Intent intent = getIntent();
        workspace = intent.getParcelableExtra("WORKSPACE");

        getSupportActionBar().setTitle(workspace.getName());

        listViewFiles = (ListView)findViewById(R.id.listViewFiles);

        listViewFiles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent readFileIntent = new Intent(context, ForeignReadFileActivity.class);
                readFileIntent.putExtra("WORKSPACE", workspace);
                readFileIntent.putExtra("FILE", files[position]);
                startActivity(readFileIntent);
            }
        });

        btnAddFile = (ButtonFloat)findViewById(R.id.btnAddFile);
        btnAddFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addFileIntent = new Intent(context, AddFileActivity.class);
                addFileIntent.putExtra("WORKSPACE", workspace);
                addFileIntent.putExtra("isForeign", true);
                startActivity(addFileIntent);
            }
        });
    }

    public void updateFileList(List<File> fileList) {
        Log.d("FileList", "File List update start");
        files = fileList.toArray(new File[fileList.size()]);
        Log.d("FileList", "File number: " + String.valueOf(files.length));
        FileAdapter fileAdapter = new FileAdapter(this, R.layout.list_item_file, files);
        listViewFiles.setAdapter(fileAdapter);
        Log.d("FileList", "File list update finished");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_foreign_workspace, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_leave) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(String.format(getResources().getString(R.string.dialog_leave_workspace_msg), workspace.getName())).setTitle(R.string.dialog_leave_workspace_title);
            builder.setPositiveButton(R.string.dialog_confirm, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    new LeaveWorkspaceCommTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, workspace);
                }
            });

            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //User pressed Cancel
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        new FileListCommTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, workspace);

        super.onStart();
    }

    @Override
    protected void onResume() {
        new FileListCommTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, workspace);

        super.onResume();
    }

    public class FileListCommTask extends AsyncTask<Workspace, String, Void> {
        private static final String TAG = "FileList";
        private static final String REQUEST = "filelist";

        @Override
        protected Void doInBackground(Workspace... workspaces) {
            String result;
            Workspace workspace = workspaces[0];
            String ipAddress = workspace.getIpAddress();

            Log.d(TAG, "Client Started: " + REQUEST);
            try {
                Log.d(TAG, "To Server: " + ipAddress + " " + getString(R.string.port));
                SimWifiP2pSocket clientSocket = new SimWifiP2pSocket(ipAddress, Integer.parseInt(getString(R.string.port)));
                PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);

                writer.println(REQUEST);

                String workspaceJson = workspace.toJSONObject().toString();
                Log.d(TAG, "Workspace: " + workspaceJson);
                writer.println(workspaceJson);

                Log.d(TAG, "Write Complete");

                BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                Log.d(TAG, "Read Started");
                result = br.readLine();
                publishProgress(ipAddress, result);

                Log.d(TAG, "Read Finished");
                clientSocket.close();
            } catch (UnknownHostException e) {
                Log.e("Broadcast", "Exception", e);
            } catch (IOException e) {
                Log.e("Broadcast", "Exception", e);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            Log.d(TAG, "Result: " + values[1]);
            String ipAddress = values[0];

            try {
                JSONArray json = new JSONArray(values[1]);
                List<File> fileList = new ArrayList<>();
                for(int i = 0; i < json.length(); i++) {
                    JSONObject jsonFile = json.getJSONObject(i);
                    jsonFile.put("ownerId", ipAddress);
                    File file = File.fromJSONObject(jsonFile);
                    fileList.add(file);
                }
                Log.d(TAG, "Update file list");
                updateFileList(fileList);

            } catch (JSONException e) {
                Log.d(TAG, "Error parsing JSON");
                e.printStackTrace();
            }
        }
    }

    public class LeaveWorkspaceCommTask extends AsyncTask<Workspace, String, Void> {
        private static final String TAG = "LeaveWorkspaceTask";
        private static final String REQUEST = "leaveworkspace";

        @Override
        protected Void doInBackground(Workspace... workspaces) {
            String result;
            Workspace workspace = workspaces[0];
            String ipAddress = workspace.getIpAddress();

            SharedPreferences preferences = getSharedPreferences("AirDeskPreferences", MODE_PRIVATE);
            String email = preferences.getString("EMAIL", null);
            String password = preferences.getString("PASSWORD", null);

            Log.d(TAG, "Client Started: " + REQUEST);
            try {
                Log.d(TAG, "To Server: " + ipAddress + " " + getString(R.string.port));
                SimWifiP2pSocket clientSocket = new SimWifiP2pSocket(ipAddress, Integer.parseInt(getString(R.string.port)));
                PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);

                Log.d(TAG, "Write started");

                Log.d(TAG, "Request: " + REQUEST);
                writer.println(REQUEST);

                Log.d(TAG, "Email: " + email);
                writer.println(email);

                Log.d(TAG, "Password: " + password);
                writer.println(password);

                String workspaceJson = workspace.toJSONObject().toString();
                Log.d(TAG, "Workspace: " + workspaceJson);
                writer.println(workspaceJson);

                Log.d(TAG, "Write Complete");

                BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                Log.d(TAG, "Read Started");
                result = br.readLine();
                publishProgress(result);

                Log.d(TAG, "Read Finished");
                clientSocket.close();
            } catch (UnknownHostException e) {
                Toast.makeText(context, "Workspace owner unavailable, try again later", Toast.LENGTH_SHORT).show();

                Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(workspacesIntent);
            } catch (IOException e) {
                Toast.makeText(context, "Workspace owner unavailable, try again later", Toast.LENGTH_SHORT).show();

                Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(workspacesIntent);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            Log.d(TAG, "Result: " + values[0]);
            String result = values[0];

            if (result.equals("json_error")) {
                Toast.makeText(context, "Bad message format, try again", Toast.LENGTH_SHORT).show();
            } else if (result.equals("User not found")){
                Toast.makeText(context, "Error: User not found", Toast.LENGTH_SHORT).show();
            } else {
                if (result.equals("removed")) {
                    Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                    workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                    startActivity(workspacesIntent);
                } else {
                    Toast.makeText(context, "User cannot be removed at the moment", Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    @Override
    public void onBackPressed() {
        Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
        workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(workspacesIntent);
    }


}
