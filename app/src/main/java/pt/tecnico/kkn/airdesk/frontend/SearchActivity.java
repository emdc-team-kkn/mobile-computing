package pt.tecnico.kkn.airdesk.frontend;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.Workspace;
import pt.tecnico.kkn.airdesk.frontend.adapter.WorkspaceSearchAdapter;
import pt.tecnico.kkn.airdesk.helper.Validation;
import pt.tecnico.kkn.airdesk.wifidirect.DiscoveryCommTask;

public class SearchActivity extends ActionBarActivity {
    private EditText txtSearchQuery;
    private Button btnSearch;
    private ListView listViewWorkspaces;
    private List<Workspace> workspaceList;
    private WorkspaceSearchAdapter workspaceSearchAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        setTitle("Search for Workspaces");

        txtSearchQuery = (EditText)findViewById(R.id.txtQueryString);
        btnSearch = (Button)findViewById(R.id.btnSearch);
        listViewWorkspaces = (ListView)findViewById(R.id.listViewWorkspaces);

        workspaceList = new ArrayList<>();
        workspaceSearchAdapter = new WorkspaceSearchAdapter(this, R.layout.list_item_workspace_search, workspaceList);
        listViewWorkspaces.setAdapter(workspaceSearchAdapter);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Validation.hasText(txtSearchQuery, "Please enter some keywords")) {
                    String searchQuery = txtSearchQuery.getText().toString();
                    JSONArray jsonKeywords = new JSONArray();

                    workspaceList.clear();
                    workspaceSearchAdapter.notifyDataSetChanged();

                    String[] keywords = searchQuery.split(" ");
                    for (String keyword : keywords) {
                        jsonKeywords.put(keyword);
                    }

                    SharedPreferences preferences = getSharedPreferences("AirDeskPreferences", Context.MODE_PRIVATE);
                    String ipAddresses = preferences.getString("ipAddresses", null);

                    if (ipAddresses != null) {
                        try {
                            JSONArray jsonArray = new JSONArray(ipAddresses);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                String address = jsonArray.getString(i);
                                new SearchWorkspaceCommTaks().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, address, jsonKeywords.toString());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            Intent workspacesIntent = new Intent(this, WorkspacesActivity.class);
            workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(workspacesIntent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent workspacesIntent = new Intent(this, WorkspacesActivity.class);
        workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(workspacesIntent);
    }

    public class SearchWorkspaceCommTaks extends AsyncTask<String, String, Void> {
        private static final String TAG = "SearchWorkspaceTask";
        private static final String REQUEST = "searchworkspace";
        private String ipAddress;

        @Override
        protected Void doInBackground(String... params) {
            Log.d(TAG, "Client Started: " + REQUEST);
            ipAddress = params[0];
            String jsonString = params[1];
            String result;

            try {
                Log.d(TAG, "To Server: " + ipAddress + " " + getString(R.string.port));
                SimWifiP2pSocket clientSocket = new SimWifiP2pSocket(ipAddress, Integer.parseInt(getString(R.string.port)));

                PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);

                Log.d(TAG, "Write Started");
                writer.println(REQUEST);

                Log.d(TAG, "Write File: " + jsonString);
                writer.println(jsonString);

                Log.d(TAG, "Write Complete");

                BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                Log.d(TAG, "Read Started");
                result = br.readLine();
                publishProgress(result);

                Log.d(TAG, "Read Finished");
                clientSocket.close();
            } catch (UnknownHostException e) {
                Log.e("Broadcast", "Exception", e);
            } catch (IOException e) {
                Log.e("Broadcast", "Exception", e);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            String res = values[0];
            Log.d(TAG, res);
            try {
                JSONArray json = new JSONArray(res);

                for (int i = 0; i < json.length(); i++) {
                    Workspace workspace = Workspace.fromJSONObject(json.getJSONObject(i));
                    if (workspace != null) {
                        workspace.setForeign(true);
                        workspace.setIpAddress(ipAddress);
                        workspaceList.add(workspace);
                    }
                }

                workspaceSearchAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
