package pt.tecnico.kkn.airdesk.wifidirect;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonRectangle;
import com.google.android.gms.wallet.Wallet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.File;
import pt.tecnico.kkn.airdesk.datasource.LocalFileDatasource;
import pt.tecnico.kkn.airdesk.datasource.LocalWorkspaceDatasource;
import pt.tecnico.kkn.airdesk.datasource.User;
import pt.tecnico.kkn.airdesk.datasource.UserDatasource;
import pt.tecnico.kkn.airdesk.datasource.Workspace;
import pt.tecnico.kkn.airdesk.exception.UserNotFoundException;
import pt.tecnico.kkn.airdesk.frontend.EditFileActivity;

/**
 * Connection handler for our airdesk server
 */
public class AirdeskServerHandler implements Runnable {

    private static final String TAG = "AirdeskServerHandler";

    private static final int READER_ROLE = 0;
    private static final int EDITOR_ROLE = 1;
    private static final int ADMIN_ROLE = 2;

    private SimWifiP2pSocket socket;
    private Context context;

    public AirdeskServerHandler(SimWifiP2pSocket socket, Context context) {
        this.socket = socket;
        this.context = context;
    }

    @Override
    public void run() {
        String result;
        Log.d(TAG, "New connection");
        //Get the accepted socket object

        try {
            InputStream is = socket.getInputStream();
            OutputStream os = socket.getOutputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            PrintWriter writer = new PrintWriter(os, true);

            result = br.readLine();
            Log.d(TAG, "Request Type: " + result);
            switch (result) {
                case "discover":
                    String email = br.readLine();
                    String password = br.readLine();

                    Log.d(TAG, "User email: " + email);
                    User u = new User(email, "");

                    UserDatasource userDatasource = new UserDatasource(this.context);
                    userDatasource.open();
                    try {
                        if (userDatasource.isUser(u)) {
                            u = userDatasource.getUserDetail(u);
                            if (u.getPassword().equals("password") || u.getPassword().equals("")) {
                                u.setRole(0);
                                u.setPassword(password);
                                userDatasource.updateUser(u);
                            }
                        }

                        Log.d(TAG, "Searching for user workspaces");
                        User user = userDatasource.getUserDetail(u);
                        List<Workspace> workspaceList = userDatasource.getWorkspaceList(user);
                        JSONArray jsonArray = new JSONArray();

                        for (Workspace workspace : workspaceList) {
                            workspace.setForeign(true);
                            jsonArray.put(workspace.toJSONObject());
                        }
                        Log.d(TAG, "Workspaces in JSON: " + jsonArray.toString());
                        writer.println(jsonArray.toString());
                    } catch (UserNotFoundException e) {
                        writer.println("{\"error\":\"bad user format\"}");
                    } catch (Exception e) {
                        writer.println("{\"error\":\"unknown error\"}");
                        Log.e(TAG, "Unknown exception", e);
                    } finally {
                        userDatasource.close();
                    }
                    break;
                case "searchworkspace":
                    String tagWordsString = br.readLine();
                    Log.d(TAG, "Keywords: " + tagWordsString);
                    try {
                        JSONArray jsonArray = new JSONArray(tagWordsString);
                        List<String> searchList = new ArrayList<>();
                        for(int i = 0; i < jsonArray.length(); i++) {
                            //TODO WorkspaceDatasource search method
                            searchList.add(jsonArray.getString(i));
                        }
                        LocalWorkspaceDatasource lwd = new LocalWorkspaceDatasource(context);
                        lwd.open();
                        List<Workspace> foundWorkspaces = lwd.searchWorkspaces(searchList);
                        lwd.close();

                        JSONArray workspacesJson = new JSONArray();
                        for (Workspace workspace : foundWorkspaces) {
                            workspacesJson.put(workspace.toJSONObject());
                        }
                        Log.d(TAG, "Found Workspaces in JSON: " + workspacesJson.toString());
                        writer.println(workspacesJson.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Log.d(TAG, "Workspace search Finished");
                    break;
                case "joinworkspace":
                    String emailString = br.readLine();
                    String passwordString = br.readLine();
                    String addWorkspaceString = br.readLine();

                    Log.d(TAG, "User email: " + emailString);
                    User newUser = new User(emailString, "");

                    UserDatasource userJoinDatasource = new UserDatasource(this.context);
                    userJoinDatasource.open();
                    try {
                        if (userJoinDatasource.isUser(newUser)) {
                            newUser = userJoinDatasource.getUserDetail(newUser);
                            if (newUser.getPassword().equals("password") || newUser.getPassword().equals("")) {
                                newUser.setRole(0);
                                newUser.setPassword(passwordString);
                                userJoinDatasource.updateUser(newUser);
                            }
                        } else {
                            newUser.setRole(0);
                            newUser.setPassword(passwordString);
                            newUser = userJoinDatasource.newUser(newUser);
                        }
                    } catch (UserNotFoundException e) {
                        e.printStackTrace();
                    }

                    userDatasource = new UserDatasource(this.context);
                    userDatasource.open();
                    try {
                        Workspace workspace = Workspace.fromJSONObject(new JSONObject(addWorkspaceString));
                        Log.d(TAG, "Add user to DB");
                        LocalWorkspaceDatasource localWorkspaceDatasource = new LocalWorkspaceDatasource(context);
                        localWorkspaceDatasource.open();

                        if(!localWorkspaceDatasource.hasUser(workspace, newUser)) {
                            User user;
                            if (userDatasource.isUser(newUser)) {
                                user = userDatasource.getUserDetail(newUser);
                            } else {
                                user = userDatasource.newUser(newUser);
                            }

                            Log.d(TAG, "Adding user to workspace");

                            localWorkspaceDatasource.addUserToWorkspace(workspace, user);

                            localWorkspaceDatasource.close();

                            Log.d(TAG, "User " + user.getEmail() + " added to workspace " + workspace.getName());
                            writer.println("joined");
                        } else {
                            writer.println("member");
                        }
                    } catch (UserNotFoundException e) {
                        Log.d(TAG, "User not found");
                    } catch (JSONException e) {
                        writer.println("json_error");
                    } finally {
                        userDatasource.close();
                    }

                    Log.d(TAG, "Join Workspace Finished");
                    break;
                case "leaveworkspace":
                    String leaveWorkspaceEmail = br.readLine();
                    String leaveWorkspacePassword = br.readLine();
                    String leaveWorkspace = br.readLine();

                    Log.d(TAG, "User leave email: " + leaveWorkspaceEmail);

                    User lu = new User(leaveWorkspaceEmail, "");
                    UserDatasource lud = new UserDatasource(context);

                    try {
                        lud.open();
                        User leavingUser = lud.getUserDetail(lu);

                        if (leavingUser.getPassword().equals(leaveWorkspacePassword)) {
                            Workspace workspace = Workspace.fromJSONObject(new JSONObject(leaveWorkspace));
                            LocalWorkspaceDatasource localWorkspaceDatasource = new LocalWorkspaceDatasource(context);

                            localWorkspaceDatasource.open();
                            localWorkspaceDatasource.removeUserFromWorkspace(workspace, leavingUser);
                            localWorkspaceDatasource.close();

                            writer.println("removed");
                        } else {
                            writer.println(new JSONObject().put("type", "error").put("message", "Authentication error, operation failed"));
                        }
                    } catch (UserNotFoundException e) {
                        writer.println("User not found");
                    } catch (JSONException e) {
                        writer.println("json_error");
                    } finally {
                        lud.close();
                    }
                    Log.d(TAG, "Leave Workspace Finished");
                    break;
                case "filelist":
                    String workspaceJson = br.readLine();
                    Log.d(TAG, "Workspace: " + workspaceJson);


                    try {
                        Workspace workspace = Workspace.fromJSONObject(new JSONObject(workspaceJson));
                        LocalFileDatasource fileDatasource = new LocalFileDatasource(this.context, (int)workspace.getId());
                        fileDatasource.open();
                        List<File> fileList = fileDatasource.getFilesList();
                        fileDatasource.close();
                        JSONArray fileArray = new JSONArray();
                        for(File f : fileList) {
                            f.setOwnerId(String.valueOf(f.getWorkspaceId()));
                            fileArray.put(f.toJSONObject());
                        }
                        writer.println(fileArray.toString());
                        Log.d(TAG, "File List sent");
                    } catch (JSONException e) {
                        writer.println("{\"error\":\"bad user format\"}");
                    }
                    break;
                case "createfile":
                    String userAdminEmail = br.readLine();
                    String userAdminPassword = br.readLine();
                    User userAdmin = new User(userAdminEmail, "");
                    UserDatasource ud = new UserDatasource(context);

                    String wJson = br.readLine();
                    String fileJson = br.readLine();
                    Log.d(TAG, "File: " + fileJson);

                    try {
                        ud.open();

                        userAdmin = ud.getUserDetail(userAdmin);
                        Workspace workspace = Workspace.fromJSONObject(new JSONObject(wJson));

                        LocalWorkspaceDatasource lwdRole = new LocalWorkspaceDatasource(context);
                        lwdRole.open();
                        int role = lwdRole.userRole(workspace, userAdmin);
                        Log.d(TAG, "User role: " + String.valueOf(role));
                        lwdRole.close();

                        if (role > EDITOR_ROLE && userAdmin.getPassword().equals(userAdminPassword)) {
                            File file = File.fromJSONObject(new JSONObject(fileJson));
                            if (workspace.getQuota() > workspace.getSize() + file.getContent().getBytes().length) {
                                Log.d(TAG, "File Write Begin");
                                LocalFileDatasource fileDatasource = new LocalFileDatasource(context, file.getWorkspaceId());
                                fileDatasource.open();
                                try {
                                    fileDatasource.newFile(file);
                                } catch (FileNotFoundException e) {
                                    Log.d(TAG, e.getMessage());
                                } finally {
                                    fileDatasource.close();
                                }

                                java.io.File newFile = new java.io.File(context.getDir((workspace.getName() + Long.toString(workspace.getId())), Context.MODE_PRIVATE), file.getFilename() + ".txt");
                                Log.d(TAG, "Create on file path: " + newFile.getAbsolutePath());
                                try {
                                    FileOutputStream outputStream = new FileOutputStream(newFile);

                                    outputStream.write(file.getContent().getBytes());
                                    outputStream.close();
                                } catch (FileNotFoundException e) {
                                    writer.println(new JSONObject().put("type", "error").put("message", "Error when writing to file"));
                                    Log.d(TAG, e.getMessage());
                                } catch (IOException e) {
                                    writer.println(new JSONObject().put("type", "error").put("message", "Error while writing to file"));
                                    Log.d(TAG, e.getMessage());
                                }

                                Log.d(TAG, "File Write Successful");

                                writer.println(new JSONObject().put("type", "success").put("message", "File created"));

                                Log.d(TAG, "File create finished");
                            } else {
                                writer.println(new JSONObject().put("type", "error").put("message", "The file is too big for the workspace"));
                            }
                        } else {
                            writer.println(new JSONObject().put("type", "error").put("message", "You don't have the necessary permissions to create a file"));
                        }
                    } catch (JSONException e) {
                        writer.println("json_error");
                    } catch (UserNotFoundException e) {
                        writer.println("User not found");
                    } finally {
                        ud.close();
                    }
                    break;
                case "readfile":
                    String userReadEmail = br.readLine();
                    String userReaderPassword = br.readLine();

                    User userReader = new User(userReadEmail, "");
                    UserDatasource udr = new UserDatasource(context);

                    String workspaceString = br.readLine();
                    String fileString = br.readLine();

                    Log.d(TAG, "Workspace: " + workspaceString);
                    Log.d(TAG, "File: " + fileString);

                    try {
                        udr.open();

                        userReader = udr.getUserDetail(userReader);
                        Workspace workspace = Workspace.fromJSONObject(new JSONObject(workspaceString));

                        if (userReader.getPassword().equals(userReaderPassword)) {

                            File file = File.fromJSONObject(new JSONObject(fileString));

                            java.io.File openFile = new java.io.File(context.getDir((workspace.getName() + Long.toString(workspace.getId())), Context.MODE_PRIVATE), file.getFilename() + ".txt");

                            Log.d(TAG, "File path: " + openFile.getAbsolutePath());

                            FileInputStream inputStream = new FileInputStream(openFile);
                            byte[] buffer = new byte[file.getFileSize()];

                            inputStream.read(buffer, 0, file.getFileSize());
                            String content = new String(buffer);
                            inputStream.close();

                            Log.d(TAG, "File read finished: " + content);

                            JSONObject jsonContent = new JSONObject();
                            jsonContent.put("type", "file_content");
                            jsonContent.put("content", content);
                            writer.println(jsonContent.toString());
                        } else {
                            writer.println(new JSONObject().put("type", "error").put("message", "You don't have the necessary permissions to read a file"));
                        }
                    } catch (FileNotFoundException e) {
                        writer.println("file_error");
                        Log.d(TAG, e.getMessage());
                    } catch (IOException e) {
                        writer.println("file_error");
                        Log.d(TAG, e.getMessage());
                    } catch (JSONException e) {
                        writer.println("json_error");
                    } catch (UserNotFoundException e) {
                        writer.println("User not found");
                    } finally {
                        udr.close();
                    }
                    break;
                case "lockfile":
                    String userEditorEmail = br.readLine();
                    String userEditorPassword = br.readLine();
                    User userEditor = new User(userEditorEmail, "");
                    UserDatasource ude = new UserDatasource(context);

                    String lockFileString = br.readLine();

                    try {
                        ude.open();

                        userEditor = ude.getUserDetail(userEditor);
                        File file = File.fromJSONObject(new JSONObject(lockFileString));
                        Workspace workspace = new Workspace(file.getWorkspaceId(), "");

                        LocalWorkspaceDatasource lwdRole = new LocalWorkspaceDatasource(context);
                        lwdRole.open();
                        int role = lwdRole.userRole(workspace, userEditor);
                        Log.d(TAG, "User role: " + String.valueOf(role));
                        lwdRole.close();

                        if (role > READER_ROLE && userEditor.getPassword().equals(userEditorPassword)) {

                            LocalFileDatasource fileDatasource = new LocalFileDatasource(context, file.getWorkspaceId());

                            try {
                                fileDatasource.open();
                                if (fileDatasource.isFileLocked(file) == false) {
                                    Boolean l = fileDatasource.lockFile(file);
                                    Log.d(TAG, "File locked: " + l.toString());
                                    writer.println(new JSONObject().put("type", "success").put("message", "File locked").toString());
                                } else {
                                    writer.println(new JSONObject().put("type", "error").put("message", "File already locked").toString());
                                }
                            } catch (FileNotFoundException e) {
                                writer.println(new JSONObject().put("type", "error").put("message", "File not found").toString());
                                Log.d("FILE", e.getMessage());
                            } finally {
                                fileDatasource.close();
                            }
                        } else {
                            writer.println(new JSONObject().put("type", "error").put("message", "You don't have the necessary permissions to edit a file"));
                        }
                    } catch (JSONException e) {
                        writer.println("json_error");
                    } catch (UserNotFoundException e) {
                        writer.println("User not found");
                    }
                    Log.d(TAG, "File Lock finished");

                    break;
                case "unlockfile":
                    String unlockFileString = br.readLine();

                    try {
                        File file = File.fromJSONObject(new JSONObject(unlockFileString));

                        LocalFileDatasource fileDatasource = new LocalFileDatasource(context, file.getWorkspaceId());

                        Log.d(TAG, "Unlock file begin");
                        fileDatasource.open();
                        try {
                            fileDatasource.unlockFile(file);
                        } catch (FileNotFoundException e) {
                            writer.println(new JSONObject().put("type", "error").put("message", "Error while saving file information"));
                            Log.d(TAG, "Error on File Unlock in DB");
                        } finally {
                            fileDatasource.close();
                        }
                        Log.d(TAG, "Unlock file end");

                        writer.println(new JSONObject().put("type", "success").put("message", "File edited"));
                    } catch (JSONException e) {
                        writer.println("json_error");
                    }
                    Log.d(TAG, "File Unlock finished");

                    break;
                case "editfile":
                    String userEditEmail = br.readLine();
                    String userEditPassword = br.readLine();
                    User userEdit = new User(userEditEmail, "");
                    UserDatasource uded = new UserDatasource(context);

                    String oldFilename = br.readLine();
                    String editFileString = br.readLine();
                    String editFileWorkspace = br.readLine();
                    Boolean locked = false;

                    Log.d(TAG, "File: " + oldFilename + " " + editFileString + " " + editFileWorkspace);

                    try {
                        uded.open();
                        userEdit = uded.getUserDetail(userEdit);

                        Workspace workspace = Workspace.fromJSONObject(new JSONObject(editFileWorkspace));

                        LocalWorkspaceDatasource lwdRole = new LocalWorkspaceDatasource(context);
                        lwdRole.open();
                        int role = lwdRole.userRole(workspace, userEdit);
                        Log.d(TAG, "User role: " + String.valueOf(role));
                        lwdRole.close();

                        if (role > READER_ROLE && userEdit.getPassword().equals(userEditPassword)) {
                            File file = File.fromJSONObject(new JSONObject(editFileString));
                            if (workspace.getQuota() > workspace.getSize() + file.getContent().getBytes().length) {
                                String newContent = file.getContent();
                                Log.d(TAG, "Content: " + newContent);
                                file.setContent("");
                                file.setFileSize(newContent.getBytes().length);

                                Log.d(TAG, "DB file update begin");
                                LocalFileDatasource fileDatasource = new LocalFileDatasource(context, file.getWorkspaceId());
                                fileDatasource.open();
                                try {
                                    locked = fileDatasource.isFileLocked(file);
                                    Log.d(TAG, "File lock: " + locked.toString());
                                    fileDatasource.updateFile(file);
                                } catch (FileNotFoundException e) {
                                    writer.println(new JSONObject().put("type", "error").put("message", "Error while saving file information"));
                                    Log.d(TAG, "Database error");
                                } finally {
                                    fileDatasource.close();
                                }
                                Log.d(TAG, "DB file update end");

                                Log.d(TAG, "File content write begin");
                                java.io.File fileToWrite = new java.io.File(context.getDir((workspace.getName() + Long.toString(workspace.getId())), Context.MODE_PRIVATE), oldFilename + ".txt");

                                if (!oldFilename.equals(file.getFilename())) {
                                    java.io.File newFile = new java.io.File(context.getDir((workspace.getName() + Long.toString(workspace.getId())), Context.MODE_PRIVATE), file.getFilename() + ".txt");
                                    fileToWrite.renameTo(newFile);
                                    fileToWrite = newFile;
                                }
                                Log.d(TAG, "File path: " + fileToWrite.getAbsolutePath());
                                try {
                                    FileOutputStream outputStream = new FileOutputStream(fileToWrite.getAbsolutePath());

                                    outputStream.write(newContent.getBytes());
                                    outputStream.close();
                                } catch (FileNotFoundException e) {
                                    writer.println(new JSONObject().put("type", "error").put("message", "Error when writing to file"));
                                    Log.d(TAG, e.getMessage());
                                } catch (IOException e) {
                                    writer.println(new JSONObject().put("type", "error").put("message", "Error while writing to file"));
                                    Log.d(TAG, e.getMessage());
                                }
                                Log.d(TAG, "File content write end");

                                Log.d(TAG, "Unlock file begin");
                                fileDatasource.open();
                                try {
                                    fileDatasource.unlockFile(file);
                                } catch (FileNotFoundException e) {
                                    writer.println(new JSONObject().put("type", "error").put("message", "Error while saving file information"));
                                    Log.d(TAG, "Error on File Unlock in DB");
                                } finally {
                                    fileDatasource.close();
                                }
                                Log.d(TAG, "Unlock file end");

                                writer.println(new JSONObject().put("type", "success").put("message", "File edited"));
                            } else {
                                writer.println(new JSONObject().put("type", "error").put("message", "The file is too big for the workspace"));
                            }
                        } else {
                            writer.println(new JSONObject().put("type", "error").put("message", "You don't have the necessary permissions to edit a file"));
                        }
                    } catch (JSONException e) {
                        writer.println("json_error");
                    } catch (UserNotFoundException e) {
                        writer.println("User not found");
                    } finally {
                        uded.close();
                    }
                    Log.d(TAG, "File Edit finished");
                    break;
                case "deletefile":
                    String userDeleteEmail = br.readLine();
                    String userDeletePassword = br.readLine();
                    User userAdminDelete = new User(userDeleteEmail, "");
                    UserDatasource udd = new UserDatasource(context);

                    String deleteWorkspaceFileString = br.readLine();
                    String deleteFileString = br.readLine();

                    try {
                        udd.open();

                        userAdminDelete = udd.getUserDetail(userAdminDelete);
                        Workspace workspace = Workspace.fromJSONObject(new JSONObject(deleteWorkspaceFileString));

                        LocalWorkspaceDatasource lwdRole = new LocalWorkspaceDatasource(context);
                        lwdRole.open();
                        int role = lwdRole.userRole(workspace, userAdminDelete);
                        Log.d(TAG, "User role: " + String.valueOf(role));
                        lwdRole.close();

                        if (role > EDITOR_ROLE && userAdminDelete.getPassword().equals(userDeletePassword)) {

                            File file = File.fromJSONObject(new JSONObject(deleteFileString));

                            LocalFileDatasource fileDatasource = new LocalFileDatasource(context, file.getWorkspaceId());
                            fileDatasource.open();
                            fileDatasource.deleteFile(file);

                            java.io.File delFile = new java.io.File(context.getDir((workspace.getName() + Long.toString(workspace.getId())), Context.MODE_PRIVATE), file.getFilename() + ".txt");
                            delFile.delete();

                            fileDatasource.close();

                            writer.println(new JSONObject().put("type", "success").put("message", "File deleted"));
                        } else {
                            writer.println(new JSONObject().put("type", "error").put("message", "You don't have the necessary permissions to delete a file"));
                        }
                    } catch (JSONException e) {
                        writer.println("json_error");
                    } catch (UserNotFoundException e) {
                        writer.println("User not found");
                    }
                    writer.println("File Delete finished");
                    break;
                default:
                    writer.println("AirdeskServerHandler");
                    break;
            }

            Log.d(TAG, "Response Sent");
            is.close();
            os.close();
            socket.close();
        } catch (IOException e) {
            Log.e(TAG, "Exception when handling connection", e);
        }
    }
}
