package pt.tecnico.kkn.airdesk.wifidirect;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import pt.inesc.termite.wifidirect.SimWifiP2pDevice;
import pt.inesc.termite.wifidirect.SimWifiP2pDeviceList;
import pt.inesc.termite.wifidirect.SimWifiP2pInfo;
import pt.inesc.termite.wifidirect.SimWifiP2pManager;
import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.Workspace;

/**
 * Group info listener for our Airdesk app
 */
public class AirdeskGroupInfoListener implements SimWifiP2pManager.GroupInfoListener {

    public static final String GROUP_DISCOVER_RESULT = "GROUP_DISCOVER_RESULT";

    private static final String TAG = "GroupInfoListener";
    private final LocalBroadcastManager localBroadcaster;
    private Context context;

    public AirdeskGroupInfoListener(Context context) {
        this.context = context;
        this.localBroadcaster = LocalBroadcastManager.getInstance(context);
    }

    @Override
    public synchronized void onGroupInfoAvailable(SimWifiP2pDeviceList simWifiP2pDeviceList, SimWifiP2pInfo simWifiP2pInfo) {
        //TODO CHANGE METHOD
        // compile list of network members
        SharedPreferences preferences = context.getSharedPreferences("AirDeskPreferences", Context.MODE_PRIVATE);
        String email = preferences.getString("EMAIL", null);

        JSONArray ipAddressList = new JSONArray();

        Log.d(TAG, simWifiP2pDeviceList.toString());
        if (email != null) {
            for (String deviceName : simWifiP2pInfo.getDevicesInNetwork()) {
                SimWifiP2pDevice device = simWifiP2pDeviceList.getByName(deviceName);
                Log.d("VIRTUAL_IP", device.getVirtIp());
                ipAddressList.put(device.getVirtIp());
            }
        }

        // Store the ip addresses in the shared preference
        String ipAddressString = ipAddressList.toString();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("ipAddresses", ipAddressString);
        editor.commit();

        Log.d(TAG, ipAddressString);

        Intent intent = new Intent(GROUP_DISCOVER_RESULT);
        localBroadcaster.sendBroadcast(intent);
    }

}
