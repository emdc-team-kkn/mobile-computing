package pt.tecnico.kkn.airdesk.frontend;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonRectangle;
import com.gc.materialdesign.views.Slider;

import java.util.ArrayList;

import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.LocalWorkspaceDatasource;
import pt.tecnico.kkn.airdesk.datasource.User;
import pt.tecnico.kkn.airdesk.datasource.UserDatasource;
import pt.tecnico.kkn.airdesk.datasource.Workspace;
import pt.tecnico.kkn.airdesk.exception.UserNotFoundException;
import pt.tecnico.kkn.airdesk.exception.WorkspaceNotFoundException;
import pt.tecnico.kkn.airdesk.helper.MemBytesToHuman;
import pt.tecnico.kkn.airdesk.helper.Memory;
import pt.tecnico.kkn.airdesk.helper.Validation;

public class AddWorkspaceActivity extends ActionBarActivity {
    private TextView lblQuota;
    private EditText txtWorkspaceName;
    private Slider seekBar;
    private EditText txtDescription;
//    private MultiAutoCompleteTextView txtClients;
    private SwitchCompat switchPrivate;
    private EditText txtTags;
    private Context context = this;
    private Workspace workspace;

    private ButtonRectangle btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_workspace);

        txtWorkspaceName = (EditText)findViewById(R.id.txtWoerkspaceName);
        txtDescription = (EditText)findViewById(R.id.txtDescription);
//        txtClients = (MultiAutoCompleteTextView)findViewById(R.id.txtClients);
        switchPrivate = (SwitchCompat)findViewById(R.id.tglPublic);
        txtTags = (EditText)findViewById(R.id.txtTags);
        seekBar = (Slider)findViewById(R.id.seekBarQuota);
        lblQuota = (TextView)findViewById(R.id.lblQuota);

        getSupportActionBar().setTitle("Add Workspace");

//        Cursor emailCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, null, null, null);
//        ArrayList<String> emailAddressCollection = new ArrayList<String>();
//        while (emailCursor.moveToNext())
//        {
//            String email = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
//            emailAddressCollection.add(email);
//        }
//        emailCursor.close();
//
//        String[] emailAddresses = new String[emailAddressCollection.size()];
//        emailAddressCollection.toArray(emailAddresses);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, emailAddresses);
//        txtClients.setAdapter(adapter);
//        txtClients.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        final MemBytesToHuman memory = new MemBytesToHuman();
        final Memory freeMemory = MemBytesToHuman.bytesToHumanType(memory.getFreeMemory(), "Kb");
        seekBar.setMax(freeMemory.getValue());
        seekBar.setOnValueChangedListener(new Slider.OnValueChangedListener() {
            @Override
            public void onValueChanged(int progress) {
                lblQuota.setText("Quota: " + Integer.toString(progress) + freeMemory.getType());
            }
        });

        btnSave = (ButtonRectangle)findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quota = (int)MemBytesToHuman.humanToBytes(new Memory(seekBar.getValue(), freeMemory.getType()));
                if (quota > 0) {
                    if (Validation.hasText(txtWorkspaceName, "The workspace name is required")) {
                        //save workspace info to database
                        String workspaceName = txtWorkspaceName.getText().toString();

                        String descriptionTxt = txtDescription.getText().toString();

                        String tags = "";
                        Boolean isPublic = switchPrivate.isChecked();
                        if (isPublic) {
                            tags = txtTags.getText().toString();
                        }

                        workspace = new Workspace(false, isPublic, quota, workspaceName, descriptionTxt, tags);

                        LocalWorkspaceDatasource workspaceDatasource = new LocalWorkspaceDatasource(context);
                        workspaceDatasource.open();
                        try {
                            workspace = workspaceDatasource.newWorkspace(workspace);
                            context.getDir((workspace.getName() + Long.toString(workspace.getId())), Context.MODE_PRIVATE);
                        } catch (WorkspaceNotFoundException e) {
                            Toast.makeText(context, "Error! Try again.", Toast.LENGTH_LONG).show();
                            Log.d("WORKSPACE", e.getMessage());
                        } finally {
                            workspaceDatasource.close();
                        }

//                        String clientsString = txtClients.getText().toString();
//                        String[] clientsList = clientsString.split(", ");
//                        for (String client : clientsList) {
//                            User user = new User(-1, client, "User", "password", 0);
//                            UserDatasource userDatasource = new UserDatasource(context);
//                            userDatasource.open();
//                            try {
//                                if (!userDatasource.isUser(user)) {
//                                    user = userDatasource.newUser(user);
//                                } else {
//                                    user = userDatasource.getUserDetail(user);
//                                };
//
//                                workspaceDatasource.open();
//                                workspaceDatasource.addUserToWorkspace(workspace, user);
//                                workspaceDatasource.close();
//
//                            } catch (UserNotFoundException e) {
//                                Log.d("USER", "Add User DB error.");
//                            } finally {
//                                userDatasource.close();
//                            }
//                        }


                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage(String.format(getResources().getString(R.string.dialog_invite_to_workspace_msg), workspace.getName())).setTitle(R.string.dialog_add_workspace_title);
                        builder.setPositiveButton(R.string.dialog_confirm, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent workspaceIntent = new Intent(context, InviteUsersActivity.class);
                                workspaceIntent.putExtra("WORKSPACE", workspace);
                                finish();
                                startActivity(workspaceIntent);
                            }
                        });
                        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                                Intent intent = new Intent(context, WorkspacesActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    };
                } else {
                    Toast.makeText(context, "Quota needs to be bigger than 0(zero)", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_add_workspace, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}
