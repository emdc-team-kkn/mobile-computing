package pt.tecnico.kkn.airdesk.datasource;

import java.util.List;

import pt.tecnico.kkn.airdesk.exception.UserNotFoundException;

/**
 * Data source interface for our user
 */
public interface IUserDatasource {

    public List<User> getUsersList();

    public User getUserDetail(User u) throws UserNotFoundException;

    public User newUser(User u) throws UserNotFoundException;

    public boolean deleteUser(User u) throws UserNotFoundException;

    public boolean isUser(User u);

    public void open();

    public void close();
}
