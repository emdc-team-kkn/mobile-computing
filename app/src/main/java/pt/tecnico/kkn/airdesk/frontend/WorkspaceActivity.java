package pt.tecnico.kkn.airdesk.frontend;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonFloat;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.File;
import pt.tecnico.kkn.airdesk.datasource.LocalFileDatasource;
import pt.tecnico.kkn.airdesk.datasource.LocalWorkspaceDatasource;
import pt.tecnico.kkn.airdesk.datasource.Workspace;
import pt.tecnico.kkn.airdesk.exception.WorkspaceNotFoundException;
import pt.tecnico.kkn.airdesk.frontend.adapter.FileAdapter;

public class WorkspaceActivity extends ActionBarActivity {
    private ListView listViewFiles;
    private ButtonFloat btnAddFile;

    private Workspace workspace;
    private File[] files;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workspace);

        Intent intent = getIntent();
        workspace = intent.getParcelableExtra("WORKSPACE");

        setTitle(workspace.getName());

        LocalFileDatasource fileDatasource = new LocalFileDatasource(context, (int)workspace.getId());
        fileDatasource.open();
        List<File> fileList = fileDatasource.getFilesList();
        fileDatasource.close();

        files = fileList.toArray(new File[fileList.size()]);

        FileAdapter fileAdapter = new FileAdapter(this, R.layout.list_item_file, files);
        listViewFiles = (ListView)findViewById(R.id.listViewFiles);
        listViewFiles.setAdapter(fileAdapter);

        listViewFiles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                //File read/edit
                Intent readFileIntent = new Intent(context, ReadFileActivity.class);
                readFileIntent.putExtra("WORKSPACE", workspace);
                readFileIntent.putExtra("FILE", files[position]);
                startActivity(readFileIntent);
            }
        });

        btnAddFile = (ButtonFloat)findViewById(R.id.btnAddFile);
        btnAddFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addFileintent = new Intent(context, AddFileActivity.class);
                addFileintent.putExtra("WORKSPACE", workspace);
                startActivity(addFileintent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_workspace, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_invite_users:
                Intent inviteUsersIntent = new Intent(context, InviteUsersActivity.class);
                inviteUsersIntent.putExtra("WORKSPACE", workspace);
                startActivity(inviteUsersIntent);

                return true;
            case R.id.action_edit:
                Intent editWorkspaceIntent = new Intent(context, EditWorkspaceActivity.class);
                editWorkspaceIntent.putExtra("TYPE", "WORKSPACE_EDIT");
                editWorkspaceIntent.putExtra("WORKSPACE", workspace);
                startActivity(editWorkspaceIntent);

                return true;
            case R.id.action_discard:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(String.format(getResources().getString(R.string.dialog_delete_workspace_msg), workspace.getName())).setTitle(R.string.dialog_delete_workspace_title);
                builder.setPositiveButton(R.string.dialog_confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        LocalWorkspaceDatasource workspaceDatasource = new LocalWorkspaceDatasource(context);
                        workspaceDatasource.open();

                        try {
                            workspaceDatasource.removeWorkspaceUsers(workspace);
                            workspaceDatasource.removeWorkspaceFiles(workspace);
                            workspaceDatasource.deleteWorkspace(workspace);

                            java.io.File workspaceDir = context.getDir((workspace.getName() + Long.toString(workspace.getId())), Context.MODE_PRIVATE);

                            if (workspaceDir.isDirectory()) {
                                for(java.io.File child : workspaceDir.listFiles()) {
                                    child.delete();
                                }

                                workspaceDir.delete();
                            }

                        } catch (WorkspaceNotFoundException e) {
                            Toast.makeText(context, "Error while deleting workspace.\nPlease try again!", Toast.LENGTH_LONG);
                            Log.d("DB_ERROR", "Workspace " + workspace.getName() + "(" + workspace.getId() + ") not deleted.");
                        };
                        workspaceDatasource.close();

                        Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                        workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        workspacesIntent.putExtra("TYPE", "WORKSPACE_DELETED");
                        workspacesIntent.putExtra("WORKSPACE_NAME", workspace.getName());
                        startActivity(workspacesIntent);
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
        workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(workspacesIntent);
    }
}
