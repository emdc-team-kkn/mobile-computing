package pt.tecnico.kkn.airdesk.frontend.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.List;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.Workspace;
import pt.tecnico.kkn.airdesk.frontend.WorkspacesActivity;

/**
 * Created by nikola on 16.3.15.
 */
public class WorkspaceSearchAdapter extends ArrayAdapter<Workspace> {
    private Context context;
    private int layoutResourceId;
    private List<Workspace> data = null;

    public WorkspaceSearchAdapter(Context context, int layoutResourceId, List<Workspace> data) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        WorkspaceHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new WorkspaceHolder();
            holder.txtName = (TextView)row.findViewById(R.id.txtName);
            holder.btnJoin = (Button)row.findViewById(R.id.btnJoin);

            row.setTag(holder);
        }
        else
        {
            holder = (WorkspaceHolder)row.getTag();
        }

        Workspace workspace = data.get(position);
        holder.txtName.setText(workspace.getName());
        holder.btnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new JoinWorkspaceCommTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data.get(position));
            }
        });

        return row;
    }

    static class WorkspaceHolder
    {
        TextView txtName;
        Button btnJoin;
    }

    public class JoinWorkspaceCommTask extends AsyncTask<Workspace, String, Void> {
        private static final String TAG = "JoinWorkspaceTask";
        private static final String REQUEST = "joinworkspace";
        private Workspace workspace;

        @Override
        protected Void doInBackground(Workspace... workspaces) {
            String result;
            workspace = workspaces[0];
            String ipAddress = workspace.getIpAddress();

            SharedPreferences preferences = context.getSharedPreferences("AirDeskPreferences", Context.MODE_PRIVATE);
            String email = preferences.getString("EMAIL", null);
            String password = preferences.getString("PASSWORD", null);

            Log.d(TAG, "Client Started: " + REQUEST);
            try {
                Log.d(TAG, "To Server: " + ipAddress + " " + context.getString(R.string.port));
                SimWifiP2pSocket clientSocket = new SimWifiP2pSocket(ipAddress, Integer.parseInt(context.getString(R.string.port)));
                PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);

                Log.d(TAG, "Write started");

                Log.d(TAG, "Request: " + REQUEST);
                writer.println(REQUEST);

                Log.d(TAG, "Email: " + email);
                writer.println(email);

                Log.d(TAG, "Password: " + password);
                writer.println(password);

                String workspaceJson = workspace.toJSONObject().toString();
                Log.d(TAG, "Workspace: " + workspaceJson);
                writer.println(workspaceJson);

                Log.d(TAG, "Write Complete");

                BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                Log.d(TAG, "Read Started");
                result = br.readLine();
                publishProgress(result);

                Log.d(TAG, "Read Finished");
                clientSocket.close();
            } catch (UnknownHostException e) {
                Toast.makeText(context, "Workspace owner unavailable, try again later", Toast.LENGTH_SHORT).show();

                Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(workspacesIntent);
            } catch (IOException e) {
                Toast.makeText(context, "Workspace owner unavailable, try again later", Toast.LENGTH_SHORT).show();

                Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(workspacesIntent);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            Log.d(TAG, "Result: " + values[0]);
            String result = values[0];

            if (result.equals("json_error")) {
                Toast.makeText(context, "Bad message format, try again", Toast.LENGTH_SHORT).show();
            } else {
                if (result.equals("joined")) {
                    Toast.makeText(context, "Successfully joined workspace " + workspace.getName(), Toast.LENGTH_SHORT).show();
                } else if (result.equals("member")) {
                    Toast.makeText(context, "Cannot join workspace, already a member", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Cannot join workspace at the moment", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
