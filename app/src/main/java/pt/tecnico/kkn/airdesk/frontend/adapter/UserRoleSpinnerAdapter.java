package pt.tecnico.kkn.airdesk.frontend.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import pt.tecnico.kkn.airdesk.R;

/**
 * Created by nikola on 13.5.15.
 */
public class UserRoleSpinnerAdapter extends ArrayAdapter<String> {
    private Context context;
    private int layoutResourceId;
    private String data[] = null;

    public UserRoleSpinnerAdapter(Context context, int layoutResourceId, String[] data) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        RoleHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) this.context).getLayoutInflater();
            row = inflater.inflate(this.layoutResourceId, parent, false);

            holder = new RoleHolder();
            holder.lblRole = (TextView)row.findViewById(R.id.lblRole);

            row.setTag(holder);
        } else {
            holder = (RoleHolder)row.getTag();
        }

        holder.lblRole.setText(data[position]);

        return row;
    }

    static class RoleHolder {
        TextView lblRole;
    }
}
