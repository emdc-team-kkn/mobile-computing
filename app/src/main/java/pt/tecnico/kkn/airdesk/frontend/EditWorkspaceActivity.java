package pt.tecnico.kkn.airdesk.frontend;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonRectangle;
import com.gc.materialdesign.views.Slider;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.LocalWorkspaceDatasource;
import pt.tecnico.kkn.airdesk.datasource.User;
import pt.tecnico.kkn.airdesk.datasource.UserDatasource;
import pt.tecnico.kkn.airdesk.datasource.Workspace;
import pt.tecnico.kkn.airdesk.exception.UserNotFoundException;
import pt.tecnico.kkn.airdesk.exception.WorkspaceNotFoundException;
import pt.tecnico.kkn.airdesk.helper.MemBytesToHuman;
import pt.tecnico.kkn.airdesk.helper.Memory;
import pt.tecnico.kkn.airdesk.helper.Validation;

public class EditWorkspaceActivity extends ActionBarActivity {
    private TextView lblQuota;
    private EditText txtWorkspaceName;
    private Slider seekBar;
    private EditText txtDescription;
//    private MultiAutoCompleteTextView txtClients;
    private SwitchCompat switchPrivate;
    private EditText txtTags;
    private Context context = this;
    private Workspace workspace;
    private Memory freeMemory;

    private ButtonRectangle btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_workspace);

        Intent intent = getIntent();

        txtWorkspaceName = (EditText)findViewById(R.id.txtWoerkspaceName);
        txtDescription = (EditText)findViewById(R.id.txtDescription);
//        txtClients = (MultiAutoCompleteTextView)findViewById(R.id.txtClients);
        switchPrivate = (SwitchCompat)findViewById(R.id.tglPublic);
        txtTags = (EditText)findViewById(R.id.txtTags);
        seekBar = (Slider)findViewById(R.id.seekBarQuota);

        lblQuota = (TextView)findViewById(R.id.lblQuota);

        getSupportActionBar().setTitle("Edit Workspace");

        workspace = intent.getParcelableExtra("WORKSPACE");

        txtWorkspaceName.setText(workspace.getName());
        txtDescription.setText(workspace.getPublicProfile());
        switchPrivate.setChecked(workspace.isPublic());
        txtTags.setText(workspace.getTags());

        final MemBytesToHuman memory = new MemBytesToHuman();
        Memory quotaMemory = MemBytesToHuman.bytesToHuman(workspace.getQuota());
        long freeMemSize = memory.getFreeMemory();
        freeMemory = new Memory((int)MemBytesToHuman.humanToBytesType(freeMemSize, quotaMemory.getType()), quotaMemory.getType());
        lblQuota.setText("Quota: " + quotaMemory.getValue() + quotaMemory.getType());

//        Cursor emailCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, null, null, null);
//
//        ArrayList<String> emailAddressCollection = new ArrayList<String>();
//
//        while (emailCursor.moveToNext())
//        {
//            String email = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
//            emailAddressCollection.add(email);
//        }
//        emailCursor.close();
//
//        String[] emailAddresses = new String[emailAddressCollection.size()];
//        emailAddressCollection.toArray(emailAddresses);
//
//        LocalWorkspaceDatasource workspaceDatasource = new LocalWorkspaceDatasource(context);
//        workspaceDatasource.open();
//        List<User> usersList = workspaceDatasource.getUserList(workspace);
//        String userEmails = "";
//
//        for (User u : usersList) {
//            userEmails += u.getEmail() + ", ";
//        };
//        workspaceDatasource.close();
//
//        txtClients.setText(userEmails);
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//                android.R.layout.simple_dropdown_item_1line, emailAddresses);
//
//        txtClients.setAdapter(adapter);
//        txtClients.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        seekBar.setMax(freeMemory.getValue());
        seekBar.setValue(quotaMemory.getValue());

        seekBar.setOnValueChangedListener(new Slider.OnValueChangedListener() {
            @Override
            public void onValueChanged(int progress) {
                lblQuota.setText("Quota: " + Integer.toString(progress) + freeMemory.getType());
            }
        });

        btnSave = (ButtonRectangle)findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveWorkspace();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_edit_workspace, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(String.format(getResources().getString(R.string.dialog_edit_workspace_msg), workspace.getName())).setTitle(R.string.dialog_edit_workspace_title);
            builder.setPositiveButton(R.string.dialog_confirm, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    saveWorkspace();
                    Intent workspaceIntent = new Intent(context, WorkspaceActivity.class);
                    workspaceIntent.putExtra("WORKSPACE", workspace);
                    finish();
                    startActivity(workspaceIntent);
                }
            });
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                    Intent workspaceIntent = new Intent(context, WorkspaceActivity.class);
                    workspaceIntent.putExtra("WORKSPACE", workspace);
                    finish();
                    startActivity(workspaceIntent);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveWorkspace() {
        //save workspace info to database

        int quota = (int)MemBytesToHuman.humanToBytes(new Memory(seekBar.getValue(), freeMemory.getType()));

        if (quota > 0) {
            if (Validation.hasText(txtWorkspaceName, "The workspace name is required")) {
                String workspaceName = txtWorkspaceName.getText().toString();
                String oldWorkspaceName = "";

                String descriptionTxt = txtDescription.getText().toString();
                String tags = "";
                Boolean isPublic = switchPrivate.isChecked();
                if (isPublic) {
                    tags = txtTags.getText().toString();
                }

                oldWorkspaceName = workspace.getName();
                workspace.setForeign(false);
                workspace.setPublic(isPublic);
                workspace.setQuota(quota);
                workspace.setName(workspaceName);
                workspace.setPublicProfile(descriptionTxt);
                workspace.setTags(tags);

                LocalWorkspaceDatasource workspaceDatasource = new LocalWorkspaceDatasource(context);
                workspaceDatasource.open();
                try {
                    workspaceDatasource.updateWorkspace(workspace);
                    workspaceDatasource.removeWorkspaceUsers(workspace);
                    File oldWorkspaceDir = context.getDir(oldWorkspaceName, MODE_PRIVATE);
                    File workspaceDir = context.getDir(workspaceName, MODE_PRIVATE);
                    oldWorkspaceDir.renameTo(workspaceDir.getAbsoluteFile());
                } catch (WorkspaceNotFoundException e) {
                    Toast.makeText(context, "Error! Try again.", Toast.LENGTH_LONG).show();
                    Log.d("WORKSPACE", e.getMessage());
                } finally {
                    workspaceDatasource.close();
                }

//                String clientsString = txtClients.getText().toString();
//                String[] clientsList = clientsString.split(", ");
//                for (String client : clientsList) {
//                    User user = new User(-1, client, "User", "password", 0);
//                    UserDatasource userDatasource = new UserDatasource(context);
//                    userDatasource.open();
//                    try {
//                        if (!userDatasource.isUser(user)) {
//                            user = userDatasource.newUser(user);
//                        } else {
//                            user = userDatasource.getUserDetail(user);
//                        }
//                        ;
//                    } catch (UserNotFoundException e) {
//                        Log.d("USER", "Add User DB error.");
//                    } finally {
//                        userDatasource.close();
//                    }
//                    workspaceDatasource.open();
//                    workspaceDatasource.addUserToWorkspace(workspace, user);
//                    workspaceDatasource.close();
//                }
                Intent workspaceIntent = new Intent(context, WorkspaceActivity.class);
                workspaceIntent.putExtra("WORKSPACE", workspace);
                finish();
                startActivity(workspaceIntent);
            }
        } else {
            Toast.makeText(context, "Quota needs to be bigger than 0(zero)", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(String.format(getResources().getString(R.string.dialog_edit_workspace_msg), workspace.getName())).setTitle(R.string.dialog_edit_workspace_title);
        builder.setPositiveButton(R.string.dialog_confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                saveWorkspace();
                Intent workspaceIntent = new Intent(context, WorkspaceActivity.class);
                workspaceIntent.putExtra("WORKSPACE", workspace);
                finish();
                startActivity(workspaceIntent);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                Intent workspaceIntent = new Intent(context, WorkspaceActivity.class);
                workspaceIntent.putExtra("WORKSPACE", workspace);
                finish();
                startActivity(workspaceIntent);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
