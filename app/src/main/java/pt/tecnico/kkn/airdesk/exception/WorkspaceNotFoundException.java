package pt.tecnico.kkn.airdesk.exception;

/**
 * Exception throw when workspace not found
 */
public class WorkspaceNotFoundException extends Exception {
}
