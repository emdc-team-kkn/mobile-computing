package pt.tecnico.kkn.airdesk.frontend;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonRectangle;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.UnknownHostException;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.File;
import pt.tecnico.kkn.airdesk.datasource.LocalFileDatasource;
import pt.tecnico.kkn.airdesk.datasource.Workspace;

public class ForeignReadFileActivity extends ActionBarActivity {
    private TextView lblContent;
    private ButtonRectangle btnEdit;
    private ButtonRectangle btnDelete;
    private Workspace workspace;
    private File file;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foreign_read_file);

        Intent intent = getIntent();
        workspace = intent.getParcelableExtra("WORKSPACE");
        file = intent.getParcelableExtra("FILE");

        setTitle(file.getFilename());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lblContent = (TextView)findViewById(R.id.lblReadContent);
        btnEdit = (ButtonRectangle)findViewById(R.id.btnEdit);
        btnDelete = (ButtonRectangle)findViewById(R.id.btnDelete);

        new ReadFileCommTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, workspace.getIpAddress(), file.toJSONObject().toString(), workspace.toJSONObject().toString());

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editFile();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteFile();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_read_file, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                editFile();

                return true;
            case R.id.action_discard:
                deleteFile();

                return true;
            case android.R.id.home:
                Intent workspaceIntent = new Intent(context, ForeignWorkspaceActivity.class);
                workspaceIntent.putExtra("WORKSPACE", workspace);
                finish();
                startActivity(workspaceIntent);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void editFile() {
        file.setLockedBy(1);
        new LockFileCommTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, workspace.getIpAddress(), file.toJSONObject().toString());
    }

    private void deleteFile() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(String.format(getResources().getString(R.string.dialog_delete_file_msg), file.getFilename())).setTitle(R.string.dialog_delete_file_title);
        builder.setPositiveButton(R.string.dialog_confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                new DeleteFileCommTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, workspace.getIpAddress(), file.toJSONObject().toString(), workspace.toJSONObject().toString());
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        Intent workspaceIntent = new Intent(context, ForeignWorkspaceActivity.class);
        workspaceIntent.putExtra("WORKSPACE", workspace);
        finish();
        startActivity(workspaceIntent);
    }

    public class ReadFileCommTask extends AsyncTask<String, String, Void> {
        private static final String TAG = "ReadFileTask";
        private static final String REQUEST = "readfile";

        @Override
        protected Void doInBackground(String... params) {
            Log.d(TAG, "Client Started");
            String ipAddress = params[0];
            String jsonString = params[1];
            String workspaceString = params[2];
            String result;

            SharedPreferences preferences = getSharedPreferences("AirDeskPreferences", MODE_PRIVATE);
            String email = preferences.getString("EMAIL", null);
            String password = preferences.getString("PASSWORD", null);

            try {
                Log.d(TAG, "To Server: " + ipAddress + " " + getString(R.string.port));
                SimWifiP2pSocket clientSocket = new SimWifiP2pSocket(ipAddress, Integer.parseInt(getString(R.string.port)));

                PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);

                Log.d(TAG, "Write Started");
                writer.println(REQUEST);

                Log.d(TAG, "Write Email: " + email);
                writer.println(email);

                Log.d(TAG, "Write Password: " + password);
                writer.println(password);

                Log.d(TAG, "Write Workspace: " + workspaceString);
                writer.println(workspaceString);

                Log.d(TAG, "Write File: " + jsonString);
                writer.println(jsonString);

                Log.d(TAG, "Write Complete");

                BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                Log.d(TAG, "Read Started");
                result = br.readLine();
                publishProgress(result);

                Log.d(TAG, "Read Finished");
                clientSocket.close();
            } catch (UnknownHostException e) {
                Toast.makeText(context, "Workspace owner unavailable, try again later", Toast.LENGTH_SHORT).show();

                Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(workspacesIntent);
            } catch (IOException e) {
                Toast.makeText(context, "Workspace owner unavailable, try again later", Toast.LENGTH_SHORT).show();

                Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(workspacesIntent);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            String res = values[0];
            Log.d(TAG, res);

            if (res.equals("json_error")) {
                Toast.makeText(context, "Bad message format, try again", Toast.LENGTH_SHORT).show();
            } else if (res.equals("User not found")){
                Toast.makeText(context, "Error: User not found", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    JSONObject result = new JSONObject(res);
                    String type = result.getString("type");
                    String message = result.getString("content");
                    Log.d(TAG, type + ": " + message);
                    if (type.equals("error")) {
                        Toast.makeText(context, type.toUpperCase() + ": " + message, Toast.LENGTH_SHORT).show();
                    } else if (type.equals("file_content")) {
                        file.setContent(message);
                        lblContent.setText(message);
                    }

                } catch (JSONException e) {
                    Toast.makeText(context, "Bad message format, try again", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public class LockFileCommTask extends AsyncTask<String, String, Void> {
        private static final String TAG = "LockFileTask";
        private static final String REQUEST = "lockfile";

        @Override
        protected Void doInBackground(String... params) {
            Log.d(TAG, "Client Started");
            String ipAddress = params[0];
            String jsonString = params[1];
            String result;

            SharedPreferences preferences = getSharedPreferences("AirDeskPreferences", MODE_PRIVATE);
            String email = preferences.getString("EMAIL", null);
            String password = preferences.getString("PASSWORD", null);

            try {
                Log.d(TAG, "To Server: " + ipAddress + " " + getString(R.string.port));
                SimWifiP2pSocket clientSocket = new SimWifiP2pSocket(ipAddress, Integer.parseInt(getString(R.string.port)));

                PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);

                Log.d(TAG, "Write Started");
                Log.d(TAG, "Request: " + REQUEST);
                writer.println(REQUEST);

                Log.d(TAG, "Write Email: " + email);
                writer.println(email);

                Log.d(TAG, "Write Password: " + password);
                writer.println(password);

                Log.d(TAG, "Write File: " + jsonString);
                writer.println(jsonString);

                Log.d(TAG, "Write Complete");

                BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                Log.d(TAG, "Read Started");
                result = br.readLine();
                publishProgress(result);

                Log.d(TAG, "Read Finished");
                clientSocket.close();
            } catch (UnknownHostException e) {
                Toast.makeText(context, "Workspace owner unavailable, try again later", Toast.LENGTH_SHORT).show();

                Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(workspacesIntent);
            } catch (IOException e) {
                Toast.makeText(context, "Workspace owner unavailable, try again later", Toast.LENGTH_SHORT).show();

                Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(workspacesIntent);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            String res = values[0];

            if (res.equals("json_error")) {
                Toast.makeText(context, "Bad message format, try again", Toast.LENGTH_SHORT).show();
            } else if (res.equals("User not found")){
                Toast.makeText(context, "Error: User not found", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    JSONObject result = new JSONObject(res);
                    String type = result.getString("type");
                    String message = result.getString("message");

                    if (type.equals("error")) {
                        Toast.makeText(context, type.toUpperCase() + ": " + message, Toast.LENGTH_SHORT).show();
                    } else if (type.equals("success")) {
                        Intent editFileIntent = new Intent(context, ForeignEditFileActivity.class);
                        editFileIntent.putExtra("WORKSPACE", workspace);
                        editFileIntent.putExtra("FILE", file);
                        startActivity(editFileIntent);
                    }

                } catch (JSONException e) {
                    Toast.makeText(context, "Bad message format, try again", Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    public class DeleteFileCommTask extends AsyncTask<String, String, Void> {
        private static final String TAG = "DeleteFileTask";
        private static final String REQUEST = "deletefile";

        @Override
        protected Void doInBackground(String... params) {
            Log.d(TAG, "Client Started");
            String ipAddress = params[0];
            String jsonString = params[1];
            String workspaceString = params[2];
            String result;

            SharedPreferences preferences = getSharedPreferences("AirDeskPreferences", MODE_PRIVATE);
            String email = preferences.getString("EMAIL", null);
            String password = preferences.getString("PASSWORD", null);

            try {
                Log.d(TAG, "To Server: " + ipAddress + " " + getString(R.string.port));
                SimWifiP2pSocket clientSocket = new SimWifiP2pSocket(ipAddress, Integer.parseInt(getString(R.string.port)));

                PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);

                Log.d(TAG, "Write Started");
                Log.d(TAG, "Request: " + REQUEST);
                writer.println(REQUEST);

                Log.d(TAG, "Write Email: " + email);
                writer.println(email);

                Log.d(TAG, "Write Password: " + password);
                writer.println(password);

                Log.d(TAG, "Write Workspace: " + workspaceString);
                writer.println(workspaceString);

                Log.d(TAG, "Write File: " + jsonString);
                writer.println(jsonString);

                Log.d(TAG, "Write Complete");

                BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                Log.d(TAG, "Read Started");
                result = br.readLine();
                publishProgress(result);

                Log.d(TAG, "Read Finished");
                clientSocket.close();
            } catch (UnknownHostException e) {
                Toast.makeText(context, "Workspace owner unavailable, try again later", Toast.LENGTH_SHORT).show();

                Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(workspacesIntent);
            } catch (IOException e) {
                Toast.makeText(context, "Workspace owner unavailable, try again later", Toast.LENGTH_SHORT).show();

                Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(workspacesIntent);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            String res = values[0];
            Log.d(TAG, res);

            if (res.equals("json_error")) {
                Toast.makeText(context, "Bad message format, try again", Toast.LENGTH_SHORT).show();
            } else if (res.equals("User not found")){
                Toast.makeText(context, "Error: User not found", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    JSONObject result = new JSONObject(res);
                    String type = result.getString("type");
                    String message = result.getString("message");

                    Toast.makeText(context, type.toUpperCase() + ": " + message, Toast.LENGTH_SHORT).show();

                    if (type.equals("success")) {
                        Intent workspaceIntent = new Intent(context, ForeignWorkspaceActivity.class);
                        workspaceIntent.putExtra("WORKSPACE", workspace);
                        finish();
                        startActivity(workspaceIntent);
                    }

                } catch (JSONException e) {
                    Toast.makeText(context, "Bad message format, try again", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
