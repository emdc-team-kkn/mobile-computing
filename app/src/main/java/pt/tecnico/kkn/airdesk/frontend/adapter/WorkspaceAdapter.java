package pt.tecnico.kkn.airdesk.frontend.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.Workspace;

/**
 * Created by nikola on 16.3.15.
 */
public class WorkspaceAdapter extends ArrayAdapter<Workspace> {
    private Context context;
    private int layoutResourceId;
    private Workspace data[] = null;

    public WorkspaceAdapter(Context context, int layoutResourceId, Workspace[] data) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        WorkspaceHolder holder;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new WorkspaceHolder();
            holder.txtName = (TextView)row.findViewById(R.id.txtName);

            row.setTag(holder);
        }
        else
        {
            holder = (WorkspaceHolder)row.getTag();
        }

        Workspace workspace = data[position];
        holder.txtName.setText(workspace.getName());

        return row;
    }

    static class WorkspaceHolder
    {
        TextView txtName;
    }
}
