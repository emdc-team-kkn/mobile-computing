package pt.tecnico.kkn.airdesk.wifidirect;

import android.app.Notification;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Messenger;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import pt.inesc.termite.wifidirect.SimWifiP2pBroadcast;
import pt.inesc.termite.wifidirect.SimWifiP2pManager;
import pt.inesc.termite.wifidirect.service.SimWifiP2pService;
import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocketManager;
import pt.tecnico.kkn.airdesk.R;

/**
 * Server service run in the background
 */
public class AirdeskService extends Service {

    private final static String TAG = "AirdeskService";
    private static final int ONGOING_NOTIFICATION_ID = 101;
    private Messenger mService = null;
    private SimWifiP2pManager mManager = null;
    private SimWifiP2pManager.Channel mChannel = null;
    private AirdeskServer server;
    private AirdeskGroupInfoListener groupInfoListener;

    public AirdeskService() {

    }

    @Override
    public void onCreate() {
        Log.d(TAG, "Service started");

        // initialize the WDSim API
        SimWifiP2pSocketManager.Init(this);

        // register broadcast receiver
        IntentFilter filter = new IntentFilter();
        filter.addAction(SimWifiP2pBroadcast.WIFI_P2P_STATE_CHANGED_ACTION);
        filter.addAction(SimWifiP2pBroadcast.WIFI_P2P_PEERS_CHANGED_ACTION);
        filter.addAction(SimWifiP2pBroadcast.WIFI_P2P_NETWORK_MEMBERSHIP_CHANGED_ACTION);
        filter.addAction(SimWifiP2pBroadcast.WIFI_P2P_GROUP_OWNERSHIP_CHANGED_ACTION);
        WifiP2pBroadcastReceiver broadcastReceiver = new WifiP2pBroadcastReceiver();

        this.registerReceiver(broadcastReceiver, filter);

        Intent simWifiIntent = new Intent(this, SimWifiP2pService.class);
        bindService(simWifiIntent, mConnection, Context.BIND_AUTO_CREATE);
        groupInfoListener = new AirdeskGroupInfoListener(this);
        server = new AirdeskServer(Integer.valueOf(getString(R.string.port)), this);
        new Thread(server).start();

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_stat_ad)
                        .setContentTitle("Airdesk")
                        .setContentText("Service running");
        Notification notification = notificationBuilder.build();
        startForeground(ONGOING_NOTIFICATION_ID, notification);
    }

    @Override
    public void onDestroy() {
        server.stop();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();
            if (action != null) {
                switch(action) {
                    case WifiP2pBroadcastReceiver.UPDATE_GROUP:
                        updateGroup();
                        break;
                    default:
                        break;
                }
            }
        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void updateGroup() {
        mManager.requestGroupInfo(mChannel, groupInfoListener);
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);
            mManager = new SimWifiP2pManager(mService);
            mChannel = mManager.initialize(getApplication(), getMainLooper(), null);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
            mManager = null;
            mChannel = null;
        }
    };
}
