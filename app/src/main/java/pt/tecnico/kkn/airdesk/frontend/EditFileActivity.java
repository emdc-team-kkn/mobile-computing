package pt.tecnico.kkn.airdesk.frontend;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonRectangle;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.LocalFileDatasource;
import pt.tecnico.kkn.airdesk.datasource.Workspace;
import pt.tecnico.kkn.airdesk.helper.Validation;

public class EditFileActivity extends ActionBarActivity {
    private ButtonRectangle btnSaveFile;
    private EditText txtFilename;
    private EditText txtContent;

    private Workspace workspace;
    private pt.tecnico.kkn.airdesk.datasource.File file;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_file);

        setTitle(R.string.title_activity_file_edit);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        workspace = intent.getParcelableExtra("WORKSPACE");
        file = intent.getParcelableExtra("FILE");

        txtFilename = (EditText)findViewById(R.id.txtFilename);
        txtContent = (EditText)findViewById(R.id.txtContent);

        txtFilename.setText(file.getFilename());

        java.io.File openFile = new java.io.File(context.getDir((workspace.getName() + Long.toString(workspace.getId())), Context.MODE_PRIVATE), file.getFilename() + ".txt");
        try {
            FileInputStream inputStream = new FileInputStream(openFile);
            byte[] buffer = new byte[file.getFileSize()];

            inputStream.read(buffer, 0, file.getFileSize());

            String content = new String(buffer);
            txtContent.setText(content);

            inputStream.close();
        } catch (FileNotFoundException e) {
            Toast.makeText(context, "Error reading from file", Toast.LENGTH_LONG).show();
            Log.d("FILE", e.getMessage());
        } catch (IOException e) {
            Toast.makeText(context, "Error reading from file", Toast.LENGTH_LONG).show();
            Log.d("FILE", e.getMessage());
        }

        btnSaveFile = (ButtonRectangle)findViewById(R.id.btnSave);

        btnSaveFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveFile();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_file, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_discard:
                LocalFileDatasource fileDatasource = new LocalFileDatasource(context, (int) workspace.getId());
                fileDatasource.open();

                try {
                    fileDatasource.deleteFile(file);
                    fileDatasource.unlockFile(file);

                    java.io.File delFile = new java.io.File(context.getDir((workspace.getName() + Long.toString(workspace.getId())), Context.MODE_PRIVATE), file.getFilename() + ".txt");
                    delFile.delete();

                    Intent workspaceIntent = new Intent(context, WorkspaceActivity.class);
                    workspaceIntent.putExtra("WORKSPACE", workspace);
                    startActivity(workspaceIntent);
                } catch (FileNotFoundException e) {
                    Toast.makeText(context, "Could not delete file: File not found", Toast.LENGTH_LONG).show();
                    Log.d("FILE", e.getMessage());
                } finally {
                    fileDatasource.close();
                }

                return true;
            case android.R.id.home:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(String.format(getResources().getString(R.string.dialog_edit_file_msg), file.getFilename())).setTitle(R.string.dialog_edit_file_title);
                builder.setPositiveButton(R.string.dialog_confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        saveFile();

                        Intent readFileIntent = new Intent(context, ReadFileActivity.class);
                        readFileIntent.putExtra("WORKSPACE", workspace);
                        readFileIntent.putExtra("FILE", file);
                        finish();
                        startActivity(readFileIntent);
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        Intent readFileIntent = new Intent(context, ReadFileActivity.class);
                        readFileIntent.putExtra("WORKSPACE", workspace);
                        readFileIntent.putExtra("FILE", file);
                        finish();
                        startActivity(readFileIntent);

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void saveFile() {
        //Write file to FS and save to DB
        String filename = txtFilename.getText().toString();
        String newContent = txtContent.getText().toString();

        if (Validation.hasText(txtFilename, "The filename is required")) {
            if (workspace.getQuota() > workspace.getSize() - file.getFileSize() + newContent.getBytes().length) {
                String oldFilename = file.getFilename();
                file.setFilename(filename);
                file.setFileSize(newContent.getBytes().length);

                LocalFileDatasource fileDatasource = new LocalFileDatasource(context, (int) workspace.getId());
                fileDatasource.open();
                try {
                    fileDatasource.updateFile(file);
                } catch (FileNotFoundException e) {
                    Toast.makeText(context, "Error while saving file information", Toast.LENGTH_LONG).show();
                    Log.d("FILE", "Database error");
                } finally {
                    fileDatasource.close();
                }

                File fileToWrite = new File(context.getDir((workspace.getName() + Long.toString(workspace.getId())), Context.MODE_PRIVATE), oldFilename + ".txt");

                if (!oldFilename.equals(filename)) {
                    File newFile = new File(context.getDir((workspace.getName() + Long.toString(workspace.getId())), Context.MODE_PRIVATE), filename + ".txt");
                    fileToWrite.renameTo(newFile);
                    fileToWrite = newFile;
                }
                Log.d("TEST", fileToWrite.getAbsolutePath());
                try {
                    FileOutputStream outputStream = new FileOutputStream(fileToWrite.getAbsolutePath());

                    outputStream.write(newContent.getBytes());
                    outputStream.close();
                } catch (FileNotFoundException e) {
                    Toast.makeText(context, "Error when writing to file.", Toast.LENGTH_LONG).show();
                    Log.d("FILE", e.getMessage());
                } catch (IOException e) {
                    Toast.makeText(context, "Error while writing to file.", Toast.LENGTH_LONG).show();
                    Log.d("FILE", e.getMessage());
                }


                fileDatasource.open();
                try {
                    fileDatasource.unlockFile(file);
                } catch (FileNotFoundException e) {
                    Toast.makeText(context, "Error while saving file information", Toast.LENGTH_LONG).show();
                    Log.d("FILE", "Error on File Unlock in DB");
                } finally {
                    fileDatasource.close();
                }

                Intent readFileIntent = new Intent(context, ReadFileActivity.class);
                readFileIntent.putExtra("WORKSPACE", workspace);
                readFileIntent.putExtra("FILE", file);
                startActivity(readFileIntent);
            } else {
                Toast.makeText(context, "The file is too big for the workspace", Toast.LENGTH_LONG).show();
                Log.d("FILE", "Workspace exceeds quota.");
            }
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(String.format(getResources().getString(R.string.dialog_edit_file_msg), file.getFilename())).setTitle(R.string.dialog_edit_file_title);
        builder.setPositiveButton(R.string.dialog_confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                saveFile();

                Intent readFileIntent = new Intent(context, ReadFileActivity.class);
                readFileIntent.putExtra("WORKSPACE", workspace);
                readFileIntent.putExtra("FILE", file);
                finish();
                startActivity(readFileIntent);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                Intent readFileIntent = new Intent(context, ReadFileActivity.class);
                readFileIntent.putExtra("WORKSPACE", workspace);
                readFileIntent.putExtra("FILE", file);
                finish();
                startActivity(readFileIntent);

            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
