package pt.tecnico.kkn.airdesk.db.table;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Helper class for the users table
 * Columns: _id, email, nickname
 */
public class UsersTableHelper {

    private static final String TAG = "UsersTableHelper";

    public static final String TABLE_NAME = "users";
    public static final String COL_ID = "_id";
    public static final String COL_EMAIL = "email";
    public static final String COL_NICKNAME = "nickname";
    public static final String COL_PASSWORD = "password";
    public static final String COL_ROLE = "role";

    /**
     * Version 1 table
     */
    private static final String createTableV1 = "create table " + TABLE_NAME + " ( " +
        COL_ID + " integer primary key autoincrement, " +
        COL_EMAIL + " text, " +
        COL_NICKNAME + " text, " +
        COL_PASSWORD + " text, " +
        COL_ROLE + " integer " +
        ")";

    public static void onCreate(SQLiteDatabase db) {
        logQuery(createTableV1);
        db.execSQL(createTableV1);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {
            default:
                throw new IllegalStateException("onUpgrade() called with unknown version" + newVersion);
        }
    }

    private static void logQuery(String query) {
        Log.d(TAG, "Query executed: " + query);
    }
}
