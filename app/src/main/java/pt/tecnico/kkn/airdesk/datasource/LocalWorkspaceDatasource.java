package pt.tecnico.kkn.airdesk.datasource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pt.tecnico.kkn.airdesk.db.AirDeskDbOpenHelper;
import pt.tecnico.kkn.airdesk.db.table.AclsTableHelper;
import pt.tecnico.kkn.airdesk.db.table.FilesTableHelper;
import pt.tecnico.kkn.airdesk.db.table.UsersTableHelper;
import pt.tecnico.kkn.airdesk.db.table.WorkspacesTableHelper;
import pt.tecnico.kkn.airdesk.exception.WorkspaceNotFoundException;

/**
 * This class implement the data source interface for local workspace
 */
public class LocalWorkspaceDatasource implements IWorkspaceDatasource {

    private SQLiteDatabase db;
    private AirDeskDbOpenHelper dbOpenHelper;
    private SQLiteStatement newWorkspaceStatement;
    private SQLiteStatement updateWorkspaceStatement;
    private SQLiteStatement deleteWorkspaceStatement;
    private SQLiteStatement addUserToWorkspaceStatement;
    private SQLiteStatement removeUserFromWorkspaceStatement;

    public LocalWorkspaceDatasource(Context context) {
        dbOpenHelper = new AirDeskDbOpenHelper(context);
    }

    @Override
    public List<Workspace> getWorkspacesList() {
        String query = "select * from " + WorkspacesTableHelper.TABLE_NAME + " where " + WorkspacesTableHelper.COL_DELETED_AT + " is null";
        Cursor c = db.rawQuery(query, null);
        List<Workspace> ret = new ArrayList<>();
        while(c.moveToNext()) {
            long id = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_ID));
            String name = c.getString(c.getColumnIndex(WorkspacesTableHelper.COL_NAME));
            Integer quota = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_QUOTA));
            Integer size = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_SIZE));
            Boolean isPublic = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_IS_PUBLIC)) == 1;
            Boolean isForeign = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_IS_FOREIGN)) == 1;
            String publicProfile = c.getString(c.getColumnIndex(WorkspacesTableHelper.COL_PUBLIC_PROFILE));
            String tags = c.getString(c.getColumnIndex(WorkspacesTableHelper.COL_TAGS));

            ret.add(new Workspace(id, false, isPublic, quota, size, name, publicProfile, tags));
        }
        c.close();

        return ret;
    }

    public List<Workspace> searchWorkspaces(List<String > keywordList) {

        String whereString = "";
        for (String keyword : keywordList) {
            whereString += WorkspacesTableHelper.COL_TAGS + " like '%" + keyword + "%' OR ";
        }
        whereString = whereString.substring(0, whereString.length() - 4);

        String query = "select * from " + WorkspacesTableHelper.TABLE_NAME + " where " + whereString + " and " + WorkspacesTableHelper.COL_IS_PUBLIC + " = 1 and " + WorkspacesTableHelper.COL_DELETED_AT + " is null";
        Log.d("SearchString", query);

        Cursor c = db.rawQuery(query, null);
        List<Workspace> ret = new ArrayList<>();
        while(c.moveToNext()) {
            long id = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_ID));
            String name = c.getString(c.getColumnIndex(WorkspacesTableHelper.COL_NAME));
            Integer quota = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_QUOTA));
            Integer size = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_SIZE));
            Boolean isPublic = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_IS_PUBLIC)) == 1;
            Boolean isForeign = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_IS_FOREIGN)) == 1;
            String publicProfile = c.getString(c.getColumnIndex(WorkspacesTableHelper.COL_PUBLIC_PROFILE));
            String tags = c.getString(c.getColumnIndex(WorkspacesTableHelper.COL_TAGS));

            ret.add(new Workspace(id, isForeign, isPublic, quota, size, name, publicProfile, tags));
        }
        c.close();

        return ret;
    }

    @Override
    public Workspace getWorkspaceDetail(Workspace w) throws WorkspaceNotFoundException {
        String query = "select * from " +
            WorkspacesTableHelper.TABLE_NAME + " where " +
            WorkspacesTableHelper.COL_ID + " = ?";
        Cursor c = db.rawQuery(query, new String[]{String.valueOf(w.getId())});
        if (c.getCount() == 0) {
            throw new WorkspaceNotFoundException();
        }
        Workspace ret = null;
        while(c.moveToNext()) {
            long id = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_ID));
            String name = c.getString(c.getColumnIndex(WorkspacesTableHelper.COL_NAME));
            Integer quota = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_QUOTA));
            Integer size = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_SIZE));
            Boolean isPublic = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_IS_PUBLIC)) == 1;
            Boolean isForeign = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_IS_FOREIGN)) == 1;
            String publicProfile = c.getString(c.getColumnIndex(WorkspacesTableHelper.COL_PUBLIC_PROFILE));
            String tags = c.getString(c.getColumnIndex(WorkspacesTableHelper.COL_TAGS));
            c.close();

            return new Workspace(id, isForeign, isPublic, quota, size, name, publicProfile, tags);
        }
        c.close();
        return ret;
    }

    @Override
    public Workspace newWorkspace(Workspace w) throws WorkspaceNotFoundException {
        if (newWorkspaceStatement == null) {
            newWorkspaceStatement = db.compileStatement(
                "insert into " + WorkspacesTableHelper.TABLE_NAME + "( " +
                    WorkspacesTableHelper.COL_NAME + " , " +
                    WorkspacesTableHelper.COL_QUOTA + " , " +
                    WorkspacesTableHelper.COL_SIZE + " , " +
                    WorkspacesTableHelper.COL_IS_PUBLIC + " , " +
                    WorkspacesTableHelper.COL_IS_FOREIGN + " , " +
                    WorkspacesTableHelper.COL_PUBLIC_PROFILE + " , " +
                    WorkspacesTableHelper.COL_TAGS + " , " +
                    WorkspacesTableHelper.COL_CREATED_AT + ", " +
                    WorkspacesTableHelper.COL_UPDATED_AT +
                    " ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
            );
        }
        bindWorkspaceValue(newWorkspaceStatement, w);
        long currentTime = getCurrentTime();
        newWorkspaceStatement.bindLong(8, currentTime);
        newWorkspaceStatement.bindLong(9, currentTime);
        long id = newWorkspaceStatement.executeInsert();
        if (id == -1) {
            throw new WorkspaceNotFoundException();
        }
        w.setId(id);
        return w;
    }

    @Override
    public boolean updateWorkspace(Workspace w) throws WorkspaceNotFoundException {
        if (updateWorkspaceStatement == null) {
            updateWorkspaceStatement = db.compileStatement(
                "update " + WorkspacesTableHelper.TABLE_NAME + " set " +
                    WorkspacesTableHelper.COL_NAME + " = ?, " +
                    WorkspacesTableHelper.COL_QUOTA + " = ?, " +
                    WorkspacesTableHelper.COL_SIZE + " = ?, " +
                    WorkspacesTableHelper.COL_IS_PUBLIC + " = ?, " +
                    WorkspacesTableHelper.COL_IS_FOREIGN + " = ?, " +
                    WorkspacesTableHelper.COL_PUBLIC_PROFILE + " = ?, " +
                    WorkspacesTableHelper.COL_TAGS + " = ?, " +
                    WorkspacesTableHelper.COL_UPDATED_AT + " = ? " +
                    "where " + WorkspacesTableHelper.COL_ID + " = ?"
            );
        }
        bindWorkspaceValue(updateWorkspaceStatement, w);
        long currentTime = getCurrentTime();
        updateWorkspaceStatement.bindLong(8, currentTime);
        updateWorkspaceStatement.bindLong(9, w.getId());
        int updatedRows = updateWorkspaceStatement.executeUpdateDelete();
        if (updatedRows == 0) {
            throw new WorkspaceNotFoundException();
        }
        return updatedRows == 1;
    }

    @Override
    public boolean deleteWorkspace(Workspace w) throws WorkspaceNotFoundException {
        if (deleteWorkspaceStatement == null) {
            deleteWorkspaceStatement = db.compileStatement(
                "update " + WorkspacesTableHelper.TABLE_NAME + " set " +
                    WorkspacesTableHelper.COL_DELETED_AT + " = ? " +
                    "where " + WorkspacesTableHelper.COL_ID + " = ?"
            );
        }
        long currentTime = getCurrentTime();
        deleteWorkspaceStatement.bindLong(1, currentTime);
        deleteWorkspaceStatement.bindLong(2, w.getId());
        int updatedRow = deleteWorkspaceStatement.executeUpdateDelete();
        if (updatedRow == 0) {
            throw new WorkspaceNotFoundException();
        }
        return updatedRow == 1;
    }

    @Override
    public boolean undeleteWorkspace(Workspace w) throws WorkspaceNotFoundException {
        if (deleteWorkspaceStatement == null) {
            deleteWorkspaceStatement = db.compileStatement(
                "update " + WorkspacesTableHelper.TABLE_NAME + " set " +
                    WorkspacesTableHelper.COL_DELETED_AT + " = ? " +
                    "where " + WorkspacesTableHelper.COL_ID + " = ?"
            );
        }
        deleteWorkspaceStatement.bindNull(1);
        deleteWorkspaceStatement.bindLong(2, w.getId());
        int updatedRow = deleteWorkspaceStatement.executeUpdateDelete();
        if (updatedRow == 0) {
            throw new WorkspaceNotFoundException();
        }
        return updatedRow == 1;
    }

    @Override
    public boolean addUserToWorkspace(Workspace w, User u) {
        if (addUserToWorkspaceStatement == null) {
            addUserToWorkspaceStatement = db.compileStatement(
                "insert into " + AclsTableHelper.TABLE_NAME + " ( " +
                    AclsTableHelper.COL_USER_ID + " , " +
                        AclsTableHelper.COL_WORKSPACE_ID + " , " +
                        AclsTableHelper.COL_USER_ROLE +
                    " ) VALUES (?, ?, ?)"
            );
        }
        addUserToWorkspaceStatement.bindLong(1, u.getId());
        addUserToWorkspaceStatement.bindLong(2, w.getId());
        addUserToWorkspaceStatement.bindLong(3, u.getRole());

        long id = addUserToWorkspaceStatement.executeInsert();
        return id != -1;
    }

    public int updateUserRole(Workspace w, User u) {
        ContentValues args = new ContentValues();
        args.put(AclsTableHelper.COL_USER_ROLE, u.getRole());

        int result = db.update(AclsTableHelper.TABLE_NAME,
                args,
                AclsTableHelper.COL_USER_ID + " = ? AND " + AclsTableHelper.COL_WORKSPACE_ID + " = ?",
                new String[]{ Long.toString(u.getId()), Long.toString(w.getId()) }
        );

        return result;
    }

    @Override
    public boolean removeUserFromWorkspace(Workspace w, User u) {
        int updatedRow = db.delete(AclsTableHelper.TABLE_NAME, AclsTableHelper.COL_USER_ID + " = ? AND " + AclsTableHelper.COL_WORKSPACE_ID + " = ?", new String[]{Long.toString(u.getId()), Long.toString(w.getId())});

        return updatedRow > 0;
    }

    public boolean hasUser(Workspace w, User u) {
        String userQuery = "select * from " + UsersTableHelper.TABLE_NAME + " inner join " + AclsTableHelper.TABLE_NAME + " on " + UsersTableHelper.TABLE_NAME + "." + UsersTableHelper.COL_ID + " = " + AclsTableHelper.TABLE_NAME + "." + AclsTableHelper.COL_USER_ID + " where " + AclsTableHelper.COL_WORKSPACE_ID + " = ? and " + UsersTableHelper.COL_EMAIL + " = ?";

        String[] args = { String.valueOf(w.getId()), u.getEmail() };
        Cursor c = db.rawQuery(userQuery, args);
        int count = c.getCount();

        c.close();
        return  count > 0;
    }

    public int userRole(Workspace w, User u) {
        String userQuery = "select * from " + AclsTableHelper.TABLE_NAME + " where " + AclsTableHelper.COL_WORKSPACE_ID + " = ? and " + AclsTableHelper.COL_USER_ID + " = ?";

        String[] args = { String.valueOf(w.getId()), String.valueOf(u.getId()) };
        Cursor c = db.rawQuery(userQuery, args);

        int role = 0;
        while(c.moveToNext()) {
            role = c.getInt(c.getColumnIndex(AclsTableHelper.COL_USER_ROLE));
            c.close();

            return role;
        }
        c.close();

        return role;
    }

    public int removeWorkspaceUsers(Workspace w) {
        return db.delete(AclsTableHelper.TABLE_NAME, AclsTableHelper.COL_WORKSPACE_ID + " = ?", new String[] { Long.toString(w.getId()) });

    }

    public int removeWorkspaceFiles(Workspace w) {
        return db.delete(FilesTableHelper.TABLE_NAME, FilesTableHelper.COL_WORKSPACE_ID + " = ?", new String[] { Long.toString(w.getId()) });
    }

    @Override
    public boolean isUserAllowed(Workspace w, User u) {
        String checkQuery = "select * from " + AclsTableHelper.TABLE_NAME + " where " +
            AclsTableHelper.COL_USER_ID + " = ? " + " AND " +
            AclsTableHelper.COL_WORKSPACE_ID + " = ?";

        String[] args = { String.valueOf(u.getId()), String.valueOf(w.getId()) };
        Cursor c = db.rawQuery(checkQuery, args);
        int total = c.getCount();
        c.close();
        return total > 0;
    }

    @Override
    public List<User> getUserList(Workspace w) {
        String userQuery = "select * from " + UsersTableHelper.TABLE_NAME + " inner join " + AclsTableHelper.TABLE_NAME + " on " + UsersTableHelper.TABLE_NAME + "." + UsersTableHelper.COL_ID + " = " + AclsTableHelper.TABLE_NAME + "." + AclsTableHelper.COL_USER_ID + " where " + AclsTableHelper.COL_WORKSPACE_ID + " = ?";

        String[] args = { String.valueOf(w.getId()) };
        Cursor c = db.rawQuery(userQuery, args);

        List<User> ret = new ArrayList<>();
        while (c.moveToNext()) {
            long id = c.getInt(c.getColumnIndex(AclsTableHelper.COL_USER_ID));
            String email = c.getString(c.getColumnIndex(UsersTableHelper.COL_EMAIL));
            String nickname = c.getString(c.getColumnIndex(UsersTableHelper.COL_NICKNAME));
            String password = c.getString(c.getColumnIndex(UsersTableHelper.COL_PASSWORD));
            int role = c.getInt(c.getColumnIndex(AclsTableHelper.COL_USER_ROLE));

            ret.add(new User(id, email, nickname, password, role));
        }
        c.close();

        return ret;
    }

    @Override
    public void open() {
        db = dbOpenHelper.getWritableDatabase();
    }

    @Override
    public void close() {
        if (newWorkspaceStatement != null) {
            newWorkspaceStatement.close();
            newWorkspaceStatement = null;
        }
        if (updateWorkspaceStatement != null) {
            updateWorkspaceStatement.close();
            updateWorkspaceStatement = null;
        }
        if (deleteWorkspaceStatement != null) {
            deleteWorkspaceStatement.close();
            deleteWorkspaceStatement = null;
        }
        if (addUserToWorkspaceStatement != null) {
            addUserToWorkspaceStatement.close();
            addUserToWorkspaceStatement = null;
        }
        if (removeUserFromWorkspaceStatement != null) {
            removeUserFromWorkspaceStatement.close();
            removeUserFromWorkspaceStatement = null;
        }
        if (db != null) {
            db.close();
            db = null;
        }
    }

    /**
     * Helper function to bind workspace object to the update/insert statement
     * @param statement The statement
     * @param w The object to bind
     */
    private void bindWorkspaceValue(SQLiteStatement statement, Workspace w) {
        statement.bindString(1, w.getName());
        statement.bindLong(2, w.getQuota());
        statement.bindLong(3, w.getSize());
        statement.bindLong(4, w.isPublic() ? 1 : 0);
        statement.bindLong(5, w.isForeign() ? 1 : 0);
        statement.bindString(6, w.getPublicProfile());
        statement.bindString(7, w.getTags());
    }

    /**
     * Return the current time
     */
    private long getCurrentTime() {
        return new Date().getTime();
    }
}
