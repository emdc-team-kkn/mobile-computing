package pt.tecnico.kkn.airdesk.helper;

/**
 * Created by nikola on 19.3.15.
 */
public class Memory {
    private int value;
    private String type;

    public Memory(int value, String type) {
        this.value = value;
        this.type = type;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}