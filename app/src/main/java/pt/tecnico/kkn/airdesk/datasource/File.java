package pt.tecnico.kkn.airdesk.datasource;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * This class represent a file in our system
 */
public class File implements Parcelable {
    private long id;
    private int workspaceId;
    private String filename;
    private String ownerId;
    private String content;
    private int fileSize;
    private int createdAt;
    private int updatedAt;
    private int deletedAt;
    private int lockedBy;

    public File(int workspaceId, String filename, String content) {
        this.workspaceId = workspaceId;
        this.filename = filename;
        this.content = content;
        this.fileSize = content.getBytes().length;
    }

    public File(int id, int workspaceId, String filename) {
        this.id = id;
        this.workspaceId = workspaceId;
        this.filename = filename;
    }

    public File(int id, int workspaceId, String filename, String content, int fileSize, int createdAt, int updatedAt, int deletedAt, int lockedBy) {
        this.id = id;
        this.workspaceId = workspaceId;
        this.filename = filename;
        this.content = content;
        this.fileSize = fileSize;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
        this.lockedBy = lockedBy;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
        this.fileSize = content.getBytes().length;
    }

    public int getWorkspaceId() {
        return workspaceId;
    }

    public void setWorkspaceId(int workspaceId) {
        this.workspaceId = workspaceId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public int getCreatedAt() {
        return createdAt;
    }

    public int getUpdatedAt() {
        return updatedAt;
    }

    public int getDeletedAt() {
        return deletedAt;
    }

    public void setLockedBy(int lockedBy) {
        this.lockedBy = lockedBy;
    }

    public int getLockedBy() {
        return lockedBy;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public File(Parcel in) {
        this.id = in.readLong();
        this.workspaceId = in.readInt();
        this.filename = in.readString();
        this.ownerId = in.readString();
        this.content = in.readString();
        this.fileSize = in.readInt();
        this.createdAt = in.readInt();
        this.updatedAt = in.readInt();
        this.deletedAt = in.readInt();
        this.lockedBy = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeLong(id);
        out.writeInt(workspaceId);
        out.writeString(filename);
        out.writeString(ownerId);
        out.writeString(content);
        out.writeInt(fileSize);
        out.writeInt(createdAt);
        out.writeInt(updatedAt);
        out.writeInt(deletedAt);
        out.writeInt(lockedBy);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public File createFromParcel(Parcel in) {
            return new File(in);
        }

        public File[] newArray(int size) {
            return new File[size];
        }
    };

    public JSONObject toJSONObject() {
        JSONObject file = new JSONObject();

        try {
            file.put("id", String.valueOf(this.id));
            file.put("workspaceId", this.workspaceId);
            file.put("ownerId", String.valueOf(this.workspaceId));
            file.put("filename", this.filename);
            file.put("content", this.content);
            file.put("fileSize", String.valueOf(this.fileSize));
            file.put("createdAt", String.valueOf(this.createdAt));
            file.put("updatedAt", String.valueOf(this.updatedAt));
            file.put("deletedAt", String.valueOf(this.deletedAt));
            file.put("lockedBy", String.valueOf(this.lockedBy));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return file;
    }

    public static File fromJSONObject(JSONObject f) {
        try {
            long id = f.getLong("id");
            int workspaceId = f.getInt("workspaceId");
            String filename = f.getString("filename");
            String ownerId = f.getString("ownerId");
            String content = f.getString("content");
            int fileSize = f.getInt("fileSize");
            int createdAt = f.getInt("createdAt");
            int updatedAt = f.getInt("updatedAt");
            int deletedAt = f.getInt("deletedAt");
            int lockedBy = f.getInt("lockedBy");

            File file = new File((int)id, workspaceId, filename, content, fileSize, createdAt, updatedAt, deletedAt, lockedBy);
            file.setOwnerId(ownerId);

            return file;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
