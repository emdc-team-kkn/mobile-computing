package pt.tecnico.kkn.airdesk.db.table;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Helper class for the workspaces table
 * Columns: _id, is_foreign, quota, is_public, public_profile
 */
public class WorkspacesTableHelper {

    private static final String TAG = "WorkspacesTableHelper";

    public static final String TABLE_NAME = "workspaces";
    public static final String COL_ID = "_id";
    public static final String COL_NAME = "name";
    public static final String COL_QUOTA = "quota";
    public static final String COL_SIZE = "size";
    public static final String COL_IS_PUBLIC = "is_public";
    public static final String COL_IS_FOREIGN = "is_foreign";
    public static final String COL_PUBLIC_PROFILE = "public_profile";
    public static final String COL_TAGS = "tags";
    public static final String COL_CREATED_AT = "created_at";
    public static final String COL_UPDATED_AT = "updated_at";
    public static final String COL_DELETED_AT = "deleted_at";

    /**
     * Version 1 table
     */
    private static final String createTableV1 = "create table " + TABLE_NAME + " ( " +
        COL_ID + " integer primary key autoincrement, " +
        COL_NAME + " name text, " +
        COL_QUOTA + " integer, " +
        COL_SIZE + " integer, " +
        COL_IS_PUBLIC + " integer, " +
        COL_IS_FOREIGN + " integer, " +
        COL_PUBLIC_PROFILE + " text, " +
        COL_TAGS + " text, " +
        COL_CREATED_AT + " integer, " +
        COL_UPDATED_AT + " integer, " +
        COL_DELETED_AT + " integer " +
        ");";

    public static void onCreate(SQLiteDatabase db) {
        logQuery(createTableV1);
        db.execSQL(createTableV1);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {
            default:
                throw new IllegalStateException("onUpgrade() called with unknown version" + newVersion);
        }
    }

    private static void logQuery(String query) {
        Log.d(TAG, "Query executed: " + query);
    }
}
