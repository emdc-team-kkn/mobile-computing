package pt.tecnico.kkn.airdesk.frontend.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.File;

/**
 * Created by nikola on 16.3.15.
 */
public class FileAdapter extends ArrayAdapter<File> {
    private Context context;
    private int layoutResourceId;
    private File data[] = null;

    public FileAdapter(Context context, int layoutResourceId, File[] data) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        FileHolder holder;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new FileHolder();
            holder.txtName = (TextView)row.findViewById(R.id.txtName);

            row.setTag(holder);
        }
        else
        {
            holder = (FileHolder)row.getTag();
        }

        File file = data[position];
        holder.txtName.setText(file.getFilename());

        return row;
    }

    static class FileHolder
    {
        TextView txtName;
    }
}
