package pt.tecnico.kkn.airdesk.datasource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import pt.tecnico.kkn.airdesk.db.AirDeskDbOpenHelper;
import pt.tecnico.kkn.airdesk.db.table.AclsTableHelper;
import pt.tecnico.kkn.airdesk.db.table.UsersTableHelper;
import pt.tecnico.kkn.airdesk.db.table.WorkspacesTableHelper;
import pt.tecnico.kkn.airdesk.exception.UserNotFoundException;

/**
 * This class implement the user datasource
 */
public class UserDatasource implements IUserDatasource {

    private AirDeskDbOpenHelper dbOpenHelper;
    private SQLiteDatabase db = null;
    private SQLiteStatement newUserStatement = null;
    private SQLiteStatement updateUserStatement = null;
    private SQLiteStatement deleteUserStatement = null;

    public UserDatasource(Context context) {
        dbOpenHelper = new AirDeskDbOpenHelper(context);
    }

    @Override
    public List<User> getUsersList() {
        String query = "select * from " + UsersTableHelper.TABLE_NAME;
        Cursor c = db.rawQuery(query, null);
        List<User> ret = new ArrayList<>();
        while(c.moveToNext()) {
            long id = c.getInt(c.getColumnIndex(UsersTableHelper.COL_ID));
            String email = c.getString(c.getColumnIndex(UsersTableHelper.COL_EMAIL));
            String nickName = c.getString(c.getColumnIndex(UsersTableHelper.COL_NICKNAME));
            String password = c.getString(c.getColumnIndex(UsersTableHelper.COL_PASSWORD));
            int role = c.getInt(c.getColumnIndex(UsersTableHelper.COL_ROLE));

            ret.add(new User(id, email, nickName, password, role));
        }
        c.close();
        return ret;
    }

    @Override
    public User getUserDetail(User u) throws UserNotFoundException {
        String query = "select * from " + UsersTableHelper.TABLE_NAME + " where " +
            UsersTableHelper.COL_ID + " = ? OR " + UsersTableHelper.COL_EMAIL + " = ?";
        Cursor c = db.rawQuery(query, new String[] {String.valueOf(u.getId()), u.getEmail()});
        if (c.getCount() == 0) {
            throw new UserNotFoundException();
        }
        c.moveToNext();
        long id = c.getInt(c.getColumnIndex(UsersTableHelper.COL_ID));
        String email = c.getString(c.getColumnIndex(UsersTableHelper.COL_EMAIL));
        String nickName = c.getString(c.getColumnIndex(UsersTableHelper.COL_NICKNAME));
        String password = c.getString(c.getColumnIndex(UsersTableHelper.COL_PASSWORD));
        int role = c.getInt(c.getColumnIndex(UsersTableHelper.COL_ROLE));

        c.close();

        return new User(id, email, nickName, password, role);
    }

    @Override
    public boolean isUser(User u) {
        String query = "select * from " + UsersTableHelper.TABLE_NAME + " where " +
                UsersTableHelper.COL_EMAIL + " = ?";
        Cursor c = db.rawQuery(query, new String[]{String.valueOf(u.getEmail())});
        int count = c.getCount();
        c.close();

        return count > 0;
    }

    @Override
    public User newUser(User u) throws UserNotFoundException {
        if (newUserStatement == null) {
            newUserStatement = db.compileStatement(
                "insert into " + UsersTableHelper.TABLE_NAME + " ( " +
                    UsersTableHelper.COL_EMAIL + " , " +
                    UsersTableHelper.COL_NICKNAME + " , " +
                    UsersTableHelper.COL_PASSWORD + " , " +
                    UsersTableHelper.COL_ROLE +
                    " ) VALUES (?, ?, ?, ?)"
            );
        }
        newUserStatement.bindString(1, u.getEmail());
        newUserStatement.bindString(2, u.getNickName());
        newUserStatement.bindString(3, u.getPassword());
        newUserStatement.bindLong(4, u.getRole());
        long id = newUserStatement.executeInsert();
        if (id == -1) {
            throw new UserNotFoundException();
        }
        u.setId(id);
        return u;
    }

    @Override
    public boolean deleteUser(User u) throws UserNotFoundException {
        if (deleteUserStatement == null) {
            deleteUserStatement = db.compileStatement(
                "delete from " + UsersTableHelper.TABLE_NAME + " where " +
                    UsersTableHelper.COL_ID + " = ?"
            );
        }
        deleteUserStatement.bindLong(1, u.getId());
        int updatedRow = deleteUserStatement.executeUpdateDelete();
        if (updatedRow == 0) {
            throw new UserNotFoundException();
        }
        return updatedRow == 1;
    }

    public List<Workspace> getWorkspaceList(User u) {
        List<Workspace> ret = new ArrayList<>();

        String workspaceQuery = "select * from " + WorkspacesTableHelper.TABLE_NAME + " inner join " + AclsTableHelper.TABLE_NAME + " on " + WorkspacesTableHelper.TABLE_NAME + "." + WorkspacesTableHelper.COL_ID + " = " + AclsTableHelper.TABLE_NAME + "." + AclsTableHelper.COL_WORKSPACE_ID + " where " + AclsTableHelper.COL_USER_ID + " = ?";

        String[] args = { String.valueOf(u.getId()) };
        Cursor c = db.rawQuery(workspaceQuery, args);

        while (c.moveToNext()) {
            long id = c.getInt(c.getColumnIndex(AclsTableHelper.COL_WORKSPACE_ID));
            String name = c.getString(c.getColumnIndex(WorkspacesTableHelper.COL_NAME));
            Integer quota = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_QUOTA));
            Integer size = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_SIZE));
            Boolean isPublic = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_IS_PUBLIC)) == 1;
            Boolean isForeign = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_IS_FOREIGN)) == 1;
            String publicProfile = c.getString(c.getColumnIndex(WorkspacesTableHelper.COL_PUBLIC_PROFILE));
            String tags = c.getString(c.getColumnIndex(WorkspacesTableHelper.COL_TAGS));

            ret.add(new Workspace(id, isForeign, isPublic, quota, size, name, publicProfile, tags));
        }
        c.close();

        return ret;
    }

    public int updateUser(User u) {
        ContentValues args = new ContentValues();
        args.put(UsersTableHelper.COL_EMAIL, u.getEmail());
        args.put(UsersTableHelper.COL_NICKNAME, u.getNickName());
        args.put(UsersTableHelper.COL_PASSWORD, u.getPassword());
        args.put(UsersTableHelper.COL_ROLE, u.getRole());

        int updatedRows = db.update(UsersTableHelper.TABLE_NAME, args, UsersTableHelper.COL_ID + " = ?", new String[]{ Long.toString(u.getId()) });

        return updatedRows;
    }

    @Override
    public void open() {
        if (db == null) {
            db = dbOpenHelper.getWritableDatabase();
        }
    }

    @Override
    public void close() {
        if (updateUserStatement != null) {
            updateUserStatement.close();
            updateUserStatement = null;
        }
        if (db != null) {
            db.close();
            db = null;
        }
    }

}
