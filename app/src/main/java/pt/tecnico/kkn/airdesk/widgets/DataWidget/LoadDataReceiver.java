package pt.tecnico.kkn.airdesk.widgets.DataWidget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import pt.tecnico.kkn.airdesk.datasource.LocalFileDatasource;
import pt.tecnico.kkn.airdesk.datasource.LocalWorkspaceDatasource;
import pt.tecnico.kkn.airdesk.datasource.User;
import pt.tecnico.kkn.airdesk.datasource.UserDatasource;
import pt.tecnico.kkn.airdesk.datasource.Workspace;
import pt.tecnico.kkn.airdesk.exception.UserNotFoundException;
import pt.tecnico.kkn.airdesk.exception.WorkspaceNotFoundException;

public class LoadDataReceiver extends BroadcastReceiver {
    private String PROFILE = "A workspace made for testing. The text is always the same";
    private String CONTENT = "Nullam eros mi, mollis in sollicitudin non, tincidunt sed enim. Sed et felis metus, rhoncus ornare nibh. Ut at magna leo. Suspendisse egestas est ac dolor imperdiet pretium. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam porttitor, erat sit amet venenatis luctus, augue libero ultrices quam, ut congue nisi risus eu purus. Cras semper consectetur elementum. Nulla vel aliquet libero. Vestibulum eget felis nec purus commodo convallis. Aliquam erat volutpat.";
    private String TAGS = "test, tags, tags, for, testing";

    public LoadDataReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("pt.tecnico.kkn.airdesk.LOAD_DATA")) {
            Workspace[] workspaces = {
                    new Workspace(false, true, 10240, "[TEST]01", PROFILE, TAGS),
                    new Workspace(false, false, 51200, "[TEST]02", PROFILE, TAGS),
                    new Workspace(false, true, 819200, "[TEST]03", PROFILE, TAGS),
                    new Workspace(false, false, 10240, "[TEST]04", PROFILE, TAGS),
            };

            User[] users = {
                    new User("koevskin@gmail.com", "Nikola", "", 0),
                    new User("duckientruong@gmail.com", "Kien", "", 0),
                    new User("userx@example.com", "User X", "", 0),
                    new User("usery@example.com", "User Y", "", 0),
                    new User("userz@example.com", "User Z", "", 0),
                    new User("slj@slipsum.com", "Slipsum", "", 0)
            };

            for (Workspace workspace : workspaces) {
                Workspace w = addWorkspace(context, workspace);

                for (User user : users)
                    addUserToWorkspace(context, w, user);

                int randomFileNum = new Random().nextInt(10);
                for (int i = 0; i < randomFileNum; i++) {
                    addFileToWorkspace(context, workspace, "File" + Integer.toString(i), CONTENT);
                }
            }

            Toast.makeText(context, "Demo data added to AirDesk. Pull to refresh the data in the app.", Toast.LENGTH_LONG).show();
        }
    }

    private Workspace addWorkspace(Context context, Workspace workspace) {
        LocalWorkspaceDatasource workspaceDatasource = new LocalWorkspaceDatasource(context);
        workspaceDatasource.open();
        try {
            workspace = workspaceDatasource.newWorkspace(workspace);
            context.getDir((workspace.getName() + Long.toString(workspace.getId())), Context.MODE_PRIVATE);

            return workspace;
        } catch (WorkspaceNotFoundException e) {
            e.printStackTrace();
        } finally {
            workspaceDatasource.close();
        }

        return null;
    }

    private boolean addUserToWorkspace(Context context, Workspace workspace, User user) {
        LocalWorkspaceDatasource workspaceDatasource = new LocalWorkspaceDatasource(context);
        UserDatasource userDatasource = new UserDatasource(context);
        userDatasource.open();
        try {
            if (!userDatasource.isUser(user)) {
                user = userDatasource.newUser(user);
            } else {
                user = userDatasource.getUserDetail(user);
            }

            workspaceDatasource.open();
            workspaceDatasource.addUserToWorkspace(workspace, user);
            workspaceDatasource.close();

            return true;
        } catch (UserNotFoundException e) {
            Log.d("USER", "Add User DB error.");
        } finally {
            userDatasource.close();
        }

        return false;
    }

    private boolean addFileToWorkspace(Context context, Workspace workspace, String filename, String content) {
        if (workspace.getQuota() > workspace.getSize() + content.getBytes().length) {
            pt.tecnico.kkn.airdesk.datasource.File file = new pt.tecnico.kkn.airdesk.datasource.File((int) workspace.getId(), filename, content);

            LocalFileDatasource fileDatasource = new LocalFileDatasource(context, (int) workspace.getId());
            fileDatasource.open();
            try {
                fileDatasource.newFile(file);
            } catch (FileNotFoundException e) {
                Log.d("FILE", e.getMessage());
            } finally {
                fileDatasource.close();
            }

            File newFile = new File(context.getDir((workspace.getName() + Long.toString(workspace.getId())), Context.MODE_PRIVATE), filename + ".txt");
            try {
                FileOutputStream outputStream = new FileOutputStream(newFile);

                outputStream.write(content.getBytes());
                outputStream.close();

                return true;
            } catch (FileNotFoundException e) {
                Log.d("FILE", e.getMessage());
            } catch (IOException e) {
                Log.d("FILE", e.getMessage());
            }
        }

        return false;
    }
}
