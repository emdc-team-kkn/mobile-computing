package pt.tecnico.kkn.airdesk.frontend;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Toast;

import com.gc.materialdesign.views.Button;

import java.util.List;

import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.LocalWorkspaceDatasource;
import pt.tecnico.kkn.airdesk.datasource.User;
import pt.tecnico.kkn.airdesk.datasource.UserDatasource;
import pt.tecnico.kkn.airdesk.datasource.Workspace;
import pt.tecnico.kkn.airdesk.exception.UserNotFoundException;
import pt.tecnico.kkn.airdesk.frontend.adapter.UserAdapter;
import pt.tecnico.kkn.airdesk.frontend.adapter.UserRoleSpinnerAdapter;
import pt.tecnico.kkn.airdesk.helper.Validation;

public class InviteUsersActivity extends ActionBarActivity {
    private AutoCompleteTextView txtUserEmail;
    private Spinner spinnerRole;
    private Button btnAdd;
    private ListView listViewUsers;
    private UserAdapter userAdapter;
    private Context context;

    private Workspace workspace;
    private List<User> userList;

    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String EMAIL_MSG = "The email is invalid";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_users);

        Intent intent = getIntent();
        workspace = intent.getParcelableExtra("WORKSPACE");
        setTitle("Invite users to " + workspace.getName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;

        txtUserEmail = (AutoCompleteTextView)findViewById(R.id.txtUserEmail);
        spinnerRole = (Spinner)findViewById(R.id.spinnerRole);
        btnAdd = (Button)findViewById(R.id.btnAdd);
        listViewUsers = (ListView)findViewById(R.id.listViewUsers);

        String[] roles = getResources().getStringArray(R.array.user_roles);

        UserRoleSpinnerAdapter roleSpinnerAdapter = new UserRoleSpinnerAdapter(this, R.layout.spinner_item_user, roles);
        roleSpinnerAdapter.setDropDownViewResource(R.layout.spinner_item_user);
        spinnerRole.setAdapter(roleSpinnerAdapter);

        LocalWorkspaceDatasource workspaceDatasource = new LocalWorkspaceDatasource(this);
        workspaceDatasource.open();
        userList = workspaceDatasource.getUserList(workspace);
        workspaceDatasource.close();

        userAdapter = new UserAdapter(this, R.layout.list_item_user, userList, workspace);
        listViewUsers.setAdapter(userAdapter);



        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Validation.isEmailAddress(txtUserEmail, EMAIL_REGEX, EMAIL_MSG, true)) {
                    String email = txtUserEmail.getText().toString();
                    String txtRole = spinnerRole.getSelectedItem().toString();
                    int role = 0;

                    switch (txtRole) {
                        case "Reader":
                            role = 0;
                            break;
                        case "Editor":
                            role = 1;
                            break;
                        case "Admin":
                            role = 2;
                            break;
                        default:
                            role = 0;
                            break;
                    }

                    User user = new User(email, "", "", role);
                    UserDatasource userDatasource = new UserDatasource(getApplicationContext());
                    userDatasource.open();
                    try {
                        LocalWorkspaceDatasource workspaceDatasource = new LocalWorkspaceDatasource(getApplicationContext());
                        workspaceDatasource.open();
                        if (!workspaceDatasource.hasUser(workspace, user)) {
                            if(userDatasource.isUser(user)) {
                                user = userDatasource.getUserDetail(user);
                                user.setRole(role);
                            } else {
                                Log.d("Add user", "new User");
                                user = userDatasource.newUser(user);
                            }

                            Boolean res = workspaceDatasource.addUserToWorkspace(workspace, user);
                            Log.d("InviteUsers", res.toString());

                            userList.add(user);
                            userAdapter.notifyDataSetChanged();
                            txtUserEmail.setText("");
                            spinnerRole.setSelection(0);
                            Toast.makeText(context, "User added", Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(context, "User already exists", Toast.LENGTH_SHORT).show();
                        }
                        workspaceDatasource.close();
                    } catch (UserNotFoundException e) {
                        e.printStackTrace();
                    } finally {
                        userDatasource.close();
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_invite_users, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            Intent workspaceIntent = new Intent(this, WorkspaceActivity.class);
            workspaceIntent.putExtra("WORKSPACE", workspace);
            finish();
            startActivity(workspaceIntent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent workspaceIntent = new Intent(this, WorkspaceActivity.class);
        workspaceIntent.putExtra("WORKSPACE", workspace);
        finish();
        startActivity(workspaceIntent);
    }
}
