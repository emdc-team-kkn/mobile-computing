package pt.tecnico.kkn.airdesk.db.table;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Helper class for the files table
 * Columns: _id, workspace_id, filename, content, filesize,
 * updated_at, created_at, deleted_at, is_locked
 */
public class FilesTableHelper {

    private static final String TAG = "FilesTableHelper";

    public static final String TABLE_NAME = "files";
    public static final String COL_ID = "_id";
    public static final String COL_WORKSPACE_ID = "workspace_id";
    public static final String COL_FILENAME = "filename";
    public static final String COL_CONTENT = "content";
    public static final String COL_FILESIZE = "filesize";
    public static final String COL_UPDATED_AT = "updated_at";
    public static final String COL_CREATED_AT = "created_at";
    public static final String COL_DELETED_AT = "deleted_at";
    public static final String COL_LOCKED_BY = "locked_by";

    /**
     * Version 1 table
     */
    private static final String createQueryV1 = "create table " + TABLE_NAME + " ( " +
        COL_ID + " integer primary key autoincrement, " +
        COL_WORKSPACE_ID + " integer, " +
        COL_FILENAME + " text, " +
        COL_CONTENT + " text, " +
        COL_FILESIZE + " integer, " +
        COL_UPDATED_AT + " integer, " +
        COL_CREATED_AT + " integer, " +
        COL_DELETED_AT + " integer, " +
        COL_LOCKED_BY + " integer " +
        ")";

    public static void onCreate(SQLiteDatabase db) {
        logQuery(createQueryV1);
        db.execSQL(createQueryV1);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {
            default:
                throw new IllegalStateException("onUpgrade() with unknown version" + newVersion);
        }
    }

    private static void logQuery(String query) {
        Log.d(TAG, "Query executed: " + query);
    }
}
