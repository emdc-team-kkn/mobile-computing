package pt.tecnico.kkn.airdesk.widgets.DataWidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.Button;
import android.widget.RemoteViews;

import com.gc.materialdesign.views.ButtonFloat;

import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.RemoteWorkspaceDatasource;
import pt.tecnico.kkn.airdesk.frontend.WorkspacesActivity;

/**
 * Implementation of App Widget functionality.
 */
public class AutoDataWidget extends AppWidgetProvider {

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        final int N = appWidgetIds.length;
        for (int i = 0; i < N; i++) {
            Intent intent = new Intent();
            intent.setAction("pt.tecnico.kkn.airdesk.LOAD_DATA");
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.auto_data_widget);
            views.setOnClickPendingIntent(R.id.btnLoadData, pendingIntent);

            ComponentName widget = new ComponentName(context, AutoDataWidget.class);
            AppWidgetManager.getInstance(context).updateAppWidget(widget, views);
        }
    }


    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

    }
}


