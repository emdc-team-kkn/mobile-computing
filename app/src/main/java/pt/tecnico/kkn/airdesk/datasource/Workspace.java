package pt.tecnico.kkn.airdesk.datasource;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * This class represent a workspace in our system
 */
public class Workspace implements Parcelable {
    private long id;

    private String name;
    private int quota;
    private int size;
    private String publicProfile;
    private boolean isPublic;
    private String tags;

    private boolean isForeign;
    private String ipAddress;

    private int createdAt;
    private int updatedAt;
    private int deletedAt;

    public Workspace(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Workspace(boolean isForeign, boolean isPublic, int quota, String name) {
        this.id = -1;
        this.isForeign = isForeign;
        this.isPublic = isPublic;
        this.quota = quota;
        this.name = name;
        this.publicProfile = "";
        this.tags = "";
        this.ipAddress = "";
    }

    public Workspace(boolean isForeign, boolean isPublic, int quota, String name, String publicProfile, String tags) {
        this.id = -1;
        this.isForeign = isForeign;
        this.isPublic = isPublic;
        this.quota = quota;
        this.size = 0;
        this.name = name;
        this.publicProfile = publicProfile;
        this.tags = tags;
        this.ipAddress = "";
    }

    public Workspace(long id, boolean isForeign, boolean isPublic, int quota, int size, String name, String publicProfile, String tags) {
        this.id = id;
        this.isForeign = isForeign;
        this.isPublic = isPublic;
        this.quota = quota;
        this.size = size;
        this.name = name;
        this.publicProfile = publicProfile;
        this.tags = tags;
        this.ipAddress = "";
    }

    public boolean isForeign() {
        return isForeign;
    }

    public void setForeign(boolean isForeing) {
        this.isForeign = isForeing;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }

    public int getQuota() {
        return quota;
    }

    public void setQuota(int quota) {
        this.quota = quota;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPublicProfile() {
        return publicProfile;
    }

    public void setPublicProfile(String publicProfile) {
        this.publicProfile = publicProfile;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Workspace(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.quota = in.readInt();
        this.size = in.readInt();
        this.publicProfile = in.readString();
        this.isPublic = in.readInt() == 1;
        this.tags = in.readString();
        this.isForeign = in.readInt() == 1;
        this.ipAddress = in.readString();

        this.createdAt = in.readInt();
        this.updatedAt = in.readInt();
        this.deletedAt = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeLong(id);
        out.writeString(name);
        out.writeInt(quota);
        out.writeInt(size);
        out.writeString(publicProfile);
        out.writeInt(isPublic? 0 : 1);
        out.writeString(tags);
        out.writeInt(isForeign? 0 : 1);
        out.writeString(ipAddress);
        out.writeInt(createdAt);
        out.writeInt(updatedAt);
        out.writeInt(deletedAt);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Workspace createFromParcel(Parcel in) {
            return new Workspace(in);
        }

        public Workspace[] newArray(int size) {
            return new Workspace[size];
        }
    };

    public JSONObject toJSONObject() {
        JSONObject file = new JSONObject();
        JSONArray arr = new JSONArray();

        try {
            file.put("id", String.valueOf(this.id));
            file.put("name", this.name);
            file.put("quota", String.valueOf(this.quota));
            file.put("size", String.valueOf(this.size));
            file.put("publicProfile", this.publicProfile);
            file.put("isPublic", String.valueOf(this.isPublic));
            file.put("tags", this.tags);
            file.put("isForeign", String.valueOf(this.isForeign));
            file.put("createdAt", String.valueOf(this.createdAt));
            file.put("updatedAt", String.valueOf(this.updatedAt));
            file.put("deletedAt", String.valueOf(this.deletedAt));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return file;
    }

    public static Workspace fromJSONObject(JSONObject w) {
        try {
            long id = w.getLong("id");


            String name = w.getString("name");
            int quota = w.getInt("quota");
            int size = w.getInt("size");
            String publicProfile = w.getString("publicProfile");
            boolean isPublic = w.getBoolean("isPublic");
            String tags = w.getString("tags");

            boolean isForeign = w.getBoolean("isForeign");

            int createdAt = w.getInt("createdAt");
            int updatedAt = w.getInt("updatedAt");
            int deletedAt = w.getInt("deletedAt");

            Workspace workspace = new Workspace(id, isForeign, isPublic, quota, size, name, publicProfile, tags);

            return workspace;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
