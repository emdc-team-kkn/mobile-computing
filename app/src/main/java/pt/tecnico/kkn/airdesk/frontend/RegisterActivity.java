package pt.tecnico.kkn.airdesk.frontend;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.gc.materialdesign.views.ButtonRectangle;

import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.helper.Validation;

public class RegisterActivity extends ActionBarActivity {
    private EditText txtEmail;
    private EditText txtNickname;
    private EditText txtPassword;
    private ButtonRectangle btnRegister;
    private Context context = this;
    // Regular Expression
    // you can change the expression based on your need
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,20}$";

    // Error Messages
    private static final String REQUIRED_MSG = "This field is required";
    private static final String EMAIL_MSG = "The email is invalid";
    private static final String PASSWORD_MSG = "At least one: uppercase letter, lowercase letter, digit, special character and no whitespace. Min. 6 characters";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getSupportActionBar().hide();

        txtEmail = (EditText)findViewById(R.id.txtEmail);
        txtNickname = (EditText)findViewById(R.id.txtNickname);
        txtPassword = (EditText)findViewById(R.id.txtPassword);
        btnRegister = (ButtonRectangle)findViewById(R.id.btnRegister);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Validation.isValid(txtEmail, EMAIL_REGEX, EMAIL_MSG, true) && Validation.hasText(txtNickname, REQUIRED_MSG) && Validation.isValid(txtPassword, PASSWORD_REGEX, PASSWORD_MSG, true)) {
                    SharedPreferences.Editor editor = getSharedPreferences("AirDeskPreferences", MODE_PRIVATE).edit();
                    editor.putString("EMAIL", txtEmail.getText().toString());
                    editor.putString("NICKNAME", txtNickname.getText().toString());
                    editor.putString("PASSWORD", txtPassword.getText().toString());
                    editor.commit();

                    Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                    workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(workspacesIntent);
                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}
