package pt.tecnico.kkn.airdesk.frontend;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonRectangle;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.UnknownHostException;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.LocalFileDatasource;
import pt.tecnico.kkn.airdesk.datasource.Workspace;
import pt.tecnico.kkn.airdesk.helper.Validation;

public class ForeignEditFileActivity extends ActionBarActivity {
    private ButtonRectangle btnSaveFile;
    private EditText txtFilename;
    private EditText txtContent;

    private Workspace workspace;
    private pt.tecnico.kkn.airdesk.datasource.File file;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foreign_edit_file);

        setTitle(R.string.title_activity_file_edit);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        workspace = intent.getParcelableExtra("WORKSPACE");
        file = intent.getParcelableExtra("FILE");

        txtFilename = (EditText)findViewById(R.id.txtFilename);
        txtContent = (EditText)findViewById(R.id.txtContent);

        txtFilename.setText(file.getFilename());
        txtContent.setText(file.getContent());

        btnSaveFile = (ButtonRectangle)findViewById(R.id.btnSave);

        btnSaveFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveFile();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_file, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_discard:
                //TODO Remote Delete
                deleteFile();

                return true;
            case android.R.id.home:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(String.format(getResources().getString(R.string.dialog_edit_file_msg), file.getFilename())).setTitle(R.string.dialog_edit_file_title);
                builder.setPositiveButton(R.string.dialog_confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        saveFile();
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        new UnlockFileCommTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, workspace.getIpAddress(), file.toJSONObject().toString());
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void saveFile() {
        //Write file to FS and save to DB
        String filename = txtFilename.getText().toString();
        String newContent = txtContent.getText().toString();

        if (Validation.hasText(txtFilename, "The filename is required")) {
            if (workspace.getQuota() > workspace.getSize() - file.getFileSize() + newContent.getBytes().length) {
                String oldFilename = file.getFilename();
                file.setFilename(filename);
                file.setContent(newContent);
                file.setFileSize(newContent.getBytes().length);

                new EditFileCommTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, workspace.getIpAddress(), oldFilename, file.toJSONObject().toString(), workspace.toJSONObject().toString());

            } else {
                Toast.makeText(context, "The file is too big for the workspace", Toast.LENGTH_LONG).show();
                Log.d("FILE", "Workspace exceeds quota.");
            }
        }
    }

    private void deleteFile() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(String.format(getResources().getString(R.string.dialog_delete_file_msg), file.getFilename())).setTitle(R.string.dialog_delete_file_title);
        builder.setPositiveButton(R.string.dialog_confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                new DeleteFileCommTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, workspace.getIpAddress(), file.toJSONObject().toString(), workspace.toJSONObject().toString());
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(String.format(getResources().getString(R.string.dialog_edit_file_msg), file.getFilename())).setTitle(R.string.dialog_edit_file_title);
        builder.setPositiveButton(R.string.dialog_confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                saveFile();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                new UnlockFileCommTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, workspace.getIpAddress(), file.toJSONObject().toString());
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public class EditFileCommTask extends AsyncTask<String, String, Void> {
        private static final String TAG = "EditFileTask";
        private static final String REQUEST = "editfile";

        @Override
        protected Void doInBackground(String... params) {
            Log.d(TAG, "Client Started: " + REQUEST);
            String ipAddress = params[0];
            String oldFilename = params[1];
            String fileString = params[2];
            String workspaceString = params[3];
            String result;

            SharedPreferences preferences = getSharedPreferences("AirDeskPreferences", MODE_PRIVATE);
            String email = preferences.getString("EMAIL", null);
            String password = preferences.getString("PASSWORD", null);

            try {
                Log.d(TAG, "To Server: " + ipAddress + " " + getString(R.string.port));
                SimWifiP2pSocket clientSocket = new SimWifiP2pSocket(ipAddress, Integer.parseInt(getString(R.string.port)));

                PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);

                Log.d(TAG, "Write Started");
                writer.println(REQUEST);

                Log.d(TAG, "Write Email: " + email);
                writer.println(email);

                Log.d(TAG, "Write Password: " + password);
                writer.println(password);

                Log.d(TAG, "Write Old Name: " + oldFilename);
                writer.println(oldFilename);

                Log.d(TAG, "Write File: " + fileString);
                writer.println(fileString);

                Log.d(TAG, "Write Workspace: " + workspaceString);
                writer.println(workspaceString);

                Log.d(TAG, "Write Complete");

                BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                Log.d(TAG, "Read Started");

                result = br.readLine();
                publishProgress(result);

                Log.d(TAG, "Read Finished");

                clientSocket.close();
            } catch (UnknownHostException e) {
                Toast.makeText(context, "Workspace owner unavailable, try again later", Toast.LENGTH_SHORT).show();

                Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(workspacesIntent);
            } catch (IOException e) {
                Toast.makeText(context, "Workspace owner unavailable, try again later", Toast.LENGTH_SHORT).show();

                Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(workspacesIntent);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            String res = values[0];
            Log.d(TAG, res);

            if (res.equals("json_error")) {
                Toast.makeText(context, "Bad message format, try again", Toast.LENGTH_SHORT).show();
            } else if (res.equals("User not found")){
                Toast.makeText(context, "Error: User not found", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    JSONObject result = new JSONObject(res);
                    String type = result.getString("type");
                    String message = result.getString("message");

                    if(type.equals("error")) {
                        new UnlockFileCommTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, workspace.getIpAddress(), file.toJSONObject().toString());
                    }

                    Toast.makeText(context, type.toUpperCase() + ": " + message, Toast.LENGTH_SHORT).show();

                    Intent workspaceIntent = new Intent(context, ForeignReadFileActivity.class);
                    workspaceIntent.putExtra("WORKSPACE", workspace);
                    workspaceIntent.putExtra("FILE", file);
                    startActivity(workspaceIntent);
                } catch (JSONException e) {
                    Toast.makeText(context, "Bad message format, try again", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public class UnlockFileCommTask extends AsyncTask<String, String, Void> {
        private static final String TAG = "UnlockFileTask";
        private static final String REQUEST = "unlockfile";

        @Override
        protected Void doInBackground(String... params) {
            Log.d(TAG, "Client Started");
            String ipAddress = params[0];
            String jsonString = params[1];
            String result;

            try {
                Log.d(TAG, "To Server: " + ipAddress + " " + getString(R.string.port));
                SimWifiP2pSocket clientSocket = new SimWifiP2pSocket(ipAddress, Integer.parseInt(getString(R.string.port)));

                PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);

                Log.d(TAG, "Write Started");
                Log.d(TAG, "Request: " + REQUEST);
                writer.println(REQUEST);

                Log.d(TAG, "Write File: " + jsonString);
                writer.println(jsonString);

                Log.d(TAG, "Write Complete");

                BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                Log.d(TAG, "Read Started");
                result = br.readLine();
                publishProgress(result);

                Log.d(TAG, "Read Finished");
                clientSocket.close();
            } catch (UnknownHostException e) {
                Log.e("Broadcast", "Exception", e);
            } catch (IOException e) {
                Log.e("Broadcast", "Exception", e);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            String res = values[0];

            try {
                JSONObject result = new JSONObject(res);
                String type = result.getString("type");
                String message = result.getString("message");

                if (type.equals("error")) {
                    Toast.makeText(context, type.toUpperCase() + ": " + message, Toast.LENGTH_SHORT).show();
                } else if (type.equals("success")) {
                    Intent editFileIntent = new Intent(context, ForeignReadFileActivity.class);
                    editFileIntent.putExtra("WORKSPACE", workspace);
                    editFileIntent.putExtra("FILE", file);
                    startActivity(editFileIntent);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public class DeleteFileCommTask extends AsyncTask<String, String, Void> {
        private static final String TAG = "DeleteFileTask";
        private static final String REQUEST = "deletefile";

        @Override
        protected Void doInBackground(String... params) {
            Log.d(TAG, "Client Started");
            String ipAddress = params[0];
            String jsonString = params[1];
            String workspaceString = params[2];
            String result;

            SharedPreferences preferences = getSharedPreferences("AirDeskPreferences", MODE_PRIVATE);
            String email = preferences.getString("EMAIL", null);
            String password = preferences.getString("PASSWORD", null);

            try {
                Log.d(TAG, "To Server: " + ipAddress + " " + getString(R.string.port));
                SimWifiP2pSocket clientSocket = new SimWifiP2pSocket(ipAddress, Integer.parseInt(getString(R.string.port)));

                PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);

                Log.d(TAG, "Write Started");
                Log.d(TAG, "Request: " + REQUEST);
                writer.println(REQUEST);

                Log.d(TAG, "Write Email: " + email);
                writer.println(email);

                Log.d(TAG, "Write Password: " + password);
                writer.println(password);

                Log.d(TAG, "Write Workspace: " + workspaceString);
                writer.println(workspaceString);

                Log.d(TAG, "Write File: " + jsonString);
                writer.println(jsonString);

                Log.d(TAG, "Write Complete");

                BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                Log.d(TAG, "Read Started");
                result = br.readLine();
                publishProgress(result);

                Log.d(TAG, "Read Finished");
                clientSocket.close();
            } catch (UnknownHostException e) {
                Toast.makeText(context, "Workspace owner unavailable, try again later", Toast.LENGTH_SHORT).show();

                Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(workspacesIntent);
            } catch (IOException e) {
                Toast.makeText(context, "Workspace owner unavailable, try again later", Toast.LENGTH_SHORT).show();

                Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(workspacesIntent);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            String res = values[0];
            Log.d(TAG, res);

            if (res.equals("json_error")) {
                Toast.makeText(context, "Bad message format, try again", Toast.LENGTH_SHORT).show();
            } else if (res.equals("User not found")){
                Toast.makeText(context, "Error: User not found", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    JSONObject result = new JSONObject(res);
                    String type = result.getString("type");
                    String message = result.getString("message");

                    Toast.makeText(context, type.toUpperCase() + ": " + message, Toast.LENGTH_SHORT).show();

                    if (type.equals("success")) {
                        Intent workspaceIntent = new Intent(context, ForeignWorkspaceActivity.class);
                        workspaceIntent.putExtra("WORKSPACE", workspace);
                        finish();
                        startActivity(workspaceIntent);
                    }

                } catch (JSONException e) {
                    Toast.makeText(context, "Bad message format, try again", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
