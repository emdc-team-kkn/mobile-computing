package pt.tecnico.kkn.airdesk.db.table;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Helper class for our acls table.
 * Columns: _id, user_id, workspace_id
 */
public class AclsTableHelper {

    private static final String TAG = "AclsTableHelper";

    public static final String TABLE_NAME = "acls";
    public static final String COL_ID = "_id";
    public static final String COL_USER_ID = "user_id";
    public static final String COL_WORKSPACE_ID = "workspace_id";
    public static final String COL_USER_ROLE = "user_role";

    /**
     * Version 1 table
     */
    private static final String createQueryV1 = "create table " + TABLE_NAME + " ( " +
        COL_ID + " integer primary key autoincrement, " +
        COL_USER_ID + " integer, " +
        COL_WORKSPACE_ID + " integer, " +
        COL_USER_ROLE + " integer " +
        ")";

    public static void onCreate(SQLiteDatabase db) {
        logQuery(createQueryV1);
        db.execSQL(createQueryV1);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {
            default:
                throw new IllegalStateException("onUpgrade() with unknown version" + newVersion);
        }
    }

    private static void logQuery(String query) {
        Log.d(TAG, "Query executed: " + query);
    }
}
