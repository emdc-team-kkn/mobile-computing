package pt.tecnico.kkn.airdesk.frontend;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonRectangle;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.File;
import pt.tecnico.kkn.airdesk.datasource.LocalFileDatasource;
import pt.tecnico.kkn.airdesk.datasource.Workspace;

public class ReadFileActivity extends ActionBarActivity {
    private TextView lblContent;
    private ButtonRectangle btnEdit;
    private ButtonRectangle btnDelete;
    private Workspace workspace;
    private File file;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_file);

        Intent intent = getIntent();
        workspace = intent.getParcelableExtra("WORKSPACE");
        file = intent.getParcelableExtra("FILE");

        setTitle(file.getFilename());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lblContent = (TextView)findViewById(R.id.lblReadContent);
        btnEdit = (ButtonRectangle)findViewById(R.id.btnEdit);
        btnDelete = (ButtonRectangle)findViewById(R.id.btnDelete);

        java.io.File openFile = new java.io.File(context.getDir((workspace.getName() + Long.toString(workspace.getId())), Context.MODE_PRIVATE), file.getFilename() + ".txt");
        try {
            FileInputStream inputStream = new FileInputStream(openFile);
            byte[] buffer = new byte[file.getFileSize()];

            inputStream.read(buffer, 0, file.getFileSize());

            String content = new String(buffer);
            lblContent.setText(content);

            inputStream.close();
        } catch (FileNotFoundException e) {
            Toast.makeText(context, "Error reading from file", Toast.LENGTH_LONG).show();
            Log.d("FILE", e.getMessage());
        } catch (IOException e) {
            Toast.makeText(context, "Error reading from file", Toast.LENGTH_LONG).show();
            Log.d("FILE", e.getMessage());
        };

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editFile();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteFile();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_read_file, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                editFile();

                return true;
            case R.id.action_discard:
                deleteFile();

                return true;
            case android.R.id.home:
                Intent workspaceIntent = new Intent(context, WorkspaceActivity.class);
                workspaceIntent.putExtra("WORKSPACE", workspace);
                finish();
                startActivity(workspaceIntent);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void editFile() {
        LocalFileDatasource fileDatasource = new LocalFileDatasource(context, (int)workspace.getId());
        fileDatasource.open();
        try {
            if (fileDatasource.isFileLocked(file) == false) {
                fileDatasource.lockFile(file);

                Intent editFileIntent = new Intent(context, EditFileActivity.class);
                editFileIntent.putExtra("WORKSPACE", workspace);
                editFileIntent.putExtra("FILE", file);
                startActivity(editFileIntent);
            } else {
                Toast.makeText(context, "File cannot be edited at the moment!", Toast.LENGTH_LONG).show();
            }
        } catch (FileNotFoundException e) {
            Toast.makeText(context, "File cannot be found", Toast.LENGTH_LONG).show();
            Log.d("FILE", e.getMessage());

            Intent workspaceIntent = new Intent(context, WorkspaceActivity.class);
            workspaceIntent.putExtra("WORKSPACE", workspace);
            finish();
            startActivity(workspaceIntent);
        } finally {
            fileDatasource.close();
        }
    }

    private void deleteFile() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(String.format(getResources().getString(R.string.dialog_delete_file_msg), file.getFilename())).setTitle(R.string.dialog_delete_file_title);
        builder.setPositiveButton(R.string.dialog_confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                LocalFileDatasource fileDatasource = new LocalFileDatasource(context, (int)workspace.getId());
                fileDatasource.open();

                try {
                    fileDatasource.deleteFile(file);

                    java.io.File delFile = new java.io.File(context.getDir((workspace.getName() + Long.toString(workspace.getId())), Context.MODE_PRIVATE), file.getFilename() + ".txt");
                    delFile.delete();

                    Intent workspaceIntent = new Intent(context, WorkspaceActivity.class);
                    workspaceIntent.putExtra("WORKSPACE", workspace);
                    startActivity(workspaceIntent);
                } catch (FileNotFoundException e) {
                    Toast.makeText(context, "Could not delete file: File not found", Toast.LENGTH_LONG).show();
                    Log.d("FILE", e.getMessage());
                } finally {
                    fileDatasource.close();
                }
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        Intent workspaceIntent = new Intent(context, WorkspaceActivity.class);
        workspaceIntent.putExtra("WORKSPACE", workspace);
        finish();
        startActivity(workspaceIntent);
    }
}
