package pt.tecnico.kkn.airdesk.datasource;

import java.util.List;

import pt.tecnico.kkn.airdesk.exception.WorkspaceNotFoundException;

/**
 * Data source interface for our workspaces
 */
public interface IWorkspaceDatasource {
    /**
     * Get the list of workspace available, only the name, id
     * and user_id (foreign workspace only) are available in the result
     * @return the list of workspace
     */
    public List<Workspace> getWorkspacesList();

    /**
     * Return the detail of a workspace
     * @return the workspace with full detail
     */
    public Workspace getWorkspaceDetail(Workspace w) throws WorkspaceNotFoundException;

    /**
     * Save a new workspace and return the workspace with id
     * @param w The workspace to be saved
     * @return the same workspace passed in, but with id field populated
     */
    public Workspace newWorkspace(Workspace w) throws WorkspaceNotFoundException;

    /**
     * Update an existing workspace
     * @param w The workspace to be updated.
     */
    public boolean updateWorkspace(Workspace w) throws WorkspaceNotFoundException;

    /**
     * Delete an existing workspace
     * @param w The workspace to be deleted
     */
    public boolean deleteWorkspace(Workspace w) throws WorkspaceNotFoundException;

    /**
     * Restore a deleted workspace
     * @param w The workspace to be restored
     */
    public boolean undeleteWorkspace(Workspace w) throws WorkspaceNotFoundException;

    /**
     * Add an user to a workspace
     * @param w The workspace to be modified
     * @param u The user to add to the workspace
     */
    public boolean addUserToWorkspace(Workspace w, User u);

    /**
     * Remove an user from a workspace
     * @param w The workspace to be modified
     * @param u The user to be removed
     */
    public boolean removeUserFromWorkspace(Workspace w, User u);

    /**
     * Check if the user is allowed to access the workspace
     * @param w The workspace to be checked
     * @param u The user to be checked
     */
    public boolean isUserAllowed(Workspace w, User u);

    /**
     * Get all the users for a workspace
     * @param w The workspace to get the users for
     */
    public List<User> getUserList(Workspace w);

    /**
     * Open the workspace
     */
    public void open();

    /**
     * Close the workspace
     */
    public void close();
}
