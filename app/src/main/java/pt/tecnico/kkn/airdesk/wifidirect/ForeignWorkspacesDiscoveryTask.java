package pt.tecnico.kkn.airdesk.wifidirect;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.Workspace;
import pt.tecnico.kkn.airdesk.frontend.WorkspacesActivity;

/**
 * Async task to discover foreign workspaces
 */
public class ForeignWorkspacesDiscoveryTask extends AsyncTask<String, Void, List<Workspace>> {
    private static final String TAG = "FWDiscovery";
    private final ExecutorService executorService;
    private final WorkspacesActivity activity;

    public ForeignWorkspacesDiscoveryTask(ExecutorService executorService, WorkspacesActivity activity) {
        this.activity = activity;
        this.executorService = executorService;
    }

    @Override
    protected List<Workspace> doInBackground(String... params) {
        Log.d(TAG, "Start workspace discovery");
        SharedPreferences preferences = activity.getSharedPreferences("AirDeskPreferences", Context.MODE_PRIVATE);
        String ipAddresses = preferences.getString("ipAddresses", null);
        String email = preferences.getString("EMAIL", null);
        String password = preferences.getString("PASSWORD", null);
        List<Callable<List<Workspace>>> requests = new ArrayList<>();
        ArrayList<Workspace> workspaceList = new ArrayList<>();

        Log.d(TAG, "IP Addresses: " + ipAddresses);
        Log.d(TAG, "EMAIL: " + email);

        if (ipAddresses != null && email != null) {
            try {
                JSONArray jsonArray = new JSONArray(ipAddresses);
                for (int i = 0; i < jsonArray.length(); i++) {
                    String address = jsonArray.getString(i);
                    requests.add(
                            new DiscoveryCommTask(
                                    address,
                                    email,
                                    password,
                                    Integer.valueOf(activity.getString(R.string.port))
                            ));
                }

                // Wait till all request is done
                // Collect all the result and send one broadcast to the main activities
                try {

                    List<Future<List<Workspace>>> result = executorService.invokeAll(requests);
                    for (Future<List<Workspace>> ft : result) {
                        try {
                            List<Workspace> tmp = ft.get();
                            workspaceList.addAll(tmp);
                        } catch (ExecutionException e) {
                            Log.d(TAG, "Group discovery failed for one address");
                        }
                    }
                } catch (InterruptedException e) {
                    Log.e(TAG, "Unable to perform group discovery");
                }

                Log.d(TAG, "Group available");
            } catch (JSONException e) {
                Log.d(TAG, "Malformed ip addressed");
            }
        } else {
            Log.d(TAG, "No peers");
        }
        return workspaceList;
    }

    @Override
    protected void onPostExecute(List<Workspace> workspaceList) {
        WorkspacesActivity.ForeignWorkspacesFragment fragment = (WorkspacesActivity.ForeignWorkspacesFragment)
                activity.getSupportFragmentManager().getFragments().get(1);//findFragmentById(R.layout.fragment_workspace_foreign);
        fragment.updateList(workspaceList);
    }
}
