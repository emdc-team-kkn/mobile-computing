package pt.tecnico.kkn.airdesk.datasource;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pt.tecnico.kkn.airdesk.db.AirDeskDbOpenHelper;
import pt.tecnico.kkn.airdesk.db.table.FilesTableHelper;

/**
 * This class implements datasource for local files
 */
public class LocalFileDatasource implements IFilesDatasource {

    private final AirDeskDbOpenHelper dbOpenHelper;
    private int workspaceId;
    private SQLiteDatabase db = null;
    private SQLiteStatement newFileStatement;
    private SQLiteStatement updateFileStatement;
    private SQLiteStatement deleteFileStatement;
    private SQLiteStatement lockFileStatement;

    public LocalFileDatasource(Context context, int workspaceId) {
        this.dbOpenHelper = new AirDeskDbOpenHelper(context);
        this.workspaceId = workspaceId;
    }

    @Override
    public void open() {
        db = dbOpenHelper.getWritableDatabase();
    }

    @Override
    public void close() {
        if (newFileStatement != null) {
            newFileStatement.close();
            newFileStatement = null;
        }
        if (updateFileStatement != null) {
            updateFileStatement.close();
            updateFileStatement = null;
        }
        if (deleteFileStatement != null) {
            deleteFileStatement.close();
            deleteFileStatement = null;
        }
        if (lockFileStatement != null) {
            lockFileStatement.close();
            lockFileStatement = null;
        }
        if (db != null) {
            db.close();
            db = null;
        }
    }

    @Override
    public List<File> getFilesList() {
        List<File> ret = new ArrayList<>();

        String query = "select * from " + FilesTableHelper.TABLE_NAME +
            " where " + FilesTableHelper.COL_WORKSPACE_ID + " = ? and " +
                        FilesTableHelper.COL_DELETED_AT + " is null";

        Cursor cursor = db.rawQuery(query, new String[] { String.valueOf(workspaceId) });
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex(FilesTableHelper.COL_ID));
            int workspaceId = cursor.getInt(cursor.getColumnIndex(FilesTableHelper.COL_WORKSPACE_ID));
            String filename = cursor.getString(cursor.getColumnIndex(FilesTableHelper.COL_FILENAME));
            String content = cursor.getString(cursor.getColumnIndex(FilesTableHelper.COL_CONTENT));
            int filesize = cursor.getInt(cursor.getColumnIndex(FilesTableHelper.COL_FILESIZE));
            int createdAt = cursor.getInt(cursor.getColumnIndex(FilesTableHelper.COL_CREATED_AT));
            int updatedAt = cursor.getInt(cursor.getColumnIndex(FilesTableHelper.COL_UPDATED_AT));
            int deletedAt = cursor.getInt(cursor.getColumnIndex(FilesTableHelper.COL_DELETED_AT));
            int lockedBy = cursor.getInt(cursor.getColumnIndex(FilesTableHelper.COL_LOCKED_BY));

            ret.add(new File(id, workspaceId, filename, content, filesize,
                    createdAt, updatedAt, deletedAt, lockedBy));
        }

        cursor.close();
        return ret;
    }

    @Override
    public File getFileDetail(File f) throws FileNotFoundException {
        String query = "select * from " + FilesTableHelper.TABLE_NAME + " where _id = ?";
        Cursor cursor = db.rawQuery(query, new String[] { String.valueOf(f.getId()) });

        if (cursor.getCount() == 0) {
            throw new FileNotFoundException();
        }

        File ret = null;
        while(cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex(FilesTableHelper.COL_ID));
            int workspaceId = cursor.getInt(cursor.getColumnIndex(FilesTableHelper.COL_WORKSPACE_ID));
            String filename = cursor.getString(cursor.getColumnIndex(FilesTableHelper.COL_FILENAME));
            String content = cursor.getString(cursor.getColumnIndex(FilesTableHelper.COL_CONTENT));
            int filesize = cursor.getInt(cursor.getColumnIndex(FilesTableHelper.COL_FILESIZE));
            int createdAt = cursor.getInt(cursor.getColumnIndex(FilesTableHelper.COL_CREATED_AT));
            int updatedAt = cursor.getInt(cursor.getColumnIndex(FilesTableHelper.COL_UPDATED_AT));
            int deletedAt = cursor.getInt(cursor.getColumnIndex(FilesTableHelper.COL_DELETED_AT));
            int lockedBy = cursor.getInt(cursor.getColumnIndex(FilesTableHelper.COL_LOCKED_BY));
            ret = new File(id, workspaceId, filename, content, filesize,
                createdAt, updatedAt, deletedAt, lockedBy);
        }
        cursor.close();
        return ret;
    }

    @Override
    public File newFile(File f) throws FileNotFoundException {
        if (newFileStatement == null) {
            newFileStatement = db.compileStatement(
                "insert into " + FilesTableHelper.TABLE_NAME + " ( " +
                    FilesTableHelper.COL_WORKSPACE_ID + " , " +
                    FilesTableHelper.COL_FILENAME + " , " +
                    FilesTableHelper.COL_CONTENT + " , " +
                    FilesTableHelper.COL_FILESIZE + " , " +
                    FilesTableHelper.COL_CREATED_AT + " , " +
                    FilesTableHelper.COL_UPDATED_AT + " , " +
                    FilesTableHelper.COL_LOCKED_BY +
                    ") values (?,?,?,?,?,?,?)"
            );
        }
        long currentTime = getCurrentTime();
        newFileStatement.bindLong(1, f.getWorkspaceId());
        newFileStatement.bindString(2, f.getFilename());
        newFileStatement.bindString(3, f.getContent());
        newFileStatement.bindLong(4, f.getFileSize());
        newFileStatement.bindLong(5, currentTime);
        newFileStatement.bindLong(6, currentTime);
        newFileStatement.bindLong(7, 0);
        long id = newFileStatement.executeInsert();
        if (id == -1) {
            throw new FileNotFoundException();
        }
        f.setId(id);
        return f;
    }

    @Override
    public boolean updateFile(File f) throws FileNotFoundException {
        if (updateFileStatement == null) {
            updateFileStatement = db.compileStatement(
                "update " + FilesTableHelper.TABLE_NAME + " set " +
                    FilesTableHelper.COL_FILENAME + " = ?," +
                    FilesTableHelper.COL_CONTENT + " = ?," +
                    FilesTableHelper.COL_FILESIZE + " = ?, " +
                    FilesTableHelper.COL_UPDATED_AT + " = ?, " +
                    FilesTableHelper.COL_LOCKED_BY + " = ? " +
                    "where " + FilesTableHelper.COL_ID + " = ?"
            );
        }
        long currentTime = getCurrentTime();
        updateFileStatement.bindString(1, f.getFilename());
        updateFileStatement.bindString(2, f.getContent());
        updateFileStatement.bindLong(3, f.getFileSize());
        updateFileStatement.bindLong(4, currentTime);
        updateFileStatement.bindLong(5, 0);
        updateFileStatement.bindLong(6, f.getId());
        int updatedRow = updateFileStatement.executeUpdateDelete();
        if (updatedRow == 0) {
            throw new FileNotFoundException();
        }
        return updatedRow == 1;
    }

    @Override
    public boolean deleteFile(File f) throws FileNotFoundException {
        if (deleteFileStatement == null) {
            deleteFileStatement = db.compileStatement(
                "update " + FilesTableHelper.TABLE_NAME + " set " +
                    FilesTableHelper.COL_DELETED_AT + " = ? " +
                    "where " + FilesTableHelper.COL_ID + " = ?"
            );
        }
        long currentTime = getCurrentTime();
        deleteFileStatement.bindLong(1, currentTime);
        deleteFileStatement.bindLong(2, f.getId());
        int updatedRow = deleteFileStatement.executeUpdateDelete();
        if (updatedRow == 0) {
            throw new FileNotFoundException();
        }
        return updatedRow == 1;
    }

    @Override
    public boolean undeleteFile(File f) throws FileNotFoundException {
        if (deleteFileStatement == null) {
            deleteFileStatement = db.compileStatement(
                "update " + FilesTableHelper.TABLE_NAME + " set " +
                    FilesTableHelper.COL_DELETED_AT + " = ? " +
                    "where " + FilesTableHelper.COL_ID + " = ?"
            );
        }
        deleteFileStatement.bindNull(1);
        deleteFileStatement.bindLong(2, f.getId());
        int updatedRow = deleteFileStatement.executeUpdateDelete();
        if (updatedRow == 0) {
            throw new FileNotFoundException();
        }
        return updatedRow == 1;
    }

    @Override
    public boolean lockFile(File f) throws FileNotFoundException {
        if (lockFileStatement == null) {
            lockFileStatement = db.compileStatement(
                "update " + FilesTableHelper.TABLE_NAME + " set " +
                    FilesTableHelper.COL_LOCKED_BY + " = ? " +
                    "where " + FilesTableHelper.COL_ID + " = ?"
            );
        }
        lockFileStatement.bindLong(1, f.getLockedBy());
        lockFileStatement.bindLong(2, f.getId());
        int updatedRow = lockFileStatement.executeUpdateDelete();
        if (updatedRow == 0) {
            throw new FileNotFoundException();
        }
        return updatedRow == 1;
    }

    @Override
    public boolean unlockFile(File f) throws FileNotFoundException {
        if (lockFileStatement == null) {
            lockFileStatement = db.compileStatement(
                "update " + FilesTableHelper.TABLE_NAME + " set " +
                    FilesTableHelper.COL_LOCKED_BY + " = ? " +
                    "where " + FilesTableHelper.COL_ID + " = ?"
            );
        }
        lockFileStatement.bindNull(1);
        lockFileStatement.bindLong(2, f.getId());
        int updatedRow = lockFileStatement.executeUpdateDelete();
        if (updatedRow == 0) {
            throw new FileNotFoundException();
        }
        return updatedRow == 1;
    }

    @Override
    public boolean isFileLocked(File f) throws FileNotFoundException {
        String query = "select " + FilesTableHelper.COL_LOCKED_BY + " from " + FilesTableHelper.TABLE_NAME + " where _id = ?";
        Cursor cursor = db.rawQuery(query, new String[] { String.valueOf(f.getId()) });

        if (cursor.getCount() == 0) {
            throw new FileNotFoundException();
        }

        int lockedBy = 0;

        while(cursor.moveToNext()) {
            lockedBy = cursor.getInt(cursor.getColumnIndex(FilesTableHelper.COL_LOCKED_BY));
        }
        cursor.close();

        return lockedBy == 1;
    }

    /**
     * Return the current time
     */
    private long getCurrentTime() {
        return new Date().getTime();
    }
}
