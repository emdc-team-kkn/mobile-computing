package pt.tecnico.kkn.airdesk.datasource;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * The data source interface for our files
 */
public interface IFilesDatasource {
    /**
     * Return a list of file from the datasource.
     * This file object in the list only contain the id, workspaceId, filename
     * & ownerId (for remote file)
     * @return the list of file
     */
    public List<File> getFilesList();

    /**
     * Return the detail of the specified file
     * @param f The file to read
     * @return The file with detail filled in
     */
    public File getFileDetail(File f) throws FileNotFoundException;

    /**
     * Save a new file and return the file with id field filled
     * @param f The file to be saved
     */
    public File newFile(File f) throws FileNotFoundException;

    /**
     * Update an existing file
     * @param f The file to be updated
     */
    public boolean updateFile(File f) throws FileNotFoundException;

    /**
     * Delete an existing file
     * @param f The file to be deleted
     */
    public boolean deleteFile(File f) throws FileNotFoundException;

    /**
     * Restore a deleted file
     * @param f The file to be deleted
     * @throws FileNotFoundException
     */
    public boolean undeleteFile(File f) throws FileNotFoundException;
    /**
     * Lock an existing file for editing
     * @param f The file to be locked
     */
    public boolean lockFile(File f) throws FileNotFoundException;

    /**
     * Unlock a file
     * @param f The file to be unlocked
     */
    public boolean unlockFile(File f) throws FileNotFoundException;

    /**
     * Check if file is locked
     * @param f The file to be checked
     */
    public boolean isFileLocked(File f) throws FileNotFoundException;

    /**
     * Open the datasource for read/write
     */
    public void open();

    /**
     * Close the datasource
     */
    public void close();
}
