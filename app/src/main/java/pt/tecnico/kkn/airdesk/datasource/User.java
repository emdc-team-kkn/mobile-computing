package pt.tecnico.kkn.airdesk.datasource;

/**
 * This class represent a user in our system
 */
public class User {
    private long id;
    private String email;
    private String nickName;
    private String password;
    private int role;

    public User(String email, String nickName) {
        this.email = email;
        this.nickName = nickName;
    }

    public User(long id, String email, String nickName) {
        this.id = id;
        this.email = email;
        this.nickName = nickName;
    }

    public User(String email, String nickName, String password, int role) {
        this.email = email;
        this.nickName = nickName;
        this.password = password;
        this.role = role;
    }

    public User(long id, String email, String nickName, String password, int role) {
        this.id = id;
        this.email = email;
        this.nickName = nickName;
        this.password = password;
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getNickName() {
        return nickName;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }
}
