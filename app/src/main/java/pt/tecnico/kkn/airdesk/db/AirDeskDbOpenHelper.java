package pt.tecnico.kkn.airdesk.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import pt.tecnico.kkn.airdesk.db.table.AclsTableHelper;
import pt.tecnico.kkn.airdesk.db.table.FilesTableHelper;
import pt.tecnico.kkn.airdesk.db.table.UsersTableHelper;
import pt.tecnico.kkn.airdesk.db.table.WorkspacesTableHelper;

/**
 * The database helper class for our application database
 */
public class AirDeskDbOpenHelper extends SQLiteOpenHelper {

    private static final int VERSION = 1;
    private static final String DB_NAME = "airdesk.db";

    public AirDeskDbOpenHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create users table
        UsersTableHelper.onCreate(db);
        // Create workspaces table
        WorkspacesTableHelper.onCreate(db);
        // Create acls table
        AclsTableHelper.onCreate(db);
        // Create files table
        FilesTableHelper.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Upgrade users table
        UsersTableHelper.onUpgrade(db, oldVersion, newVersion);
        // Upgrade workspaces table
        WorkspacesTableHelper.onUpgrade(db, oldVersion, newVersion);
        // Upgrade acls table
        AclsTableHelper.onUpgrade(db, oldVersion, newVersion);
        // Upgrade files table
        FilesTableHelper.onUpgrade(db, oldVersion, newVersion);
    }
}
