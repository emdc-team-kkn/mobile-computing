package pt.tecnico.kkn.airdesk.frontend;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonFloat;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pt.inesc.termite.wifidirect.SimWifiP2pDevice;
import pt.inesc.termite.wifidirect.SimWifiP2pDeviceList;
import pt.inesc.termite.wifidirect.SimWifiP2pManager;
import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.LocalWorkspaceDatasource;
import pt.tecnico.kkn.airdesk.datasource.Workspace;
import pt.tecnico.kkn.airdesk.frontend.adapter.WorkspaceAdapter;
import pt.tecnico.kkn.airdesk.wifidirect.AirdeskGroupInfoListener;
import pt.tecnico.kkn.airdesk.wifidirect.AirdeskService;
import pt.tecnico.kkn.airdesk.wifidirect.ForeignWorkspacesDiscoveryTask;

public class WorkspacesActivity extends ActionBarActivity implements ActionBar.TabListener, SimWifiP2pManager.PeerListListener {
    private final static String TAG = "WorkspacesActivity";

    private String email;
    private String nickname;
    private String password;
    private Boolean backPressedOnce = false;
    private Boolean start = false;
    private BroadcastReceiver groupDiscoverReceiver;
    private List<Workspace> workspacesList;
    private ExecutorService executorService;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workspaces);

        // Set up the action bar.
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        setTitle(R.string.title_activity_workspaces);

        SharedPreferences preferences = getSharedPreferences("AirDeskPreferences", MODE_PRIVATE);
        email = preferences.getString("EMAIL", null);
        nickname = preferences.getString("NICKNAME", null);
        password = preferences.getString("PASSWORD", null);

        if (email == null || nickname == null || password == null) {
            Intent intent = new Intent(this, RegisterActivity.class);
            startActivity(intent);
        }

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }

        executorService = Executors.newCachedThreadPool();

        groupDiscoverReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Discover new workspaces
                new ForeignWorkspacesDiscoveryTask(executorService, WorkspacesActivity.this)
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        };

        // Start the airdesk service
        Intent myIntent = new Intent(this, AirdeskService.class);
        this.startService(myIntent);
    }

    @Override
    protected void onStart() {
        super.onStart();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                groupDiscoverReceiver,
                new IntentFilter(AirdeskGroupInfoListener.GROUP_DISCOVER_RESULT)
        );
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(groupDiscoverReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_workspaces, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog dialog;

        switch (id) {
            case R.id.refresh_workspace:
                new ForeignWorkspacesDiscoveryTask(executorService, WorkspacesActivity.this)
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                return true;
            case R.id.action_search:
                Intent intent = new Intent(this, SearchActivity.class);
                startActivity(intent);

                return true;
            case R.id.action_about:
                builder.setMessage("App made by:\n\nNikola Koevski (80642) koevskin@gmail.com\n\nDuc Kien Truong (80536) duckientrueong@gmail.com").setTitle(R.string.dialog_about);
                builder.setNeutralButton("OK", null);
                dialog = builder.create();
                dialog.show();

                return true;
            case R.id.action_details:
                builder.setMessage("Email: " + email + "\n\nNickname: " + nickname).setTitle(R.string.dialog_title);
                builder.setNeutralButton("OK", null);
                dialog = builder.create();
                dialog.show();

                return true;
            case R.id.action_signout:
                SharedPreferences.Editor editor = getSharedPreferences("AirDeskPreferences", MODE_PRIVATE).edit();
                editor.putString("EMAIL", null);
                editor.putString("NICKNAME", null);
                editor.commit();

                Intent workspacesIntent = new Intent(this, WorkspacesActivity.class);
                startActivity(workspacesIntent);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPeersAvailable(SimWifiP2pDeviceList simWifiP2pDeviceList) {
        StringBuilder peersStr = new StringBuilder();
        //TODO CHANGE METOD
        // compile list of devices in range
        for (SimWifiP2pDevice device : simWifiP2pDeviceList.getDeviceList()) {
            String devstr = "" + device.deviceName + " (" + device.getVirtIp() + ")\n";
            peersStr.append(devstr);
        }
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return new OwnedWorkspacesFragment();
                case 1:
                    return new ForeignWorkspacesFragment();
                default:
                    return new OwnedWorkspacesFragment();
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class OwnedWorkspacesFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "0";

        private ListView listViewOwned;
        private ButtonFloat btnAddWorkspace;
        private Workspace[] workspaces;
        private WorkspaceAdapter workspaceAdapter;
        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static OwnedWorkspacesFragment newInstance(int sectionNumber) {
            OwnedWorkspacesFragment fragment = new OwnedWorkspacesFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public OwnedWorkspacesFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_workspace_owned, container, false);

            LocalWorkspaceDatasource workspaceDatasource = new LocalWorkspaceDatasource(getActivity());
            workspaceDatasource.open();
            List<Workspace> workspaceList = workspaceDatasource.getWorkspacesList();
            workspaces = workspaceList.toArray(new Workspace[workspaceList.size()]);
            workspaceDatasource.close();

            workspaceAdapter = new WorkspaceAdapter(rootView.getContext(), R.layout.list_item_workspace, workspaces);
            listViewOwned = (ListView)rootView.findViewById(R.id.listViewOwned);
            listViewOwned.setAdapter(workspaceAdapter);

            listViewOwned.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    String workspaceName = ((TextView) view.findViewById(R.id.txtName)).getText().toString();

                    Intent intent = new Intent(getActivity(), WorkspaceActivity.class);
                    intent.putExtra("WORKSPACE", workspaces[position]);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });

            final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.swipe_refresh_layout);
            swipeRefreshLayout.setColorSchemeResources(R.color.light_blue);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    LocalWorkspaceDatasource workspaceDatasource = new LocalWorkspaceDatasource(getActivity());
                    workspaceDatasource.open();
                    List<Workspace> workspaceList = workspaceDatasource.getWorkspacesList();
                    workspaces = workspaceList.toArray(new Workspace[workspaceList.size()]);
                    workspaceDatasource.close();
                    workspaceAdapter = new WorkspaceAdapter(rootView.getContext(), R.layout.list_item_workspace, workspaces);
                    listViewOwned.setAdapter(workspaceAdapter);

                    swipeRefreshLayout.setRefreshing(false);
                }
            });

            btnAddWorkspace = (ButtonFloat)rootView.findViewById(R.id.btnAddWorkspace);
            btnAddWorkspace.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), AddWorkspaceActivity.class);
                    intent.putExtra("TYPE", "WORKSPACE_ADD");
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });

            return rootView;
        }



        @Override
        public void onResume() {
            LocalWorkspaceDatasource workspaceDatasource = new LocalWorkspaceDatasource(getActivity());
            workspaceDatasource.open();
            List<Workspace> workspaceList = workspaceDatasource.getWorkspacesList();
            workspaces = workspaceList.toArray(new Workspace[workspaceList.size()]);
            workspaceDatasource.close();

            workspaceAdapter.notifyDataSetChanged();

            super.onResume();
        }
    }

    public static class ForeignWorkspacesFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "1";
        private ListView listViewForeign;
        private Workspace[] workspaces = {};
        private WorkspaceAdapter workspaceAdapter;
        private View rootView;

        private String email;
        private String nickname;

        public static ForeignWorkspacesFragment newInstance(int sectionNumber) {
            ForeignWorkspacesFragment fragment = new ForeignWorkspacesFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);

            return fragment;
        }

        public ForeignWorkspacesFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_workspace_foreign, container, false);

            SharedPreferences preferences = this.getActivity().getSharedPreferences("AirDeskPreferences", Context.MODE_PRIVATE);
            email = preferences.getString("EMAIL", null);
            nickname = preferences.getString("NICKNAME", null);

            workspaceAdapter = new WorkspaceAdapter(rootView.getContext(), R.layout.list_item_workspace, workspaces);
            listViewForeign = (ListView)rootView.findViewById(R.id.listViewForeign);
            listViewForeign.setAdapter(workspaceAdapter);

            listViewForeign.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    Intent intent = new Intent(getActivity(), ForeignWorkspaceActivity.class);
                    intent.putExtra("WORKSPACE", workspaces[position]);

                    startActivity(intent);
                }
            });

            final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.swipe_refresh_layout);
            swipeRefreshLayout.setColorSchemeResources(R.color.light_blue);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
//                    workspaceAdapter = new WorkspaceAdapter(rootView.getContext(), R.layout.list_item_workspace, workspaces);
//                    listViewForeign.setAdapter(workspaceAdapter);

                    WorkspacesActivity wa = ((WorkspacesActivity) getActivity());
                    new ForeignWorkspacesDiscoveryTask(wa.executorService, wa)
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                    swipeRefreshLayout.setRefreshing(false);
                }
            });

            this.rootView = rootView;

            WorkspacesActivity wa = ((WorkspacesActivity) getActivity());
            new ForeignWorkspacesDiscoveryTask(wa.executorService, wa)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            return rootView;
        }

        public void updateList(List<Workspace> workspaceList){
            workspaces = workspaceList.toArray(new Workspace[workspaceList.size()]);
            workspaceAdapter = new WorkspaceAdapter(rootView.getContext(), R.layout.list_item_workspace, workspaces);
            listViewForeign.setAdapter(workspaceAdapter);
            Toast.makeText(rootView.getContext(), "Foreign List Updated", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (backPressedOnce) {
            SharedPreferences pref = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("ipAddresses", null);
            editor.commit();
            super.onBackPressed();
            return;
        }

        this.backPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                backPressedOnce = false;
            }
        }, 2000);
    }

    public class ForeignWorkspacesCommTask extends AsyncTask<String, String, Void> {
        private static final String TAG = "ForeignWorkspaceTask";
        private static final String REQUEST = "discover";

        @Override
        protected Void doInBackground(String... strings) {
            String ipAddress = strings[0];
            int port = Integer.valueOf(getString(R.string.port));
            String result;
            //add workspaces to list
            Log.d(TAG, "Client Started");
            try {
                Log.d(TAG, "To Server: " + ipAddress + " " + port);
                SimWifiP2pSocket clientSocket = new SimWifiP2pSocket(ipAddress, port);
                Log.d(TAG, "Email :" + email);
                PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);

                writer.println(REQUEST);
                writer.println(email);

                Log.d(TAG, "Write Complete");

                BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                Log.d(TAG, "Read Started");
                result = br.readLine();

                JSONArray json = new JSONArray(result);

                for(int i = 0; i < json.length(); i++) {
                    Workspace workspace = Workspace.fromJSONObject(json.getJSONObject(i));
                    if (workspace != null) {
                        workspace.setForeign(true);
                        workspace.setIpAddress(ipAddress);
                        workspacesList.add(workspace);
                    }
                }

                Log.d(TAG, "Read Finished");
                clientSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Exception", e);
            } catch (JSONException e) {
                Log.e(TAG, "Json exception", e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ForeignWorkspacesFragment fragment = (ForeignWorkspacesFragment)
                    getSupportFragmentManager().getFragments().get(1);//findFragmentById(R.layout.fragment_workspace_foreign);
            fragment.updateList(workspacesList);
        }
    }
}
