package pt.tecnico.kkn.airdesk.helper;

import android.os.Environment;
import android.os.StatFs;

import java.text.DecimalFormat;

/**
 * Created by nikola on 19.3.15.
 */
public class MemBytesToHuman {
    private int totalMemory;
    private int busyMemory;
    private int freeMemory;

    public MemBytesToHuman() {
        StatFs statFs = new StatFs(Environment.getRootDirectory().getAbsolutePath());

        int totalMem = (statFs.getBlockCount() * statFs.getBlockSize());
        int freeMem = (statFs.getAvailableBlocks() * statFs.getBlockSize());
        int busyMem = totalMem - freeMem;

        this.totalMemory = totalMem;
        this.freeMemory = freeMem;
        this.busyMemory = busyMem;
    }

    public static Memory bytesToHuman (int size)
    {
        long Kb = 1  * 1024;
        long Mb = Kb * 1024;
        long Gb = Mb * 1024;
        long Tb = Gb * 1024;
        long Pb = Tb * 1024;
        long Eb = Pb * 1024;

        if (size <  Kb)
            return new Memory(size, "byte");
        else if (size >= Kb && size < Mb)
            return new Memory((int)(size / Kb), "Kb");
        else if (size >= Mb && size < Gb)
            return new Memory((int)(size / Mb), "Mb");
        else if (size >= Gb && size < Tb)
            return new Memory((int)(size / Gb), "Gb");
        else if (size >= Tb && size < Pb)
            return new Memory((int)(size / Tb), "Tb");
        else if (size >= Pb && size < Eb)
            return new Memory((int)(size / Pb), "Pb");
        else
            return new Memory((int)(size / Eb), "Eb");
    }

    public static Memory bytesToHumanType (long size, String type)
    {
        long Kb = 1  * 1024;
        long Mb = Kb * 1024;
        long Gb = Mb * 1024;
        long Tb = Gb * 1024;
        long Pb = Tb * 1024;
        long Eb = Pb * 1024;

        switch (type) {
            case "Kb":
                return new Memory((int) (size / Kb), "Kb");
            case "Mb":
                return new Memory((int) (size / Mb), "Mb");
            case "Gb":
                return new Memory((int) (size / Gb), "Gb");
            case "Tb":
                return new Memory((int) (size / Tb), "Tb");
            case "Pb":
                return new Memory((int) (size / Pb), "Pb");
            case "Eb":
                return new Memory((int) (size / Eb), "Eb");
            default:
                return new Memory((int)size, "byte");
        }
    }

    public static long humanToBytes (Memory m) {
        long Kb = 1  * 1024;
        long Mb = Kb * 1024;
        long Gb = Mb * 1024;
        long Tb = Gb * 1024;
        long Pb = Tb * 1024;
        long Eb = Pb * 1024;

        switch (m.getType()) {
            case "Eb":
                return m.getValue() * Eb;
            case "Pb":
                return m.getValue() * Pb;
            case "Tb":
                return m.getValue() * Tb;
            case "Gb":
                return m.getValue() * Gb;
            case "Mb":
                return m.getValue() * Mb;
            case "Kb":
                return m.getValue() * Kb;
            default:
                return m.getValue();
        }
    }

    public static long humanToBytesType (long size, String type) {
        long Kb = 1  * 1024;
        long Mb = Kb * 1024;
        long Gb = Mb * 1024;
        long Tb = Gb * 1024;
        long Pb = Tb * 1024;
        long Eb = Pb * 1024;

        switch (type) {
            case "Eb":
                return size / Eb;
            case "Pb":
                return size / Pb;
            case "Tb":
                return size / Tb;
            case "Gb":
                return size / Gb;
            case "Mb":
                return size / Mb;
            case "Kb":
                return size / Kb;
            default:
                return size;
        }
    }

    public int getTotalMemory() {
        return totalMemory;
    }

    public int getBusyMemory() {
        return busyMemory;
    }

    public int getFreeMemory() {
        return freeMemory;
    }
}
