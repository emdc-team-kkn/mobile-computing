package pt.tecnico.kkn.airdesk.frontend;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonRectangle;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.UnknownHostException;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.LocalFileDatasource;
import pt.tecnico.kkn.airdesk.datasource.Workspace;
import pt.tecnico.kkn.airdesk.helper.Validation;

public class AddFileActivity extends ActionBarActivity {
    private ButtonRectangle btnSaveFile;
    private EditText txtFilename;
    private EditText txtContent;

    private Workspace workspace;
    private Context context = this;
    private Boolean isForeign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_file);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        workspace = intent.getParcelableExtra("WORKSPACE");
        isForeign = workspace.isForeign();

        txtFilename = (EditText)findViewById(R.id.txtFilename);
        txtContent = (EditText)findViewById(R.id.txtContent);
        btnSaveFile = (ButtonRectangle)findViewById(R.id.btnSave);

        btnSaveFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Write file to FS and save to DB
                String filename = txtFilename.getText().toString();
                String content = txtContent.getText().toString();

                if (Validation.hasText(txtFilename, "The filename is required")) {
                    if (workspace.getQuota() > workspace.getSize() + content.getBytes().length) {
                        if(workspace.isForeign()) {

                            pt.tecnico.kkn.airdesk.datasource.File file = new pt.tecnico.kkn.airdesk.datasource.File((int)workspace.getId(), filename, content);
                            new CreateFileCommTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, workspace.getIpAddress(), file.toJSONObject().toString(), workspace.toJSONObject().toString());

                        } else {

                            pt.tecnico.kkn.airdesk.datasource.File file = new pt.tecnico.kkn.airdesk.datasource.File((int) workspace.getId(), filename, content);

                            LocalFileDatasource fileDatasource = new LocalFileDatasource(context, (int) workspace.getId());
                            fileDatasource.open();
                            try {
                                fileDatasource.newFile(file);
                            } catch (FileNotFoundException e) {
                                Toast.makeText(context, "Error while saving file information", Toast.LENGTH_LONG).show();
                                Log.d("FILE", e.getMessage());
                            } finally {
                                fileDatasource.close();
                            }

                            File newFile = new File(context.getDir((workspace.getName() + Long.toString(workspace.getId())), Context.MODE_PRIVATE), filename + ".txt");
                            try {
                                FileOutputStream outputStream = new FileOutputStream(newFile);

                                outputStream.write(content.getBytes());
                                outputStream.close();
                            } catch (FileNotFoundException e) {
                                Toast.makeText(context, "Error when writing to file.", Toast.LENGTH_LONG).show();
                                Log.d("FILE", e.getMessage());
                            } catch (IOException e) {
                                Toast.makeText(context, "Error while writing to file.", Toast.LENGTH_LONG).show();
                                Log.d("FILE", e.getMessage());
                            }

                            Intent workspaceIntent = new Intent(context, WorkspaceActivity.class);
                            workspaceIntent.putExtra("WORKSPACE", workspace);
                            startActivity(workspaceIntent);
                        }
                    } else {
                        Toast.makeText(context, "The file is too big for the workspace!", Toast.LENGTH_LONG).show();
                        Log.d("FILE", "Workspace breaches quota");
                    }
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_file, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            if (workspace.isForeign()) {
                Intent workspaceIntent = new Intent(context, ForeignWorkspaceActivity.class);
                workspaceIntent.putExtra("WORKSPACE", workspace);
                finish();
                startActivity(workspaceIntent);
            } else {
                Intent workspaceIntent = new Intent(context, WorkspaceActivity.class);
                workspaceIntent.putExtra("WORKSPACE", workspace);
                finish();
                startActivity(workspaceIntent);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class CreateFileCommTask extends AsyncTask<String, String, Void> {
        private static final String TAG = "CreateFileTask";
        private static final String REQUEST = "createfile";

        @Override
        protected Void doInBackground(String... params) {
            Log.d(TAG, "Client Started: " + REQUEST);
            String ipAddress = params[0];
            String jsonString = params[1];
            String workspaceString = params[2];
            String result;

            SharedPreferences preferences = getSharedPreferences("AirDeskPreferences", MODE_PRIVATE);
            String email = preferences.getString("EMAIL", null);
            String password = preferences.getString("PASSWORD", null);

            try {
                Log.d(TAG, "To Server: " + ipAddress + " " + getString(R.string.port));
                SimWifiP2pSocket clientSocket = new SimWifiP2pSocket(ipAddress, Integer.parseInt(getString(R.string.port)));

                PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);

                Log.d(TAG, "Write Started");
                writer.println(REQUEST);

                Log.d(TAG, "Write Email: " + email);
                writer.println(email);

                Log.d(TAG, "Write Password: " + password);
                writer.println(password);

                Log.d(TAG, "Write Workspace: " + workspaceString);
                writer.println(workspaceString);

                Log.d(TAG, "Write File: " + jsonString);
                writer.println(jsonString);

                Log.d(TAG, "Write Complete");

                BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                Log.d(TAG, "Read Started");

                result = br.readLine();
                publishProgress(result);

                Log.d(TAG, "Read Finished");

                clientSocket.close();
            } catch (UnknownHostException e) {
                Toast.makeText(context, "Workspace owner unavailable, try again later", Toast.LENGTH_SHORT).show();

                Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(workspacesIntent);
            } catch (IOException e) {
                Toast.makeText(context, "Workspace owner unavailable, try again later", Toast.LENGTH_SHORT).show();

                Intent workspacesIntent = new Intent(context, WorkspacesActivity.class);
                workspacesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(workspacesIntent);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            String res = values[0];
            Log.d(TAG, res);
            if (res.equals("json_error")) {
                Toast.makeText(context, "Bad message format, try again", Toast.LENGTH_SHORT).show();
            } else if (res.equals("User not found")){
                Toast.makeText(context, "Error: User not found", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    JSONObject result = new JSONObject(res);
                    String type = result.getString("type");
                    String message = result.getString("message");

                    Toast.makeText(context, type.toUpperCase() + ": " + message, Toast.LENGTH_LONG).show();

                    Intent workspaceIntent = new Intent(context, ForeignWorkspaceActivity.class);
                    workspaceIntent.putExtra("WORKSPACE", workspace);
                    startActivity(workspaceIntent);
                } catch (JSONException e) {
                    Toast.makeText(context, "Bad message format, try again", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
