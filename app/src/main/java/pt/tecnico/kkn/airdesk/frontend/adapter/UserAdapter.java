package pt.tecnico.kkn.airdesk.frontend.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.gc.materialdesign.views.Button;

import java.util.ArrayList;
import java.util.List;

import pt.tecnico.kkn.airdesk.R;
import pt.tecnico.kkn.airdesk.datasource.LocalWorkspaceDatasource;
import pt.tecnico.kkn.airdesk.datasource.User;
import pt.tecnico.kkn.airdesk.datasource.UserDatasource;
import pt.tecnico.kkn.airdesk.datasource.Workspace;

/**
 * Created by nikola on 13.5.15.
 */
public class UserAdapter extends ArrayAdapter<User> {
    private static final String TAG = "UserAdapter";
    private Context context;
    private Workspace workspace;
    private int layoutResourceId;
    private List<User> data = null;
    private String[] roles;

    public UserAdapter(Context context, int layoutResourceId, List<User> data, Workspace workspace) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
        this.workspace = workspace;
        roles = context.getResources().getStringArray(R.array.user_roles);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        UserHolder holder;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new UserHolder();
            holder.txtName = (TextView)row.findViewById(R.id.txtName);
            holder.btnRemove = (android.widget.Button)row.findViewById(R.id.btnRemove);
            holder.btnEdit = (android.widget.Button)row.findViewById(R.id.btnEdit);

            row.setTag(holder);
        }
        else
        {
            holder = (UserHolder)row.getTag();
        }

        final User user = data.get(position);
        holder.txtName.setText(roles[user.getRole()] + ": " + user.getEmail());

        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom_dialog);
                dialog.setTitle("Edit User Role");

                // set the custom dialog components - text, image and button
                final Spinner spinner = (Spinner) dialog.findViewById(R.id.spinner);
                android.widget.Button btnEdit = (android.widget.Button) dialog.findViewById(R.id.btnEdit);
                android.widget.Button btnCancel = (android.widget.Button) dialog.findViewById(R.id.btnCancel);

                UserRoleSpinnerAdapter roleSpinnerAdapter = new UserRoleSpinnerAdapter(context, R.layout.spinner_item_user, roles);
                roleSpinnerAdapter.setDropDownViewResource(R.layout.custom_spinner_layout);
                spinner.setAdapter(roleSpinnerAdapter);

                User updateUser = data.get(position);
                spinner.setSelection(updateUser.getRole());

                btnEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String txtRole = spinner.getSelectedItem().toString();
                        int role;

                        switch (txtRole) {
                            case "Reader":
                                role = 0;
                                break;
                            case "Editor":
                                role = 1;
                                break;
                            case "Admin":
                                role = 2;
                                break;
                            default:
                                role = 0;
                                break;
                        }

                        data.get(position).setRole(role);
                        LocalWorkspaceDatasource workspaceDatasource = new LocalWorkspaceDatasource(context);
                        workspaceDatasource.open();
                        int res = workspaceDatasource.updateUserRole(workspace, data.get(position));
                        workspaceDatasource.close();

                        Log.d(TAG, "User role updated: " + String.valueOf(res > 0));

                        notifyDataSetChanged();

                        dialog.dismiss();
                    }
                });

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(TAG, "Cancel pressed");

                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        holder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = data.get(position);
                Log.d(TAG, "User id: " + String.valueOf(user.getId()));
                LocalWorkspaceDatasource workspaceDatasource = new LocalWorkspaceDatasource(context);
                workspaceDatasource.open();
                Boolean result = workspaceDatasource.removeUserFromWorkspace(workspace, user);
                Log.d(TAG, "User removed: " + result.toString());
                workspaceDatasource.close();
                data.remove(position);
                notifyDataSetChanged();
            }
        });

        return row;
    }

    static class UserHolder
    {
        TextView txtName;
        android.widget.Button btnEdit;
        android.widget.Button btnRemove;
    }
}
