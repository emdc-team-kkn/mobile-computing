package pt.tecnico.kkn.airdesk.datasource;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.List;

import pt.tecnico.kkn.airdesk.db.AirDeskDbOpenHelper;
import pt.tecnico.kkn.airdesk.db.table.AclsTableHelper;
import pt.tecnico.kkn.airdesk.db.table.UsersTableHelper;
import pt.tecnico.kkn.airdesk.db.table.WorkspacesTableHelper;
import pt.tecnico.kkn.airdesk.exception.UserNotFoundException;

/**
 * Created by nikola on 7.4.15.
 */
public class RemoteWorkspaceDatasource {
    private SQLiteDatabase db;
    private AirDeskDbOpenHelper dbOpenHelper;
    private SQLiteStatement newWorkspaceStatement;
    private SQLiteStatement updateWorkspaceStatement;
    private SQLiteStatement deleteWorkspaceStatement;
    private SQLiteStatement addUserToWorkspaceStatement;
    private SQLiteStatement removeUserFromWorkspaceStatement;

    public RemoteWorkspaceDatasource(Context context) {
        dbOpenHelper = new AirDeskDbOpenHelper(context);
    }

    /**
     * Get all the workspaces a user has been invited into
     * @param u The user for who the DB is queried
     */
    public List<Workspace> getUserWorkspaces(User u) {
        String workspaceQuery = "select * from " + AclsTableHelper.TABLE_NAME + " inner join " + UsersTableHelper.TABLE_NAME + " on " + AclsTableHelper.TABLE_NAME + "." + AclsTableHelper.COL_USER_ID + " = " + UsersTableHelper.TABLE_NAME + "." + UsersTableHelper.COL_ID +
                                                                                " inner join " + WorkspacesTableHelper.TABLE_NAME + " on " + AclsTableHelper.TABLE_NAME + "." + AclsTableHelper.COL_WORKSPACE_ID + " = " + WorkspacesTableHelper.TABLE_NAME + "." + WorkspacesTableHelper.COL_ID +
                                " where " + UsersTableHelper.COL_EMAIL + " = ? AND " + WorkspacesTableHelper.COL_DELETED_AT + " is null";
        String[] args = { u.getEmail() };
        Cursor c = db.rawQuery(workspaceQuery, args);

        List<Workspace> ret = new ArrayList<>();
        while(c.moveToNext()) {
            long id = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_ID));
            String name = c.getString(c.getColumnIndex(WorkspacesTableHelper.COL_NAME));
            Integer quota = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_QUOTA));
            Integer size = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_SIZE));
            Boolean isPublic = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_IS_PUBLIC)) == 1;
            Boolean isForeign = c.getInt(c.getColumnIndex(WorkspacesTableHelper.COL_IS_FOREIGN)) == 1;
            String publicProfile = c.getString(c.getColumnIndex(WorkspacesTableHelper.COL_PUBLIC_PROFILE));
            String tags = c.getString(c.getColumnIndex(WorkspacesTableHelper.COL_TAGS));

            ret.add(new Workspace(id, isForeign, isPublic, quota, size, name, publicProfile, tags));
        }
        c.close();

        return ret;
    }

    /**
     * Remove a user from a workspace
     * @param w The workspace to remove from
     * @param u The user to be removed
     */
    public boolean removeUserFromWorkspace(Workspace w, User u) {
        int updatedRow = db.delete(AclsTableHelper.TABLE_NAME, AclsTableHelper.COL_USER_ID + " = ? AND " + AclsTableHelper.COL_WORKSPACE_ID + " = ?", new String[] { Long.toString(u.getId()), Long.toString(w.getId()) });

        return updatedRow > 0;
    }

    /**
     * Check the users details
     * @param email The user's email
     */
    public User getUserDetails(String email) throws UserNotFoundException{
        String query = "select * from " + UsersTableHelper.TABLE_NAME + " where " + UsersTableHelper.COL_EMAIL + " = ?";
        Cursor c = db.rawQuery(query, new String[] { email });

        if (c.getCount() == 0) {
            throw new UserNotFoundException();
        }

        c.moveToNext();
        long id = c.getInt(c.getColumnIndex(UsersTableHelper.COL_ID));
        String newEmail = c.getString(c.getColumnIndex(UsersTableHelper.COL_EMAIL));
        String nickName = c.getString(c.getColumnIndex(UsersTableHelper.COL_NICKNAME));
        c.close();
        return new User(id, newEmail, nickName);
    }

    public void open() {
        db = dbOpenHelper.getWritableDatabase();
    }

    public void close() {
        if (newWorkspaceStatement != null) {
            newWorkspaceStatement.close();
            newWorkspaceStatement = null;
        }
        if (updateWorkspaceStatement != null) {
            updateWorkspaceStatement.close();
            updateWorkspaceStatement = null;
        }
        if (deleteWorkspaceStatement != null) {
            deleteWorkspaceStatement.close();
            deleteWorkspaceStatement = null;
        }
        if (addUserToWorkspaceStatement != null) {
            addUserToWorkspaceStatement.close();
            addUserToWorkspaceStatement = null;
        }
        if (removeUserFromWorkspaceStatement != null) {
            removeUserFromWorkspaceStatement.close();
            removeUserFromWorkspaceStatement = null;
        }
        if (db != null) {
            db.close();
            db = null;
        }
    }
}
