package pt.tecnico.kkn.airdesk.wifidirect;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocketServer;

/**
 * Server components of AirDesk
 */
public class AirdeskServer implements Runnable {
    private static final String TAG = "AirdeskServer";

    private int port;
    private SimWifiP2pSocketServer mSrvSocket;
    private ExecutorService executorService = Executors.newCachedThreadPool();
    private boolean stopped = true;
    private Context context;

    public AirdeskServer(int port, Context context) {
        this.port = port;
        this.context = context;
    }

    @Override
    public void run() {
        try {
            mSrvSocket = new SimWifiP2pSocketServer(port);
            stopped = false;
            SimWifiP2pSocket sock;
            Log.d(TAG, "Server started");
            while (!stopped) {

                try {
                    sock = mSrvSocket.accept();
                    executorService.submit(new AirdeskServerHandler(sock, this.context));
                } catch (IOException e) {
                    if (stopped) {
                        Log.d(TAG, "Server stopped");
                        return;
                    }
                    Log.d("Error accepting socket:", e.getMessage());
                    break;
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "Unable to start server", e);
        } finally {
            stopped = true;
        }
    }

    public synchronized void stop() {
        if (stopped) {
            return;
        }
        stopped = true;
        try {
            mSrvSocket.close();
        } catch (IOException e) {
            Log.e(TAG, "Unable to stop server", e);
        }
    }
}
